﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="750f326c-4959-4c30-94cb-87646be40206" revisionNumber="1">
  <developerHowToDocument
    xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5"
    xmlns:xlink="http://www.w3.org/1999/xlink">

    <summary>
      <para>This page shows how to install the Attender Visio add-in</para>
    </summary>

    <introduction>
      <para>Instsallation procedure</para>
      <autoOutline/>
    </introduction>

    <procedure>
      <title>How to install the Attender Visio add-in</title>
      <steps class="ordered">
        <step>
          <content>
            <para>Requirements:</para>
            <list class="bullet">
              <listItem>
                <para>
                  Visio 2010 32-bit or 2013 32-bit. 64-bit versions are not supported.
                </para>
              </listItem>
              <listItem>
                <para>
                  WMI (Windows Management Instrumentation) should be enabled (it is by default) on your CIC server(s).
                </para>
                <para>
                  To enable WMI on Windows 2008, check the tutorial
                  <externalLink>
                    <linkText>here</linkText>
                    <linkAlternateText>http://technet.microsoft.com/en-us/library/cc771551.aspx</linkAlternateText>
                    <linkUri>http://technet.microsoft.com/en-us/library/cc771551.aspx</linkUri>
                  </externalLink>
                </para>
                <para>
                  The user you will use to authenticate against your CIC server (using Windows/Domain credentials) must have the "Remote Enable" permission.
                </para>
              </listItem>
              <listItem>
                <para>
                  .Net Framework 4.0 (see requirements
                  <externalLink>
                    <linkText>here</linkText>
                    <linkAlternateText>http://msdn.microsoft.com/en-us/library/8z6watww(v=vs.100).aspx</linkAlternateText>
                    <linkUri>http://msdn.microsoft.com/en-us/library/8z6watww(v=vs.100).aspx</linkUri>
                  </externalLink>
                  ). If .Net 4.0 is not installed, the installer will prompt you to install it by showing a message that your .Net framework is out of date. Click OK to install .Net 4.0 (no connection is necessary).
                </para>
                <para>
                  To double-check that the .Net Framework is fully installed, make sure you see the following in your "Programs and Features" control panel:
                </para>
                <mediaLink>
                  <image xlink:href="Installation_NetFramework40"/>
                </mediaLink>
              </listItem>
              <listItem>
                <para>
                  a CIC 2.3.1/2.4/3.0/4.0 server with Interaction Attendant. Note that the log file scanning functionality is only available with CIC 4.0 log files.
                </para>
              </listItem>
            </list>
          </content>
        </step>
        <step>
          <content>
            <para>Run the setup.exe program</para>
          </content>
        </step>
        <step>
          <content>
            <para>If a requirement is missing, the setup program will install it for you. Click on "Install" to continue. Click on "Yes" if asked to install this package.</para>
            <para>
              <legacyBold>
                An internet connection is required to download the required software
              </legacyBold>
            </para>
            <mediaLink>
              <image xlink:href="Installation_Requirements"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Let the installation download the required software
            </para>
            <mediaLink>
              <image xlink:href="Installation_Requirements_Download"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Follow any extra steps required to install the required software. It may include a separate installation program similar to the screenshot below:
            </para>
            <mediaLink>
              <image xlink:href="Installation_Microsoft_License_Terms"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Click on "Next" when the following page appears
            </para>
            <mediaLink>
              <image xlink:href="Installation_FirstScreen"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Click on "I accept the terms in the license agreement" and click on "Next".
            </para>
            <mediaLink>
              <image xlink:href="Installation_LicenseAgreementPage"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Leave the default directory or choose a different directory to install the add-in and click on "Next".
            </para>
            <mediaLink>
              <image xlink:href="Installation_DirectoryScreen"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Review the summary and click on "Install" (requires administrator rights)
            </para>
            <mediaLink>
              <image xlink:href="Installation_SummaryScreen"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Let the setup program complete.
            </para>
            <mediaLink>
              <image xlink:href="Installation_InProgressScreen"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              During the installation, a window will pop in the background. This is required to register the add-in in Visio. Click on "Install" to continue.
            </para>
            <mediaLink>
              <image xlink:href="Installation_OfficeCustomizationInstaller"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              When done, click on "Close".
            </para>
            <mediaLink>
              <image xlink:href="Installation_OfficeVSTO_Done"/>
            </mediaLink>
          </content>
        </step>
        <step>
          <content>
            <para>
              Finally, click on "Finish". Go to the <link xlink:href="96482b96-f448-4d57-a90f-5c3ce42b21a2">Getting Started</link> section.
            </para>
          </content>
        </step>
      </steps>
      <conclusion>
        <content>
          <para>
            Should you have any issues, please contact the author of this add-in at <externalLink>
              <linkText>http://icattender.wordpress.com</linkText>
              <linkAlternateText>http://icattender.wordpress.com</linkAlternateText>
              <linkUri>http://icattender.wordpress.com</linkUri>
            </externalLink>
          </para>
        </content>
      </conclusion>
    </procedure>

    <security>
      <content>
        <para>This program require Administrator privileges to run.</para>
      </content>
    </security>
    
    <relatedTopics/>
  </developerHowToDocument>
</topic>
