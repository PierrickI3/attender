﻿namespace Helper
{
    public class ActionEventArgs
    {
        public string ItemName { get; set; }

        public ActionEventArgs(string itemName)
        {
            ItemName = itemName;
        }
    }
}