﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Helper
{
    public class WMIHelper
    {
        #region Enums

        public enum RegHive : uint
        {
            HkeyClassesRoot = 0x80000000,
            HkeyCurrentUser = 0x80000001,
            HkeyLocalMachine = 0x80000002,
            HkeyUsers = 0x80000003,
            HkeyCurrentConfig = 0x80000005
        }
        public enum RegType
        {
            RegSz = 1,
            RegExpandSz,
            RegBinary,
            RegDword,
            RegMultiSz = 7
        }

        #endregion

        #region Properties
        public string Username { get; set; }
        public string Password { get; set; }
        public string ServerName { get; set; }
        public string RegistryServerPath { get; set; }
        #endregion

        [XmlArray("AttendantProfiles")]
        [XmlArrayItem("AttendantProfile")]
        public List<AttendantProfile> AttendantProfiles = new List<AttendantProfile>();
        private ManagementClass _managementClass;
        private string _activeAttendantConfig;
        private string _baseProfilesPath;
        private string _serverRegistryPath = @"SOFTWARE\Wow6432Node\Interactive Intelligence\EIC\Directory Services\Root";

        #region Event Handlers
        public delegate void ReadingProfileEventHandler(object sender, ActionEventArgs e);
        public event ReadingProfileEventHandler ReadingProfile;

        public delegate void ReadingScheduleEventHandler(object sender, ActionEventArgs e);
        public event ReadingScheduleEventHandler ReadingSchedule;

        public delegate void ReadingActionEventHandler(object sender, ActionEventArgs e);
        public event ReadingActionEventHandler ReadingAction;
        #endregion

        public WMIHelper()
        {

        }
        public WMIHelper(string username, string password, string serverName)
        {
            Username = username;
            Password = password;
            ServerName = serverName;

            if (!ConnectToRemoteRegistry())
            {
                return;
            }

            InitRegistryPaths();
        }

        public void LoadFrom(string filename)
        {
            var xmlSerializer = new XmlSerializer(typeof(List<AttendantProfile>), ActionTypes.AllTypes);
            var fs = new FileStream(filename, FileMode.Open);
            AttendantProfiles = (List<AttendantProfile>)xmlSerializer.Deserialize(fs);

            if (AttendantProfiles.Count > 0)
            {
                RegistryServerPath = AttendantProfiles[0].ServerPath;
            }
            else
            {
                MessageBox.Show("No profiles found.");
            }
        }
        public void SaveTo(string filename)
        {
            var xmlSerializer = new XmlSerializer(typeof(List<AttendantProfile>), ActionTypes.AllTypes);
            using (var wr = new StreamWriter(filename))
            {
                xmlSerializer.Serialize(wr, AttendantProfiles);
            }
        }

        private bool ConnectToRemoteRegistry()
        {
            try
            {
                var options = new ConnectionOptions { Impersonation = ImpersonationLevel.Impersonate, EnablePrivileges = true, Username = Username, Password = Password };
                var myScope = new ManagementScope("\\\\" + ServerName + "\\root\\default", options);
                myScope.Connect();
                _managementClass = new ManagementClass(myScope, new ManagementPath("StdRegProv"), null);

                return true;
            }
            catch (COMException)
            {
                MessageBox.Show("An error occurred while connecting to your CIC server. Please check your credentials.");
            }
            return false;
        }

        public void InitRegistryPaths()
        {
            // Get SERVER Path
            var oServerPath = GetValue(_managementClass, RegHive.HkeyLocalMachine, _serverRegistryPath, "SERVER");

            //Might be an older (non-x64) version
            if (oServerPath == null)
            {
                oServerPath = GetValue(_managementClass, RegHive.HkeyLocalMachine, @"SOFTWARE\Interactive Intelligence\EIC\Directory Services\Root", "SERVER");
                if (oServerPath == null)
                {
                    throw new Exception("Could not find SERVER value in registry.");
                }
                else
                {
                    _serverRegistryPath = @"SOFTWARE\Interactive Intelligence\EIC\Directory Services\Root";
                }
            }

            RegistryServerPath = GetMultiStringValue(oServerPath, 0);

            // Get Active Attendant Config
            var oAttendantConfig = GetValue(_managementClass, RegHive.HkeyLocalMachine, FormatRegistryPath("${SERVER}\\AttendantData"), "ActiveConfig");
            if (oAttendantConfig == null) throw new Exception("Could not find Active Attendant Config.");
            _activeAttendantConfig = GetMultiStringValue(oAttendantConfig, 0);

            _baseProfilesPath = String.Format("{0}\\{1}", FormatRegistryPath("${SERVER}\\AttendantData"), _activeAttendantConfig);
        }
        public string FormatRegistryPath(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                return String.Empty;
            }

            if (!path.StartsWith("SOFTWARE"))
            {
                path = _serverRegistryPath + path;
            }

            //path = path.Replace("${SERVER}", RegistryServerPath).Replace("\\Attendant01\\", "\\Attendant\\");
            path = path.Replace("${SERVER}", RegistryServerPath);

            return path;
        }

        public IEnumerable<AttendantProfile> GetAttendantProfiles(IEnumerable<AttendantProfile> attendantProfiles)
        {
            try
            {
                if (attendantProfiles == null)
                {
                    var foundAttendantProfiles = new List<AttendantProfile>();
                    Parallel.ForEach(GetSubKeys(_managementClass, RegHive.HkeyLocalMachine, _baseProfilesPath), attendantProfile => foundAttendantProfiles.Add(GetProfileInformation(attendantProfile)));
                    return foundAttendantProfiles;
                }

                // Return AttendantProfile objects for selected attendant profiles
                return attendantProfiles.Select(attendantProfile => GetProfileInformation(attendantProfile.Name));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return null;
        }
        public IEnumerable<ScheduleInformation> GetAttendantSchedules(string profileKey)
        {
            try
            {
                var attendantSchedules = new List<ScheduleInformation>();
                Parallel.ForEach(GetSubKeys(_managementClass, RegHive.HkeyLocalMachine, profileKey), attendantSchedule => attendantSchedules.Add(GetScheduleInformation(profileKey + "\\" + attendantSchedule)));
                return attendantSchedules;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// Reads the registry information from the CIC server.
        /// </summary>
        /// <returns>True if successful, false otherwise</returns>
        public bool GetAttendantInformation(IEnumerable<AttendantProfile> attendantProfiles)
        {
            try
            {
                Parallel.ForEach(attendantProfiles, attendantProfile =>
                {
                    Parallel.ForEach(GetAttendantSchedules(attendantProfile.FullNodePath), attendantSchedule =>
                    {
                        attendantSchedule.ActionList = GetChildrenActions(attendantSchedule.FullNodePath);
                        attendantProfile.Schedules.Add(attendantSchedule);
                    });
                    AttendantProfiles.Add(attendantProfile);
                });
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return false;
        }

        private string GetKeyValue(IDictionary<string, string> valueNames, string name)
        {
            return valueNames.ContainsKey(name) ? ReadKeyValue(valueNames[name]) : String.Empty;
        }
        private string ReadKeyValue(string value)
        {
            string returnValue = String.Empty;
            if (value.Equals("Yes", StringComparison.InvariantCultureIgnoreCase) || value.Equals("True", StringComparison.InvariantCultureIgnoreCase))
                returnValue = "true";
            else if (value.Equals("No", StringComparison.InvariantCultureIgnoreCase) || value.Equals("False", StringComparison.InvariantCultureIgnoreCase))
                returnValue = "false";
            else
                returnValue = value;

            return returnValue;
        }
        public string GetMultiStringValue(object strings, int index)
        {
            return ((string[])strings)[index];
        }

        #region WMI
        public string GetStringValue(ManagementClass mc, RegHive hDefKey, string sSubKeyName, string sValueName)
        {
            var rType = GetValueType(mc, hDefKey, sSubKeyName, sValueName);

            var inParams = mc.GetMethodParameters("GetStringValue");
            inParams["hDefKey"] = hDefKey;
            inParams["sSubKeyName"] = sSubKeyName;
            inParams["sValueName"] = sValueName;

            var oValue = String.Empty;

            switch (rType)
            {
                case RegType.RegSz:
                    var outParams = mc.InvokeMethod("GetStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["sValue"].ToString();
                    }
                    break;

                case RegType.RegExpandSz:
                    outParams = mc.InvokeMethod("GetExpandedStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["sValue"].ToString();
                    }
                    break;

                case RegType.RegMultiSz:
                    outParams = mc.InvokeMethod("GetMultiStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = ((string[])outParams["sValue"])[0];
                    }
                    break;

                case RegType.RegDword:
                    outParams = mc.InvokeMethod("GetDWORDValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["uValue"].ToString();
                    }
                    break;

                case RegType.RegBinary:
                    outParams = mc.InvokeMethod("GetBinaryValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = (outParams["uValue"] as byte[]).ToString();
                    }
                    break;
            }
            return oValue;
        }
        public object GetValue(ManagementClass mc, RegHive hDefKey, string sSubKeyName, string sValueName)
        {
            var rType = GetValueType(mc, hDefKey, sSubKeyName, sValueName);

            var inParams = mc.GetMethodParameters("GetStringValue");
            inParams["hDefKey"] = hDefKey;
            inParams["sSubKeyName"] = sSubKeyName;
            inParams["sValueName"] = sValueName;

            object oValue = null;

            switch (rType)
            {
                case RegType.RegSz:
                    var outParams = mc.InvokeMethod("GetStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["sValue"];
                    }
                    break;

                case RegType.RegExpandSz:
                    outParams = mc.InvokeMethod("GetExpandedStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["sValue"];
                    }
                    break;

                case RegType.RegMultiSz:
                    outParams = mc.InvokeMethod("GetMultiStringValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["sValue"];
                    }
                    break;

                case RegType.RegDword:
                    outParams = mc.InvokeMethod("GetDWORDValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["uValue"];
                    }
                    break;

                case RegType.RegBinary:
                    outParams = mc.InvokeMethod("GetBinaryValue", inParams, null);

                    if (Convert.ToUInt32(outParams["ReturnValue"]) == 0)
                    {
                        oValue = outParams["uValue"] as byte[];
                    }
                    break;
            }
            return oValue;
        }
        public RegType GetValueType(ManagementClass mc, RegHive hDefKey, string sSubKeyName, string sValueName)
        {
            var inParams = mc.GetMethodParameters("EnumValues");
            inParams["hDefKey"] = hDefKey;
            inParams["sSubKeyName"] = sSubKeyName;

            var outParams = mc.InvokeMethod("EnumValues", inParams, null);

            if (Convert.ToUInt32(outParams["ReturnValue"]) != 0)
            {
                return default(RegType);
            }

            var sNames = outParams["sNames"] as String[];
            var iTypes = outParams["Types"] as int[];

            for (var i = 0; i < sNames.Length; i++)
            {
                if (sNames[i] == sValueName)
                {
                    return (RegType)iTypes[i];
                }
            }
            return default(RegType);
        }
        public Dictionary<String, String> GetValues(ManagementClass mc, RegHive hDefKey, string sSubKeyName)
        {
            var dictionary = new Dictionary<String, String>();

            var inParams = mc.GetMethodParameters("EnumValues");
            inParams["hDefKey"] = hDefKey;
            inParams["sSubKeyName"] = sSubKeyName;

            var outParams = mc.InvokeMethod("EnumValues", inParams, null);
            if (outParams != null && Convert.ToUInt32(outParams["ReturnValue"]) == 0)
            {
                var names = outParams["sNames"] as string[];
                if (names != null)
                {
                    foreach (var name in names)
                    {
                        var value = GetValue(mc, hDefKey, sSubKeyName, name);
                        var values = value as string[];
                        if (values != null)
                        {
                            dictionary.Add(name, values[0]);
                        }
                    }
                }
            }
            return dictionary;
        }
        public static string[] GetSubKeys(ManagementClass mc, RegHive hDefKey, string sSubKeyName)
        {
            var inParams = mc.GetMethodParameters("EnumKey");
            inParams["hDefKey"] = hDefKey;
            inParams["sSubKeyName"] = sSubKeyName;

            var outParams = mc.InvokeMethod("EnumKey", inParams, null);
            if (outParams != null && Convert.ToUInt32(outParams["ReturnValue"]) == 0)
            {
                return outParams["sNames"] as string[];
            }
            return null;
        }
        #endregion

        private AttendantProfile GetProfileInformation(string profilePath)
        {
            var attendantProfile = new AttendantProfile();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, String.Format("{0}\\{1}", _baseProfilesPath, profilePath));

            if (ReadingProfile != null)
                ReadingProfile.BeginInvoke(this, new ActionEventArgs(GetKeyValue(valueNames, "Name")), null, null);

            attendantProfile.Active = GetKeyValue(valueNames, "Active");
            attendantProfile.AddNote = GetKeyValue(valueNames, "AddNote");
            attendantProfile.AllowTransfers = GetKeyValue(valueNames, "AllowTransfers");
            attendantProfile.ANIString = GetKeyValue(valueNames, "ANIString");
            attendantProfile.AudioFile = GetKeyValue(valueNames, "AudioFile");
            attendantProfile.Default = GetKeyValue(valueNames, "Default");
            attendantProfile.Digit = GetKeyValue(valueNames, "Digit");
            attendantProfile.DirectToQueueOnce = GetKeyValue(valueNames, "DirectToQueueOnce");
            attendantProfile.DisableSpeechRec = GetKeyValue(valueNames, "DisableSpeechRec");
            attendantProfile.DNISString = GetKeyValue(valueNames, "DNISString");
            attendantProfile.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            //attendantProfile.FullNodePath = GetKeyValue(valueNames, "FullNodePath");
            attendantProfile.JumpNode = GetKeyValue(valueNames, "JumpNode");
            attendantProfile.JumpToType = GetKeyValue(valueNames, "JumpToType");
            attendantProfile.Name = GetKeyValue(valueNames, "Name");
            attendantProfile.QueueType = GetKeyValue(valueNames, "QueueType");
            attendantProfile.Reporting = GetKeyValue(valueNames, "Reporting");
            attendantProfile.Style = GetKeyValue(valueNames, "Style");
            attendantProfile.SystemActive = GetKeyValue(valueNames, "SystemActive");
            attendantProfile.SystemNode = GetKeyValue(valueNames, "SystemNode");
            attendantProfile.Timeout = GetKeyValue(valueNames, "Timeout");
            attendantProfile.Type = GetKeyValue(valueNames, "Type");
            attendantProfile.UseANI = GetKeyValue(valueNames, "UseANI");
            attendantProfile.UseDNIS = GetKeyValue(valueNames, "UseDNIS");
            attendantProfile.UseTrunk = GetKeyValue(valueNames, "UseTrunk");
            attendantProfile.Valid = GetKeyValue(valueNames, "Valid");
            attendantProfile.Warnings = GetKeyValue(valueNames, "Warnings");

            attendantProfile.ServerPath = RegistryServerPath;

            attendantProfile.IsSelected = true;
            return attendantProfile;
        }
        private ScheduleInformation GetScheduleInformation(string schedulePath)
        {
            var scheduleInformation = new ScheduleInformation();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, schedulePath);

            if (ReadingSchedule != null)
            {
                ReadingSchedule.BeginInvoke(this, new ActionEventArgs(GetKeyValue(valueNames, "Name")), null, null);
            }

            scheduleInformation.Active = GetKeyValue(valueNames, "Active");
            scheduleInformation.AddNote = GetKeyValue(valueNames, "AddNote");
            scheduleInformation.AudioFile = GetKeyValue(valueNames, "AudioFile");
            scheduleInformation.Default = GetKeyValue(valueNames, "Default");
            scheduleInformation.DelayTime = GetKeyValue(valueNames, "DelayTime");
            scheduleInformation.Digit = GetKeyValue(valueNames, "Digit");
            scheduleInformation.EndTime = GetKeyValue(valueNames, "EndTime");
            scheduleInformation.ErrorAudioFile = GetKeyValue(valueNames, "ErrorAudioFile");
            scheduleInformation.ErrorType = GetKeyValue(valueNames, "ErrorType");
            scheduleInformation.ExtensionListener = GetKeyValue(valueNames, "ExtensionListener");
            scheduleInformation.ExtensionTimeout = GetKeyValue(valueNames, "ExtensionTimeout");
            scheduleInformation.FaxListener = GetKeyValue(valueNames, "FaxListener");
            scheduleInformation.FaxQueueType = GetKeyValue(valueNames, "FaxQueueType");
            scheduleInformation.FullNodePath = schedulePath;
            scheduleInformation.JumpNode = GetKeyValue(valueNames, "JumpNode");
            scheduleInformation.JumpToType = GetKeyValue(valueNames, "JumpToType");
            scheduleInformation.Menu = GetKeyValue(valueNames, "Menu");
            scheduleInformation.MenuActions = GetKeyValue(valueNames, "MenuActions");
            scheduleInformation.MenuDigits = GetKeyValue(valueNames, "MenuDigits");
            scheduleInformation.Name = GetKeyValue(valueNames, "Name");
            scheduleInformation.OperatorProfile = GetKeyValue(valueNames, "OperatorProfile");
            scheduleInformation.QueueType = GetKeyValue(valueNames, "QueueType");
            scheduleInformation.Periodicity = GetKeyValue(valueNames, "Periodicity");
            scheduleInformation.RepeatCt = GetKeyValue(valueNames, "RepeatCt");
            scheduleInformation.Reporting = GetKeyValue(valueNames, "Reporting");
            scheduleInformation.ScheduleData = GetKeyValue(valueNames, "ScheduleData");
            scheduleInformation.ScheduleTimeZoneID = GetKeyValue(valueNames, "ScheduleTimeZoneID");
            scheduleInformation.StartTime = GetKeyValue(valueNames, "StartTime");
            scheduleInformation.StationQueue = GetKeyValue(valueNames, "StationQueue");
            scheduleInformation.SystemActive = GetKeyValue(valueNames, "SystemActive");
            scheduleInformation.SystemNode = GetKeyValue(valueNames, "SystemNode");
            scheduleInformation.Type = GetKeyValue(valueNames, "Type");
            scheduleInformation.UserQueue = GetKeyValue(valueNames, "UserQueue");
            scheduleInformation.Valid = GetKeyValue(valueNames, "Valid");
            scheduleInformation.Warnings = GetKeyValue(valueNames, "Warnings");
            scheduleInformation.WorkgroupQueue = GetKeyValue(valueNames, "WorkgroupQueue");

            return scheduleInformation;
        }

        #region Actions

        public bool HasActions(string parentPath)
        {
            return GetSubKeys(_managementClass, RegHive.HkeyLocalMachine, parentPath) != null;
        }
        public List<Action> GetChildrenActions(string parentPath)
        {
            var actionList = new List<Action>();

            //TODO Use Parallel.ForEach here
            var actions = GetSubKeys(_managementClass, RegHive.HkeyLocalMachine, parentPath);
            if (actions == null) return null;

            foreach (var actionKey in actions)
            {
                var actionPath = parentPath + "\\" + actionKey;

                var actionType = GetStringValue(_managementClass, RegHive.HkeyLocalMachine, actionPath, "Type");

                if (ReadingAction != null)
                {
                    ReadingAction.BeginInvoke(this, new ActionEventArgs(GetStringValue(_managementClass, RegHive.HkeyLocalMachine, actionPath, "Name") + " (" + actionType + ")"), null, null);
                }

                switch (actionType)
                {
                    case "Agent Transfer":
                        var agentTransferAction = GetAgentTransferInformation(actionPath);
                        agentTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(agentTransferAction);
                        break;
                    case "Audio Playback":
                        var audioPlaybackAction = GetAudioPlaybackInformation(actionPath);
                        audioPlaybackAction.ParentFullNodePath = parentPath;
                        actionList.Add(audioPlaybackAction);
                        break;
                    case "Callback Request":
                        var callbackAction = GetCallbackRequestInformation(actionPath);
                        callbackAction.ParentFullNodePath = parentPath;
                        actionList.Add(callbackAction);
                        break;
                    case "Caller Data Entry":
                        var callerDataEntryAction = GetCallerDataEntryInformation(actionPath);
                        callerDataEntryAction.ParentFullNodePath = parentPath;
                        actionList.Add(callerDataEntryAction);
                        break;
                    case "Case":
                        var caseAction = GetCaseInformation(actionPath);
                        caseAction.ParentFullNodePath = parentPath;
                        actionList.Add(caseAction);
                        break;
                    case "Complex Operation":
                        var complexOperationAction = GetComplexOperationInformation(actionPath);
                        complexOperationAction.ParentFullNodePath = parentPath;
                        actionList.Add(complexOperationAction);
                        break;
                    case "Conference":
                        var conferenceAction = GetConferenceInformation(actionPath);
                        conferenceAction.ParentFullNodePath = parentPath;
                        actionList.Add(conferenceAction);
                        break;
                    case "Dial By Name":
                        var dialByNameAction = GetDialByNameInformation(actionPath);
                        dialByNameAction.ParentFullNodePath = parentPath;
                        actionList.Add(dialByNameAction);
                        break;
                    case "DB Query":
                        var dbQueryAction = GetDBQueryInformation(actionPath);
                        dbQueryAction.ParentFullNodePath = parentPath;
                        actionList.Add(dbQueryAction);
                        break;
                    case "DB Insert":
                        var dbInsertAction = GetDBInsertInformation(actionPath);
                        dbInsertAction.ParentFullNodePath = parentPath;
                        actionList.Add(dbInsertAction);
                        break;
                    case "DB Update":
                        var dbUpdateAction = GetDBUpdateInformation(actionPath);
                        dbUpdateAction.ParentFullNodePath = parentPath;
                        actionList.Add(dbUpdateAction);
                        break;
                    case "Disconnect":
                        var disconnectAction = GetDisconnectInformation(actionPath);
                        disconnectAction.ParentFullNodePath = parentPath;
                        actionList.Add(disconnectAction);
                        break;
                    case "Extension Dial":
                        var extensionDialAction = GetExtensionDialInformation(actionPath);
                        extensionDialAction.ParentFullNodePath = parentPath;
                        actionList.Add(extensionDialAction);
                        break;
                    case "External Transfer":
                        var externalTransferAction = GetExternalTransferInformation(actionPath);
                        externalTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(externalTransferAction);
                        break;
                    case "Fax Back":
                        var faxBackAction = GetFaxBackInformation(actionPath);
                        faxBackAction.ParentFullNodePath = parentPath;
                        actionList.Add(faxBackAction);
                        break;
                    case "IAt Transfer":
                        var iAtTransferAction = GetIAtTransferInformation(actionPath);
                        iAtTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(iAtTransferAction);
                        break;
                    case "Language Selection":
                        var languageSelectionAction = GetLanguageSelectionInformation(actionPath);
                        languageSelectionAction.ParentFullNodePath = parentPath;
                        actionList.Add(languageSelectionAction);
                        break;
                    case "Logical Transfer":
                        var logicalTransferAction = GetLogicalTransferInformation(actionPath);
                        logicalTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(logicalTransferAction);
                        break;
                    case "Logging":
                        var loggingAction = GetLoggingInformation(actionPath);
                        loggingAction.ParentFullNodePath = parentPath;
                        actionList.Add(loggingAction);
                        break;
                    case "Menu":
                        var menuAction = GetMenuInformation(actionPath);
                        menuAction.ParentFullNodePath = parentPath;
                        actionList.Add(menuAction);
                        break;
                    case "Menu Transfer":
                        var menuTransferAction = GetMenuTransferInformation(actionPath);
                        menuTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(menuTransferAction);
                        break;
                    case "Operator":
                        var operatorAction = GetOperatorInformation(actionPath);
                        operatorAction.ParentFullNodePath = parentPath;
                        actionList.Add(operatorAction);
                        break;
                    case "Other Tools":
                        var otherToolsAction = GetOtherToolsInformation(actionPath);
                        otherToolsAction.ParentFullNodePath = parentPath;
                        actionList.Add(otherToolsAction);
                        break;
                    case "Play Info":
                        var playInfoAction = GetPlayInfoInformation(actionPath);
                        playInfoAction.ParentFullNodePath = parentPath;
                        actionList.Add(playInfoAction);
                        break;
                    case "Queue Audio":
                        var queueAudioAction = GetQueueAudioInformation(actionPath);
                        queueAudioAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueAudioAction);
                        break;
                    case "Queue Menu":
                        var queueMenuAction = GetQueueMenuInformation(actionPath);
                        queueMenuAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueMenuAction);
                        break;
                    case "Queue Repeat":
                        var queueRepeatAction = GetQueueRepeatInformation(actionPath);
                        queueRepeatAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueRepeatAction);
                        break;
                    case "Queue Stay":
                        var queueStayAction = GetQueueStayInformation(actionPath);
                        queueStayAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueStayAction);
                        break;
                    case "Queue Transfer":
                        var queueTransferAction = GetQueueTransferInformation(actionPath);
                        queueTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueTransferAction);
                        break;
                    case "Queue Update Skills":
                        var queueUpdateSkillsAction = GetQueueUpdateSkillsInformation(actionPath);
                        queueUpdateSkillsAction.ParentFullNodePath = parentPath;
                        actionList.Add(queueUpdateSkillsAction);
                        break;
                    case "Receive Fax":
                        var receiveFaxAction = GetReceiveFaxInformation(actionPath);
                        receiveFaxAction.ParentFullNodePath = parentPath;
                        actionList.Add(receiveFaxAction);
                        break;
                    case "Remote Access":
                        var remoteAccessAction = GetRemoteAccessInformation(actionPath);
                        remoteAccessAction.ParentFullNodePath = parentPath;
                        actionList.Add(remoteAccessAction);
                        break;
                    case "Remote Data Query":
                        var remoteDataQueryAction = GetRemoteDataQueryInformation(actionPath);
                        remoteDataQueryAction.ParentFullNodePath = parentPath;
                        actionList.Add(remoteDataQueryAction);
                        break;
                    case "Selection":
                        var selectionAction = GetSelectionInformation(actionPath);
                        selectionAction.ParentFullNodePath = parentPath;
                        actionList.Add(selectionAction);
                        break;
                    case "Set Advanced Statistic":
                        var setAdvancedStatisticAction = GetSetAdvancedStatisticInformation(actionPath);
                        setAdvancedStatisticAction.ParentFullNodePath = parentPath;
                        actionList.Add(setAdvancedStatisticAction);
                        break;
                    case "Set Attribute":
                        var setAttributeAction = GetSetAttributeInformation(actionPath);
                        setAttributeAction.ParentFullNodePath = parentPath;
                        actionList.Add(setAttributeAction);
                        break;
                    case "Subroutine Initiator":
                        var subroutineInitiatorAction = GetSubroutineInitiatorInformation(actionPath);
                        subroutineInitiatorAction.ParentFullNodePath = parentPath;
                        actionList.Add(subroutineInitiatorAction);
                        break;
                    case "Voicemail Transfer":
                        var voicemailTransferAction = GetVoicemailTransferInformation(actionPath);
                        voicemailTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(voicemailTransferAction);
                        break;
                    case "Workgroup Transfer":
                        var workgroupTransferAction = GetWorkgroupTransferInformation(actionPath);
                        workgroupTransferAction.ParentFullNodePath = parentPath;
                        actionList.Add(workgroupTransferAction);
                        break;
                }
                var subKeys = GetSubKeys(_managementClass, RegHive.HkeyLocalMachine, actionPath);

                if (subKeys != null && subKeys.Length > 0)
                {
                    // Get the action's children
                    actionList.AddRange(GetChildrenActions(actionPath));
                }
            }
            return actionList;
        }
        public Action GetAction(string path)
        {
            foreach (var attendantProfile in AttendantProfiles)
            {
                foreach (var attendantSchedule in attendantProfile.Schedules)
                {
                    foreach (var action in attendantSchedule.ActionList)
                    {
                        if (action.FullNodePath == path)
                            return action;
                    }
                }
            }
            return null;
        }

        private AgentTransferAction GetAgentTransferInformation(string actionPath)
        {
            var action = new AgentTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            //action.JumpLocation = GetFullRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            //action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            //action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.ParkInteraction = GetKeyValue(valueNames, "ParkInteraction");
            action.QueueType = GetKeyValue(valueNames, "QueueType");
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.StationQueue = GetKeyValue(valueNames, "StationQueue");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.UserQueue = GetKeyValue(valueNames, "UserQueue");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private AudioPlaybackAction GetAudioPlaybackInformation(string actionPath)
        {
            var action = new AudioPlaybackAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.AudioSource = GetKeyValue(valueNames, "AudioSource");
            action.AudioSourceTimeout = GetKeyValue(valueNames, "AudioSourceTimeout");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.DelayTime = GetKeyValue(valueNames, "DelayTime");
            action.DynamicAudio = GetKeyValue(valueNames, "DynamicAudio");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.ExternalAudioSource = GetKeyValue(valueNames, "ExternalAudioSource");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.RepeatCt = GetKeyValue(valueNames, "RepeatCt");
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private CallbackRequestAction GetCallbackRequestInformation(string actionPath)
        {
            var action = new CallbackRequestAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.CallbackNumberNode = GetKeyValue(valueNames, "CallbackNumberNode");
            action.CallbackNumberSource = GetKeyValue(valueNames, "CallbackNumberSource");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");
            action.Workgroup = GetKeyValue(valueNames, "Workgroup");

            return action;
        }
        private CallerDataEntryAction GetCallerDataEntryInformation(string actionPath)
        {
            var action = new CallerDataEntryAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.Character = GetKeyValue(valueNames, "Character");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.DDE = GetKeyValue(valueNames, "DDE");
            action.DDEAttr = GetKeyValue(valueNames, "DDEAttr");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorAudio = GetKeyValue(valueNames, "ErrorAudio");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Length = GetKeyValue(valueNames, "Length");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Termination = GetKeyValue(valueNames, "Termination");
            action.Timeout = GetKeyValue(valueNames, "Timeout");
            action.Tries = GetKeyValue(valueNames, "Tries");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.VerificationType = GetKeyValue(valueNames, "VerificationType");
            action.Verify = GetKeyValue(valueNames, "Verify");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private CaseAction GetCaseInformation(string actionPath)
        {
            var action = new CaseAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.ExpressionFinalStepText = GetKeyValue(valueNames, "ExpressionFinalStepText");
            action.ExpressionSubText1 = GetKeyValue(valueNames, "ExpressionSubText1");
            action.ExpressionText = GetKeyValue(valueNames, "ExpressionText");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Operator = GetKeyValue(valueNames, "Operator");
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.RHSData = GetKeyValue(valueNames, "RHSData");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private ComplexOperationAction GetComplexOperationInformation(string actionPath)
        {
            var action = new ComplexOperationAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private ConferenceAction GetConferenceInformation(string actionPath)
        {
            var action = new ConferenceAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private DBInsertAction GetDBInsertInformation(string actionPath)
        {
            var action = new DBInsertAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.DataSource = GetKeyValue(valueNames, "DataSource");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.Insert = GetKeyValue(valueNames, "Insert");
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.TableName = GetKeyValue(valueNames, "TableName");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");
            action.WhereColumn = GetKeyValue(valueNames, "WhereColumn");
            action.WhereDataValue = GetKeyValue(valueNames, "WhereDataValue");
            action.WhereOperator = GetKeyValue(valueNames, "WhereOperator");
            action.WhereTimeValue = GetKeyValue(valueNames, "WhereTimeValue");
            action.WhereType = GetKeyValue(valueNames, "WhereType");

            return action;
        }
        private DBQueryAction GetDBQueryInformation(string actionPath)
        {
            var action = new DBQueryAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.DataSource = GetKeyValue(valueNames, "DataSource");
            action.DDE = GetKeyValue(valueNames, "DDE");
            action.DDEAttr = GetKeyValue(valueNames, "DDEAttr");
            action.NoReturnAction = GetKeyValue(valueNames, "NoReturnAction");
            action.QueryLocation = GetKeyValue(valueNames, "QueryLocation");
            action.ReadDataType = GetKeyValue(valueNames, "ReadDataType");
            action.ReadToCaller = GetKeyValue(valueNames, "ReadToCaller");
            action.ReturnColumn = GetKeyValue(valueNames, "ReturnColumn");
            action.WhereAttributeValue = GetKeyValue(valueNames, "WhereAttributeValue");
            action.WhereColumn = GetKeyValue(valueNames, "WhereColumn");
            action.WhereDataValue = GetKeyValue(valueNames, "WhereDataValue");
            action.WhereOperator = GetKeyValue(valueNames, "WhereOperator");
            action.WhereTimeValue = GetKeyValue(valueNames, "WhereTimeValue");
            action.WhereType = GetKeyValue(valueNames, "WhereType");

            return action;
        }
        private DBUpdateAction GetDBUpdateInformation(string actionPath)
        {
            var action = new DBUpdateAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.DataSource = GetKeyValue(valueNames, "DataSource");
            action.TableName = GetKeyValue(valueNames, "TableName");
            action.UpdateColumn = GetKeyValue(valueNames, "UpdateColumn");
            action.UpdateDate = GetKeyValue(valueNames, "UpdateDate");
            action.UpdateString = GetKeyValue(valueNames, "UpdateString");
            action.UpdateTime = GetKeyValue(valueNames, "UpdateTime");
            action.UpdateType = GetKeyValue(valueNames, "UpdateType");
            action.WhereColumn = GetKeyValue(valueNames, "WhereColumn");
            action.WhereDataValue = GetKeyValue(valueNames, "WhereDataValue");
            action.WhereOperator = GetKeyValue(valueNames, "WhereOperator");
            action.WhereStringValue = GetKeyValue(valueNames, "WhereStringValue");
            action.WhereTimeValue = GetKeyValue(valueNames, "WhereTimeValue");
            action.WhereType = GetKeyValue(valueNames, "WhereType");

            return action;
        }
        private DialByNameAction GetDialByNameInformation(string actionPath)
        {
            var action = new DialByNameAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Chars = GetKeyValue(valueNames, "Chars");
            action.LimitedSearch = GetKeyValue(valueNames, "LimitedSearch");
            action.MatchBy = GetKeyValue(valueNames, "MatchBy");
            action.MatchStyle = GetKeyValue(valueNames, "MatchStyle");

            return action;
        }
        private DisconnectAction GetDisconnectInformation(string actionPath)
        {
            var action = new DisconnectAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AudioFile = GetKeyValue(valueNames, "AudioFile");

            return action;
        }
        private ExtensionDialAction GetExtensionDialInformation(string actionPath)
        {
            var action = new ExtensionDialAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.LimitedSearch = GetKeyValue(valueNames, "LimitedSearch");
            action.RepeatCt = GetKeyValue(valueNames, "RepeatCt");
            action.Timeout = GetKeyValue(valueNames, "Timeout");

            return action;
        }
        private ExternalTransferAction GetExternalTransferInformation(string actionPath)
        {
            var action = new ExternalTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Number = GetKeyValue(valueNames, "Number");
            action.ReleaseOnTransfer = GetKeyValue(valueNames, "ReleaseOnTransfer");
            action.TransferType = GetKeyValue(valueNames, "TransferType");

            return action;
        }
        private FaxBackAction GetFaxBackInformation(string actionPath)
        {
            var action = new FaxBackAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.FaxNum = GetKeyValue(valueNames, "FaxNum");
            action.File = GetKeyValue(valueNames, "File");
            action.SendOnThisCall = GetKeyValue(valueNames, "SendOnThisCall");

            return action;
        }
        private IAtTransferAction GetIAtTransferInformation(string actionPath)
        {
            var action = new IAtTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.TransferConfig = GetKeyValue(valueNames, "TransferConfig");
            action.TransferSel = GetKeyValue(valueNames, "TransferSel");
            action.TransferType = GetKeyValue(valueNames, "TransferType");

            return action;
        }
        private LanguageSelectionAction GetLanguageSelectionInformation(string actionPath)
        {
            var action = new LanguageSelectionAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Language = GetKeyValue(valueNames, "Language");
            action.LanguageName = GetKeyValue(valueNames, "LanguageName");

            return action;
        }
        private LoggingAction GetLoggingInformation(string actionPath)
        {
            var action = new LoggingAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.CallerDataNode = GetKeyValue(valueNames, "CallerDataNode");
            action.DBDataNode = GetKeyValue(valueNames, "DBDataNode");
            action.LogData = GetKeyValue(valueNames, "LogData");
            action.LoggingAttr = GetKeyValue(valueNames, "LoggingAttr");
            action.LoggingText = GetKeyValue(valueNames, "LoggingText");

            return action;
        }
        private LogicalTransferAction GetLogicalTransferInformation(string actionPath)
        {
            var action = new LogicalTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.CallOperator = GetKeyValue(valueNames, "CallOperator");
            action.CallTrunkOperator = GetKeyValue(valueNames, "CallTrunkOperator");
            action.DBOperator = GetKeyValue(valueNames, "DBOperator");
            action.DurationOperator = GetKeyValue(valueNames, "DurationOperator");
            action.ExpType = GetKeyValue(valueNames, "ExpType");
            action.Initialized = GetKeyValue(valueNames, "Initialized");
            action.LHSCallDataAttrType = GetKeyValue(valueNames, "LHSCallDataAttrType");
            action.LHSCallDataRoutingAttr = GetKeyValue(valueNames, "LHSCallDataRoutingAttr");
            action.LHSWorkgroupAttr = GetKeyValue(valueNames, "LHSWorkgroupAttr");
            action.OnFalse = FormatRegistryPath(GetKeyValue(valueNames, "OnFalse"));
            action.OnTrue = FormatRegistryPath(GetKeyValue(valueNames, "OnTrue"));
            action.RHSCallDataValueType = GetKeyValue(valueNames, "RHSCallDataValueType");
            action.RHSCallLength = GetKeyValue(valueNames, "RHSCallLength");
            action.RHSCallLengthUnits = GetKeyValue(valueNames, "RHSCallLengthUnits");
            action.RHSDBType = GetKeyValue(valueNames, "RHSDBType");
            action.RHSDBValueType = GetKeyValue(valueNames, "RHSDBValueType");
            action.RHSDateTime = GetKeyValue(valueNames, "RHSDateTime");
            action.RHSIsDate = GetKeyValue(valueNames, "RHSIsDate");
            action.RHSIsTime = GetKeyValue(valueNames, "RHSIsTime");
            action.RHSWgDurType = GetKeyValue(valueNames, "RHSWgDurType");
            action.RHSWgDurValue = GetKeyValue(valueNames, "RHSWgDurValue");
            action.RHSWgNumType = GetKeyValue(valueNames, "RHSWgNumType");
            action.RHSWgTimeUnits = GetKeyValue(valueNames, "RHSWgTimeUnits");
            action.TimeOperator = GetKeyValue(valueNames, "TimeOperator");
            action.UserOperator = GetKeyValue(valueNames, "UserOperator");
            action.WgDurOperator = GetKeyValue(valueNames, "WgDurOperator");
            action.WgNumOperator = GetKeyValue(valueNames, "WgNumOperator");

            return action;
        }
        private MenuAction GetMenuInformation(string actionPath)
        {
            var action = new MenuAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.DelayTime = GetKeyValue(valueNames, "DelayTime");
            action.ExtensionListener = GetKeyValue(valueNames, "ExtensionListener");
            action.ExtensionTimeout = GetKeyValue(valueNames, "ExtensionTimeout");
            action.FaxListener = GetKeyValue(valueNames, "FaxListener");
            action.FaxQueueType = GetKeyValue(valueNames, "FaxQueueType");
            action.MenuActions = GetKeyValue(valueNames, "MenuActions");
            action.MenuDigits = GetKeyValue(valueNames, "MenuDigits");
            action.RepeatCt = GetKeyValue(valueNames, "RepeatCt");

            return action;
        }
        private MenuTransferAction GetMenuTransferInformation(string actionPath)
        {
            var action = new MenuTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Menu = FormatRegistryPath(GetKeyValue(valueNames, "Menu"));

            return action;
        }
        private OperatorAction GetOperatorInformation(string actionPath)
        {
            var action = new OperatorAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private OtherToolsAction GetOtherToolsInformation(string actionPath)
        {
            var action = new OtherToolsAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Tool = GetKeyValue(valueNames, "Tool");
            action.ToolName = GetKeyValue(valueNames, "ToolName");

            return action;
        }
        private PlayInfoAction GetPlayInfoInformation(string actionPath)
        {
            var action = new PlayInfoAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AttributeNames = GetKeyValue(valueNames, "AttributeNames");
            action.AudioFileNames = GetKeyValue(valueNames, "AudioFileNames");
            action.CustomFormatAttributeValues = GetKeyValue(valueNames, "CustomFormatAttributeValues");
            action.CustomFormatAudioFileNames = GetKeyValue(valueNames, "CustomFormatAudioFileNames");
            action.DataFormats = GetKeyValue(valueNames, "DataFormats");
            action.DelayTime = GetKeyValue(valueNames, "DelayTime");
            action.ImmediateNode = GetKeyValue(valueNames, "ImmediateNode");
            action.RepeatCt = GetKeyValue(valueNames, "RepeatCt");

            return action;
        }
        private QueueAudioAction GetQueueAudioInformation(string actionPath)
        {
            var action = new QueueAudioAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.Continuous = GetKeyValue(valueNames, "Continuous");
            action.Style = GetKeyValue(valueNames, "Style");
            action.Timeout = GetKeyValue(valueNames, "Timeout");

            return action;
        }
        private QueueMenuAction GetQueueMenuInformation(string actionPath)
        {
            var action = new QueueMenuAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AudioFile = GetKeyValue(valueNames, "AudioFile");
            action.Timeout = GetKeyValue(valueNames, "Timeout");

            return action;
        }
        private QueueRepeatAction GetQueueRepeatInformation(string actionPath)
        {
            var action = new QueueRepeatAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.RepeatCt = GetKeyValue(valueNames, "RepeatCt");
            action.RepeatNode = GetKeyValue(valueNames, "RepeatNode");

            return action;
        }
        private QueueStayAction GetQueueStayInformation(string actionPath)
        {
            var action = new QueueStayAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            return action;
        }
        private QueueTransferAction GetQueueTransferInformation(string actionPath)
        {
            var action = new QueueTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Workgroup = GetKeyValue(valueNames, "Workgroup");

            return action;
        }
        private QueueUpdateSkillsAction GetQueueUpdateSkillsInformation(string actionPath)
        {
            var action = new QueueUpdateSkillsAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.DynamicSkillsToAdd = GetKeyValue(valueNames, "DynamicSkillsToAdd");
            action.DynamicSkillsToRemove = GetKeyValue(valueNames, "DynamicSkillsToRemove");

            return action;
        }
        private ReceiveFaxAction GetReceiveFaxInformation(string actionPath)
        {
            var action = new ReceiveFaxAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.QueueType = GetKeyValue(valueNames, "QueueType");
            action.UserQueue = GetKeyValue(valueNames, "UserQueue");
            action.WorkgroupQueue = GetKeyValue(valueNames, "WorkgroupQueue");

            return action;
        }
        private RemoteAccessAction GetRemoteAccessInformation(string actionPath)
        {
            var action = new RemoteAccessAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Code = GetKeyValue(valueNames, "Code");

            return action;
        }
        private RemoteDataQueryAction GetRemoteDataQueryInformation(string actionPath)
        {
            var action = new RemoteDataQueryAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Certificate = GetKeyValue(valueNames, "Certificate");
            action.ConnectTimeout = GetKeyValue(valueNames, "ConnectTimeout");
            action.IgnoreCertMismatch = GetKeyValue(valueNames, "IgnoreCertMismatch");
            action.IgnoreInvalidCert = GetKeyValue(valueNames, "IgnoreInvalidCert");
            action.IgnoreUnknownCA = GetKeyValue(valueNames, "IgnoreUnknownCA");
            action.IgnoreWrongCert = GetKeyValue(valueNames, "IgnoreWrongCert");
            action.InputXML = GetKeyValue(valueNames, "InputXML");
            action.MethodOutputAttributeNames = GetKeyValue(valueNames, "MethodOutputAttributeNames");
            action.MethodOutputXMLNames = GetKeyValue(valueNames, "MethodOutputXMLNames");
            action.Password = GetKeyValue(valueNames, "Password");
            action.RequestTimeout = GetKeyValue(valueNames, "RequestTimeout");
            action.SSLEnabled = GetKeyValue(valueNames, "SSLEnabled");
            action.Timeout = GetKeyValue(valueNames, "Timeout");
            action.URL = GetKeyValue(valueNames, "URL");
            action.Username = GetKeyValue(valueNames, "Username");

            return action;
        }
        private SelectionAction GetSelectionInformation(string actionPath)
        {
            var action = new SelectionAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.ExpressionFinalStepText = GetKeyValue(valueNames, "ExpressionFinalStepText");
            action.ExpressionSubText1 = GetKeyValue(valueNames, "ExpressionSubText1");
            action.ExpressionText = GetKeyValue(valueNames, "ExpressionText");
            action.ExpressionType = GetKeyValue(valueNames, "ExpressionType");
            action.LHSData = GetKeyValue(valueNames, "LHSData");
            action.SelectionChildren = GetKeyValue(valueNames, "SelectionChildren");

            return action;
        }
        private SetAdvancedStatisticAction GetSetAdvancedStatisticInformation(string actionPath)
        {
            var action = new SetAdvancedStatisticAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.StatType = GetKeyValue(valueNames, "StatType");
            action.StatValue = GetKeyValue(valueNames, "StatValue");

            return action;
        }
        private SetAttributeAction GetSetAttributeInformation(string actionPath)
        {
            var action = new SetAttributeAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Attribute = GetKeyValue(valueNames, "Attribute");
            action.Value = GetKeyValue(valueNames, "Value");

            return action;
        }
        private SubroutineInitiatorAction GetSubroutineInitiatorInformation(string actionPath)
        {
            var action = new SubroutineInitiatorAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.Subroutine = GetKeyValue(valueNames, "Subroutine");

            return action;
        }
        private VoicemailTransferAction GetVoicemailTransferInformation(string actionPath)
        {
            var action = new VoicemailTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            action.JumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            action.NextLocation = FormatRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AttempIATThreadedTransfer = GetKeyValue(valueNames, "AttempIATThreadedTransfer");
            action.EscapeCharacter = GetKeyValue(valueNames, "EscapeCharacter");
            action.GroupType = GetKeyValue(valueNames, "GroupType");
            action.IATThreadedTransferTimeout = GetKeyValue(valueNames, "IATThreadedTransferTimeout");
            action.IsACD = GetKeyValue(valueNames, "IsACD");
            action.Priority = GetKeyValue(valueNames, "Priority");
            action.PriorityType = GetKeyValue(valueNames, "PriorityType");
            action.Timeout = GetKeyValue(valueNames, "Timeout");
            action.TimeoutJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "TimeoutJumpLocation"));
            action.TimeoutJumpToType = GetKeyValue(valueNames, "TimeoutJumpToType");
            action.TimeoutStyle = GetKeyValue(valueNames, "TimeoutStyle");
            action.WorkgroupQueue = GetKeyValue(valueNames, "WorkgroupQueue");

            return action;
        }
        private WorkgroupTransferAction GetWorkgroupTransferInformation(string actionPath)
        {
            var action = new WorkgroupTransferAction();

            var valueNames = GetValues(_managementClass, RegHive.HkeyLocalMachine, actionPath);

            action.Active = GetKeyValue(valueNames, "Active");
            action.AddNote = GetKeyValue(valueNames, "AddNote");
            action.ChildIndex = GetKeyValue(valueNames, "ChildIndex");
            action.Default = GetKeyValue(valueNames, "Default");
            action.Digit = GetKeyValue(valueNames, "Digit");
            action.ErrorJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "ErrorJumpLocation"));
            action.ErrorType = GetKeyValue(valueNames, "ErrorType");
            action.FullNodePath = FormatRegistryPath(GetKeyValue(valueNames, "FullNodePath"));
            //action.JumpLocation = GetFullRegistryPath(GetKeyValue(valueNames, "JumpLocation"));
            //action.JumpNode = GetKeyValue(valueNames, "JumpNode");
            //action.JumpToType = GetKeyValue(valueNames, "JumpToType");
            action.Name = GetKeyValue(valueNames, "Name");
            //action.NextLocation = GetFullRegistryPath(GetKeyValue(valueNames, "NextLocation"));
            action.Reporting = GetKeyValue(valueNames, "Reporting");
            action.SystemActive = GetKeyValue(valueNames, "SystemActive");
            action.SystemNode = GetKeyValue(valueNames, "SystemNode");
            action.Type = GetKeyValue(valueNames, "Type");
            action.Valid = GetKeyValue(valueNames, "Valid");
            action.Warnings = GetKeyValue(valueNames, "Warnings");

            action.AttempIATThreadedTransfer = GetKeyValue(valueNames, "AttempIATThreadedTransfer");
            action.EscapeCharacter = GetKeyValue(valueNames, "EscapeCharacter");
            action.GroupType = GetKeyValue(valueNames, "GroupType");
            action.IATThreadedTransferTimeout = GetKeyValue(valueNames, "IATThreadedTransferTimeout");
            action.IsACD = GetKeyValue(valueNames, "IsACD");
            action.Priority = GetKeyValue(valueNames, "Priority");
            action.PriorityType = GetKeyValue(valueNames, "PriorityType");
            action.Timeout = GetKeyValue(valueNames, "Timeout");
            action.TimeoutJumpLocation = FormatRegistryPath(GetKeyValue(valueNames, "TimeoutJumpLocation"));
            //action.TimeoutJumpToType = GetKeyValue(valueNames, "TimeoutJumpToType");
            action.TimeoutStyle = GetKeyValue(valueNames, "TimeoutStyle");
            action.Workgroup = GetKeyValue(valueNames, "Workgroup");

            return action;
        }

        #endregion
    }
}
