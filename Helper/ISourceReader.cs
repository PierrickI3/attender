﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using static Helper.WMIHelper;

namespace Helper
{
    public interface ISourceReader
    {
        List<AttendantProfile> AttendantProfiles
        {
            get;
            set;
        }

        string RegistryServerPath
        {
            get;
        }

        string FormatRegistryPath(string path);

        Action GetAction(string path);

        bool GetAttendantInformation(IEnumerable<AttendantProfile> attendantProfiles);

        IEnumerable<AttendantProfile> GetAttendantProfiles(IEnumerable<AttendantProfile> attendantProfiles);

        IEnumerable<ScheduleInformation> GetAttendantSchedules(string profileKey);

        bool HasActions(string parentPath);

        void InitRegistryPaths();

        void LoadFrom(string filename);

        void SaveTo(string filename);

        event ReadingActionEventHandler ReadingAction;

        event ReadingProfileEventHandler ReadingProfile;

        event ReadingScheduleEventHandler ReadingSchedule;
    }
}