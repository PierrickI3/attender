﻿using System;

namespace Helper
{
    public delegate void ReadingActionEventHandler(object sender, ActionEventArgs e);
}