﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Helper
{
    public class RegFileReader : ISourceReader
    {
        private const string BaseRegistryPath32 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Interactive Intelligence\\EIC\\Directory Services\\Root";

        private const string BaseRegistryPath64 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Interactive Intelligence\\EIC\\Directory Services\\Root";

        private string _currentBaseRegistryPath = string.Empty;

        private string _registryServerPath;

        private readonly RegFileObject _regFileObject;

        private string _regFilePath;

        private string _activeAttendantConfig;

        private string _baseProfilesPath;

        private List<AttendantProfile> _attendantProfiles = new List<AttendantProfile>();

        public Dictionary<string, Dictionary<string, RegValueObject>> AttendantRegValues = new Dictionary<string, Dictionary<string, RegValueObject>>();

        public List<AttendantProfile> AttendantProfiles
        {
            get
            {
                return this._attendantProfiles;
            }
            set
            {
                this._attendantProfiles = value;
            }
        }

        public string RegistryServerPath
        {
            get
            {
                if (string.IsNullOrEmpty(this._registryServerPath))
                {
                    string stringValue = this.GetStringValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Interactive Intelligence\\EIC\\Directory Services\\Root", "SERVER");
                    if (!string.IsNullOrEmpty(stringValue))
                    {
                        this._registryServerPath = string.Format("{0}{1}", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Interactive Intelligence\\EIC\\Directory Services\\Root", stringValue);
                        this._currentBaseRegistryPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Interactive Intelligence\\EIC\\Directory Services\\Root";
                        return this._registryServerPath;
                    }
                    stringValue = this.GetStringValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Interactive Intelligence\\EIC\\Directory Services\\Root", "SERVER");
                    if (!string.IsNullOrEmpty(stringValue))
                    {
                        this._registryServerPath = string.Format("{0}{1}", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Interactive Intelligence\\EIC\\Directory Services\\Root", stringValue);
                        this._currentBaseRegistryPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Interactive Intelligence\\EIC\\Directory Services\\Root";
                        return this._registryServerPath;
                    }
                    MessageBox.Show("Failed to find SERVER path. Cannot continue.");
                }
                return this._registryServerPath;
            }
        }

        public RegFileReader()
        {
        }

        public RegFileReader(string regFilePath)
        {
            _regFilePath = regFilePath;
            _regFileObject = new RegFileObject(this._regFilePath);
            AttendantRegValues = this._regFileObject.RegValues;
            InitRegistryPaths();
        }

        public string FormatRegistryPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }
            if (!path.StartsWith("SOFTWARE"))
            {
                path = string.Format("{0}{1}", this._currentBaseRegistryPath, path);
            }
            path = path.Replace("${SERVER}", this.GetStringValue(this._currentBaseRegistryPath, "SERVER"));
            path = path.TrimEnd(new char[] { '\\' });
            return path;
        }

        public Action GetAction(string path)
        {
            Action action;
            List<AttendantProfile>.Enumerator enumerator = this._attendantProfiles.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    List<ScheduleInformation>.Enumerator enumerator1 = enumerator.Current.Schedules.GetEnumerator();
                    try
                    {
                        while (enumerator1.MoveNext())
                        {
                            List<Action>.Enumerator enumerator2 = enumerator1.Current.ActionList.GetEnumerator();
                            try
                            {
                                while (enumerator2.MoveNext())
                                {
                                    Action current = enumerator2.Current;
                                    if (current.FullNodePath != path)
                                    {
                                        continue;
                                    }
                                    action = current;
                                    return action;
                                }
                            }
                            finally
                            {
                                ((IDisposable)enumerator2).Dispose();
                            }
                        }
                    }
                    finally
                    {
                        ((IDisposable)enumerator1).Dispose();
                    }
                }
                return null;
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
        }

        public List<Action> GetActionInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            string key = valueNames.Key;
            if (this.ReadingAction != null)
            {
                this.ReadingAction.BeginInvoke(this, new ActionEventArgs(string.Concat(this.GetKeyValue(valueNames, "Name"), " (", this.GetKeyValue(valueNames, "Type"), ")")), null, null);
            }
            List<Action> actions = new List<Action>();
            string keyValue = this.GetKeyValue(valueNames, "ParentPath");
            string str = this.GetKeyValue(valueNames, "Type");
            string str1 = str;
            if (str != null)
            {
                switch (str1)
                {
                    case "Agent Transfer":
                        {
                            AgentTransferAction agentTransferInformation = this.GetAgentTransferInformation(valueNames);
                            agentTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(agentTransferInformation);
                            break;
                        }
                    case "Audio Playback":
                        {
                            AudioPlaybackAction audioPlaybackInformation = this.GetAudioPlaybackInformation(valueNames);
                            audioPlaybackInformation.ParentFullNodePath = keyValue;
                            actions.Add(audioPlaybackInformation);
                            break;
                        }
                    case "Callback Request":
                        {
                            CallbackRequestAction callbackRequestInformation = this.GetCallbackRequestInformation(valueNames);
                            callbackRequestInformation.ParentFullNodePath = keyValue;
                            actions.Add(callbackRequestInformation);
                            break;
                        }
                    case "Caller Data Entry":
                        {
                            CallerDataEntryAction callerDataEntryInformation = this.GetCallerDataEntryInformation(valueNames);
                            callerDataEntryInformation.ParentFullNodePath = keyValue;
                            actions.Add(callerDataEntryInformation);
                            break;
                        }
                    case "Case":
                        {
                            CaseAction caseInformation = this.GetCaseInformation(valueNames);
                            caseInformation.ParentFullNodePath = keyValue;
                            actions.Add(caseInformation);
                            break;
                        }
                    case "Complex Operation":
                        {
                            ComplexOperationAction complexOperationInformation = this.GetComplexOperationInformation(valueNames);
                            complexOperationInformation.ParentFullNodePath = keyValue;
                            actions.Add(complexOperationInformation);
                            break;
                        }
                    case "Conference":
                        {
                            ConferenceAction conferenceInformation = this.GetConferenceInformation(valueNames);
                            conferenceInformation.ParentFullNodePath = keyValue;
                            actions.Add(conferenceInformation);
                            break;
                        }
                    case "Dial By Name":
                        {
                            DialByNameAction dialByNameInformation = this.GetDialByNameInformation(valueNames);
                            dialByNameInformation.ParentFullNodePath = keyValue;
                            actions.Add(dialByNameInformation);
                            break;
                        }
                    case "DB Query":
                        {
                            DBQueryAction dBQueryInformation = this.GetDBQueryInformation(valueNames);
                            dBQueryInformation.ParentFullNodePath = keyValue;
                            actions.Add(dBQueryInformation);
                            break;
                        }
                    case "DB Insert":
                        {
                            DBInsertAction dBInsertInformation = this.GetDBInsertInformation(valueNames);
                            dBInsertInformation.ParentFullNodePath = keyValue;
                            actions.Add(dBInsertInformation);
                            break;
                        }
                    case "DB Update":
                        {
                            DBUpdateAction dBUpdateInformation = this.GetDBUpdateInformation(valueNames);
                            dBUpdateInformation.ParentFullNodePath = keyValue;
                            actions.Add(dBUpdateInformation);
                            break;
                        }
                    case "Disconnect":
                        {
                            DisconnectAction disconnectInformation = this.GetDisconnectInformation(valueNames);
                            disconnectInformation.ParentFullNodePath = keyValue;
                            actions.Add(disconnectInformation);
                            break;
                        }
                    case "Extension Dial":
                        {
                            ExtensionDialAction extensionDialInformation = this.GetExtensionDialInformation(valueNames);
                            extensionDialInformation.ParentFullNodePath = keyValue;
                            actions.Add(extensionDialInformation);
                            break;
                        }
                    case "External Transfer":
                        {
                            ExternalTransferAction externalTransferInformation = this.GetExternalTransferInformation(valueNames);
                            externalTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(externalTransferInformation);
                            break;
                        }
                    case "Fax Back":
                        {
                            FaxBackAction faxBackInformation = this.GetFaxBackInformation(valueNames);
                            faxBackInformation.ParentFullNodePath = keyValue;
                            actions.Add(faxBackInformation);
                            break;
                        }
                    case "IAt Transfer":
                        {
                            IAtTransferAction atTransferInformation = this.GetIAtTransferInformation(valueNames);
                            atTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(atTransferInformation);
                            break;
                        }
                    case "Language Selection":
                        {
                            LanguageSelectionAction languageSelectionInformation = this.GetLanguageSelectionInformation(valueNames);
                            languageSelectionInformation.ParentFullNodePath = keyValue;
                            actions.Add(languageSelectionInformation);
                            break;
                        }
                    case "Logical Transfer":
                        {
                            LogicalTransferAction logicalTransferInformation = this.GetLogicalTransferInformation(valueNames);
                            logicalTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(logicalTransferInformation);
                            break;
                        }
                    case "Logging":
                        {
                            LoggingAction loggingInformation = this.GetLoggingInformation(valueNames);
                            loggingInformation.ParentFullNodePath = keyValue;
                            actions.Add(loggingInformation);
                            break;
                        }
                    case "Menu":
                        {
                            MenuAction menuInformation = this.GetMenuInformation(valueNames);
                            menuInformation.ParentFullNodePath = keyValue;
                            actions.Add(menuInformation);
                            break;
                        }
                    case "Menu Transfer":
                        {
                            MenuTransferAction menuTransferInformation = this.GetMenuTransferInformation(valueNames);
                            menuTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(menuTransferInformation);
                            break;
                        }
                    case "Operator":
                        {
                            OperatorAction operatorInformation = this.GetOperatorInformation(valueNames);
                            operatorInformation.ParentFullNodePath = keyValue;
                            actions.Add(operatorInformation);
                            break;
                        }
                    case "Other Tools":
                        {
                            OtherToolsAction otherToolsInformation = this.GetOtherToolsInformation(valueNames);
                            otherToolsInformation.ParentFullNodePath = keyValue;
                            actions.Add(otherToolsInformation);
                            break;
                        }
                    case "Play Info":
                        {
                            PlayInfoAction playInfoInformation = this.GetPlayInfoInformation(valueNames);
                            playInfoInformation.ParentFullNodePath = keyValue;
                            actions.Add(playInfoInformation);
                            break;
                        }
                    case "Queue Audio":
                        {
                            QueueAudioAction queueAudioInformation = this.GetQueueAudioInformation(valueNames);
                            queueAudioInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueAudioInformation);
                            break;
                        }
                    case "Queue Menu":
                        {
                            QueueMenuAction queueMenuInformation = this.GetQueueMenuInformation(valueNames);
                            queueMenuInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueMenuInformation);
                            break;
                        }
                    case "Queue Repeat":
                        {
                            QueueRepeatAction queueRepeatInformation = this.GetQueueRepeatInformation(valueNames);
                            queueRepeatInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueRepeatInformation);
                            break;
                        }
                    case "Queue Stay":
                        {
                            QueueStayAction queueStayInformation = this.GetQueueStayInformation(valueNames);
                            queueStayInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueStayInformation);
                            break;
                        }
                    case "Queue Transfer":
                        {
                            QueueTransferAction queueTransferInformation = this.GetQueueTransferInformation(valueNames);
                            queueTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueTransferInformation);
                            break;
                        }
                    case "Queue Update Skills":
                        {
                            QueueUpdateSkillsAction queueUpdateSkillsInformation = this.GetQueueUpdateSkillsInformation(valueNames);
                            queueUpdateSkillsInformation.ParentFullNodePath = keyValue;
                            actions.Add(queueUpdateSkillsInformation);
                            break;
                        }
                    case "Receive Fax":
                        {
                            ReceiveFaxAction receiveFaxInformation = this.GetReceiveFaxInformation(valueNames);
                            receiveFaxInformation.ParentFullNodePath = keyValue;
                            actions.Add(receiveFaxInformation);
                            break;
                        }
                    case "Remote Access":
                        {
                            RemoteAccessAction remoteAccessInformation = this.GetRemoteAccessInformation(valueNames);
                            remoteAccessInformation.ParentFullNodePath = keyValue;
                            actions.Add(remoteAccessInformation);
                            break;
                        }
                    case "Remote Data Query":
                        {
                            RemoteDataQueryAction remoteDataQueryInformation = this.GetRemoteDataQueryInformation(valueNames);
                            remoteDataQueryInformation.ParentFullNodePath = keyValue;
                            actions.Add(remoteDataQueryInformation);
                            break;
                        }
                    case "Selection":
                        {
                            SelectionAction selectionInformation = this.GetSelectionInformation(valueNames);
                            selectionInformation.ParentFullNodePath = keyValue;
                            actions.Add(selectionInformation);
                            break;
                        }
                    case "Set Advanced Statistic":
                        {
                            SetAdvancedStatisticAction setAdvancedStatisticInformation = this.GetSetAdvancedStatisticInformation(valueNames);
                            setAdvancedStatisticInformation.ParentFullNodePath = keyValue;
                            actions.Add(setAdvancedStatisticInformation);
                            break;
                        }
                    case "Set Attribute":
                        {
                            SetAttributeAction setAttributeInformation = this.GetSetAttributeInformation(valueNames);
                            setAttributeInformation.ParentFullNodePath = keyValue;
                            actions.Add(setAttributeInformation);
                            break;
                        }
                    case "Subroutine Initiator":
                        {
                            SubroutineInitiatorAction subroutineInitiatorInformation = this.GetSubroutineInitiatorInformation(valueNames);
                            subroutineInitiatorInformation.ParentFullNodePath = keyValue;
                            actions.Add(subroutineInitiatorInformation);
                            break;
                        }
                    case "Voicemail Transfer":
                        {
                            VoicemailTransferAction voicemailTransferInformation = this.GetVoicemailTransferInformation(valueNames);
                            voicemailTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(voicemailTransferInformation);
                            break;
                        }
                    case "Workgroup Transfer":
                        {
                            WorkgroupTransferAction workgroupTransferInformation = this.GetWorkgroupTransferInformation(valueNames);
                            workgroupTransferInformation.ParentFullNodePath = keyValue;
                            actions.Add(workgroupTransferInformation);
                            break;
                        }
                    case "Screen Pop":
                        {
                            ScreenPopAction screenPopInformation = this.GetScreenPopInformation(valueNames);
                            screenPopInformation.ParentFullNodePath = keyValue;
                            actions.Add(screenPopInformation);
                            break;
                        }
                }
            }
            IEnumerable<KeyValuePair<string, Dictionary<string, RegValueObject>>> subKeys = this.GetSubKeys(key);
            if (subKeys != null && subKeys.Any<KeyValuePair<string, Dictionary<string, RegValueObject>>>())
            {
                Parallel.ForEach<KeyValuePair<string, Dictionary<string, RegValueObject>>>(subKeys, (KeyValuePair<string, Dictionary<string, RegValueObject>> subAction) => actions.AddRange(this.GetActionInformation(subAction)));
            }
            return actions;
        }

        private AgentTransferAction GetAgentTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            AgentTransferAction agentTransferAction = new AgentTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                ParkInteraction = this.GetKeyValue(valueNames, "ParkInteraction"),
                QueueType = this.GetKeyValue(valueNames, "QueueType"),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                StationQueue = this.GetKeyValue(valueNames, "StationQueue"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                UserQueue = this.GetKeyValue(valueNames, "UserQueue"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return agentTransferAction;
        }

        public bool GetAttendantInformation(IEnumerable<AttendantProfile> attendantProfiles)
        {
            bool flag;
            try
            {
                Parallel.ForEach<AttendantProfile>(attendantProfiles, (AttendantProfile attendantProfile) => {
                    Parallel.ForEach<ScheduleInformation>(this.GetAttendantSchedules(attendantProfile.FullNodePath), (ScheduleInformation attendantSchedule) => {
                        attendantSchedule.ActionList = this.GetChildrenActions(attendantSchedule.FullNodePath);
                        attendantProfile.Schedules.Add(attendantSchedule);
                    });
                    this._attendantProfiles.Add(attendantProfile);
                });
                flag = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                return false;
            }
            return flag;
        }

        public IEnumerable<AttendantProfile> GetAttendantProfiles(IEnumerable<AttendantProfile> attendantProfiles)
        {
            List<AttendantProfile> attendantProfiles1 = new List<AttendantProfile>();
            Parallel.ForEach(GetSubKeys(_baseProfilesPath), (KeyValuePair<string, Dictionary<string, RegValueObject>> attendantProfile) => attendantProfiles1.Add(GetProfileInformation(attendantProfile)));
            return attendantProfiles1;
        }

        public IEnumerable<ScheduleInformation> GetAttendantSchedules(string profileKey)
        {
            IEnumerable<ScheduleInformation> scheduleInformations;
            try
            {
                List<ScheduleInformation> scheduleInformations1 = new List<ScheduleInformation>();
                Parallel.ForEach<KeyValuePair<string, Dictionary<string, RegValueObject>>>(this.GetSubKeys(profileKey), (KeyValuePair<string, Dictionary<string, RegValueObject>> attendantSchedule) => scheduleInformations1.Add(this.GetScheduleInformation(attendantSchedule)));
                scheduleInformations = scheduleInformations1;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                return null;
            }
            return scheduleInformations;
        }

        private AudioPlaybackAction GetAudioPlaybackInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            AudioPlaybackAction audioPlaybackAction = new AudioPlaybackAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                AudioSource = this.GetKeyValue(valueNames, "AudioSource"),
                AudioSourceTimeout = this.GetKeyValue(valueNames, "AudioSourceTimeout"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                DelayTime = this.GetKeyValue(valueNames, "DelayTime"),
                DynamicAudio = this.GetKeyValue(valueNames, "DynamicAudio"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                ExternalAudioSource = this.GetKeyValue(valueNames, "ExternalAudioSource"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                RepeatCt = this.GetKeyValue(valueNames, "RepeatCt"),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return audioPlaybackAction;
        }

        private CallbackRequestAction GetCallbackRequestInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            CallbackRequestAction callbackRequestAction = new CallbackRequestAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                CallbackNumberNode = this.GetKeyValue(valueNames, "CallbackNumberNode"),
                CallbackNumberSource = this.GetKeyValue(valueNames, "CallbackNumberSource"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Workgroup = this.GetKeyValue(valueNames, "Workgroup")
            };
            return callbackRequestAction;
        }

        private CallerDataEntryAction GetCallerDataEntryInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            CallerDataEntryAction callerDataEntryAction = new CallerDataEntryAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                Character = this.GetKeyValue(valueNames, "Character"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                DDE = this.GetKeyValue(valueNames, "DDE"),
                DDEAttr = this.GetKeyValue(valueNames, "DDEAttr"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorAudio = this.GetKeyValue(valueNames, "ErrorAudio"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Length = this.GetKeyValue(valueNames, "Length"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Termination = this.GetKeyValue(valueNames, "Termination"),
                Timeout = this.GetKeyValue(valueNames, "Timeout"),
                Tries = this.GetKeyValue(valueNames, "Tries"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                VerificationType = this.GetKeyValue(valueNames, "VerificationType"),
                Verify = this.GetKeyValue(valueNames, "Verify"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return callerDataEntryAction;
        }

        private CaseAction GetCaseInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            CaseAction caseAction = new CaseAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                ExpressionFinalStepText = this.GetKeyValue(valueNames, "ExpressionFinalStepText"),
                ExpressionSubText1 = this.GetKeyValue(valueNames, "ExpressionSubText1"),
                ExpressionText = this.GetKeyValue(valueNames, "ExpressionText"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Operator = this.GetKeyValue(valueNames, "Operator"),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                RHSData = this.GetKeyValue(valueNames, "RHSData"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return caseAction;
        }

        public List<Action> GetChildrenActions(string key)
        {
            List<Action> actions;
            try
            {
                List<Action> actions1 = new List<Action>();
                Parallel.ForEach<KeyValuePair<string, Dictionary<string, RegValueObject>>>(this.GetSubKeys(key), (KeyValuePair<string, Dictionary<string, RegValueObject>> action) => actions1.AddRange(this.GetActionInformation(action)));
                actions = actions1;
            }
            catch (Exception)
            {
                return null;
            }
            return actions;
        }

        private ComplexOperationAction GetComplexOperationInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ComplexOperationAction complexOperationAction = new ComplexOperationAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return complexOperationAction;
        }

        private ConferenceAction GetConferenceInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ConferenceAction conferenceAction = new ConferenceAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return conferenceAction;
        }

        private DBInsertAction GetDBInsertInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            DBInsertAction dBInsertAction = new DBInsertAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                DataSource = this.GetKeyValue(valueNames, "DataSource"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                Insert = this.GetKeyValue(valueNames, "Insert"),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                TableName = this.GetKeyValue(valueNames, "TableName"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                WhereColumn = this.GetKeyValue(valueNames, "WhereColumn"),
                WhereDataValue = this.GetKeyValue(valueNames, "WhereDataValue"),
                WhereOperator = this.GetKeyValue(valueNames, "WhereOperator"),
                WhereTimeValue = this.GetKeyValue(valueNames, "WhereTimeValue"),
                WhereType = this.GetKeyValue(valueNames, "WhereType")
            };
            return dBInsertAction;
        }

        private DBQueryAction GetDBQueryInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            DBQueryAction dBQueryAction = new DBQueryAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                DataSource = this.GetKeyValue(valueNames, "DataSource"),
                DDE = this.GetKeyValue(valueNames, "DDE"),
                DDEAttr = this.GetKeyValue(valueNames, "DDEAttr"),
                NoReturnAction = this.GetKeyValue(valueNames, "NoReturnAction"),
                QueryLocation = this.GetKeyValue(valueNames, "QueryLocation"),
                ReadDataType = this.GetKeyValue(valueNames, "ReadDataType"),
                ReadToCaller = this.GetKeyValue(valueNames, "ReadToCaller"),
                ReturnColumn = this.GetKeyValue(valueNames, "ReturnColumn"),
                WhereAttributeValue = this.GetKeyValue(valueNames, "WhereAttributeValue"),
                WhereColumn = this.GetKeyValue(valueNames, "WhereColumn"),
                WhereDataValue = this.GetKeyValue(valueNames, "WhereDataValue"),
                WhereOperator = this.GetKeyValue(valueNames, "WhereOperator"),
                WhereTimeValue = this.GetKeyValue(valueNames, "WhereTimeValue"),
                WhereType = this.GetKeyValue(valueNames, "WhereType")
            };
            return dBQueryAction;
        }

        private DBUpdateAction GetDBUpdateInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            DBUpdateAction dBUpdateAction = new DBUpdateAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                DataSource = this.GetKeyValue(valueNames, "DataSource"),
                TableName = this.GetKeyValue(valueNames, "TableName"),
                UpdateColumn = this.GetKeyValue(valueNames, "UpdateColumn"),
                UpdateDate = this.GetKeyValue(valueNames, "UpdateDate"),
                UpdateString = this.GetKeyValue(valueNames, "UpdateString"),
                UpdateTime = this.GetKeyValue(valueNames, "UpdateTime"),
                UpdateType = this.GetKeyValue(valueNames, "UpdateType"),
                WhereColumn = this.GetKeyValue(valueNames, "WhereColumn"),
                WhereDataValue = this.GetKeyValue(valueNames, "WhereDataValue"),
                WhereOperator = this.GetKeyValue(valueNames, "WhereOperator"),
                WhereStringValue = this.GetKeyValue(valueNames, "WhereStringValue"),
                WhereTimeValue = this.GetKeyValue(valueNames, "WhereTimeValue"),
                WhereType = this.GetKeyValue(valueNames, "WhereType")
            };
            return dBUpdateAction;
        }

        private DialByNameAction GetDialByNameInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            DialByNameAction dialByNameAction = new DialByNameAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Chars = this.GetKeyValue(valueNames, "Chars"),
                LimitedSearch = this.GetKeyValue(valueNames, "LimitedSearch"),
                MatchBy = this.GetKeyValue(valueNames, "MatchBy"),
                MatchStyle = this.GetKeyValue(valueNames, "MatchStyle")
            };
            return dialByNameAction;
        }

        private DisconnectAction GetDisconnectInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            DisconnectAction disconnectAction = new DisconnectAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile")
            };
            return disconnectAction;
        }

        private ExtensionDialAction GetExtensionDialInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ExtensionDialAction extensionDialAction = new ExtensionDialAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                LimitedSearch = this.GetKeyValue(valueNames, "LimitedSearch"),
                RepeatCt = this.GetKeyValue(valueNames, "RepeatCt"),
                Timeout = this.GetKeyValue(valueNames, "Timeout")
            };
            return extensionDialAction;
        }

        private ExternalTransferAction GetExternalTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ExternalTransferAction externalTransferAction = new ExternalTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Number = this.GetKeyValue(valueNames, "Number"),
                ReleaseOnTransfer = this.GetKeyValue(valueNames, "ReleaseOnTransfer"),
                TransferType = this.GetKeyValue(valueNames, "TransferType")
            };
            return externalTransferAction;
        }

        private FaxBackAction GetFaxBackInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            FaxBackAction faxBackAction = new FaxBackAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                FaxNum = this.GetKeyValue(valueNames, "FaxNum"),
                File = this.GetKeyValue(valueNames, "File"),
                SendOnThisCall = this.GetKeyValue(valueNames, "SendOnThisCall")
            };
            return faxBackAction;
        }

        private IAtTransferAction GetIAtTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            IAtTransferAction atTransferAction = new IAtTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                TransferConfig = this.GetKeyValue(valueNames, "TransferConfig"),
                TransferSel = this.GetKeyValue(valueNames, "TransferSel"),
                TransferType = this.GetKeyValue(valueNames, "TransferType")
            };
            return atTransferAction;
        }

        private string GetKeyValue(KeyValuePair<string, Dictionary<string, RegValueObject>> keyPair, string key)
        {
            if (!keyPair.Value.ContainsKey(key))
            {
                return string.Empty;
            }
            return keyPair.Value[key].Value.Replace("\r\n", string.Empty);
        }

        private LanguageSelectionAction GetLanguageSelectionInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            LanguageSelectionAction languageSelectionAction = new LanguageSelectionAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Language = this.GetKeyValue(valueNames, "Language"),
                LanguageName = this.GetKeyValue(valueNames, "LanguageName")
            };
            return languageSelectionAction;
        }

        private LoggingAction GetLoggingInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            LoggingAction loggingAction = new LoggingAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                CallerDataNode = this.GetKeyValue(valueNames, "CallerDataNode"),
                DBDataNode = this.GetKeyValue(valueNames, "DBDataNode"),
                LogData = this.GetKeyValue(valueNames, "LogData"),
                LoggingAttr = this.GetKeyValue(valueNames, "LoggingAttr"),
                LoggingText = this.GetKeyValue(valueNames, "LoggingText")
            };
            return loggingAction;
        }

        private LogicalTransferAction GetLogicalTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            LogicalTransferAction logicalTransferAction = new LogicalTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                CallOperator = this.GetKeyValue(valueNames, "CallOperator"),
                CallTrunkOperator = this.GetKeyValue(valueNames, "CallTrunkOperator"),
                DBOperator = this.GetKeyValue(valueNames, "DBOperator"),
                DurationOperator = this.GetKeyValue(valueNames, "DurationOperator"),
                ExpType = this.GetKeyValue(valueNames, "ExpType"),
                Initialized = this.GetKeyValue(valueNames, "Initialized"),
                LHSCallDataAttrType = this.GetKeyValue(valueNames, "LHSCallDataAttrType"),
                LHSCallDataRoutingAttr = this.GetKeyValue(valueNames, "LHSCallDataRoutingAttr"),
                LHSWorkgroupAttr = this.GetKeyValue(valueNames, "LHSWorkgroupAttr"),
                OnFalse = this.FormatRegistryPath(this.GetKeyValue(valueNames, "OnFalse")),
                OnTrue = this.FormatRegistryPath(this.GetKeyValue(valueNames, "OnTrue")),
                RHSCallDataValueType = this.GetKeyValue(valueNames, "RHSCallDataValueType"),
                RHSCallLength = this.GetKeyValue(valueNames, "RHSCallLength"),
                RHSCallLengthUnits = this.GetKeyValue(valueNames, "RHSCallLengthUnits"),
                RHSDBType = this.GetKeyValue(valueNames, "RHSDBType"),
                RHSDBValueType = this.GetKeyValue(valueNames, "RHSDBValueType"),
                RHSDateTime = this.GetKeyValue(valueNames, "RHSDateTime"),
                RHSIsDate = this.GetKeyValue(valueNames, "RHSIsDate"),
                RHSIsTime = this.GetKeyValue(valueNames, "RHSIsTime"),
                RHSWgDurType = this.GetKeyValue(valueNames, "RHSWgDurType"),
                RHSWgDurValue = this.GetKeyValue(valueNames, "RHSWgDurValue"),
                RHSWgNumType = this.GetKeyValue(valueNames, "RHSWgNumType"),
                RHSWgTimeUnits = this.GetKeyValue(valueNames, "RHSWgTimeUnits"),
                TimeOperator = this.GetKeyValue(valueNames, "TimeOperator"),
                UserOperator = this.GetKeyValue(valueNames, "UserOperator"),
                WgDurOperator = this.GetKeyValue(valueNames, "WgDurOperator"),
                WgNumOperator = this.GetKeyValue(valueNames, "WgNumOperator")
            };
            return logicalTransferAction;
        }

        private MenuAction GetMenuInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            MenuAction menuAction = new MenuAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                DelayTime = this.GetKeyValue(valueNames, "DelayTime"),
                ExtensionListener = this.GetKeyValue(valueNames, "ExtensionListener"),
                ExtensionTimeout = this.GetKeyValue(valueNames, "ExtensionTimeout"),
                FaxListener = this.GetKeyValue(valueNames, "FaxListener"),
                FaxQueueType = this.GetKeyValue(valueNames, "FaxQueueType"),
                MenuActions = this.GetKeyValue(valueNames, "MenuActions"),
                MenuDigits = this.GetKeyValue(valueNames, "MenuDigits"),
                RepeatCt = this.GetKeyValue(valueNames, "RepeatCt")
            };
            return menuAction;
        }

        private MenuTransferAction GetMenuTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            MenuTransferAction menuTransferAction = new MenuTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Menu = this.FormatRegistryPath(this.GetKeyValue(valueNames, "Menu"))
            };
            return menuTransferAction;
        }

        private OperatorAction GetOperatorInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            OperatorAction operatorAction = new OperatorAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return operatorAction;
        }

        private OtherToolsAction GetOtherToolsInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            OtherToolsAction otherToolsAction = new OtherToolsAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Tool = this.GetKeyValue(valueNames, "Tool"),
                ToolName = this.GetKeyValue(valueNames, "ToolName")
            };
            return otherToolsAction;
        }

        private PlayInfoAction GetPlayInfoInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            PlayInfoAction playInfoAction = new PlayInfoAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AttributeNames = this.GetKeyValue(valueNames, "AttributeNames"),
                AudioFileNames = this.GetKeyValue(valueNames, "AudioFileNames"),
                CustomFormatAttributeValues = this.GetKeyValue(valueNames, "CustomFormatAttributeValues"),
                CustomFormatAudioFileNames = this.GetKeyValue(valueNames, "CustomFormatAudioFileNames"),
                DataFormats = this.GetKeyValue(valueNames, "DataFormats"),
                DelayTime = this.GetKeyValue(valueNames, "DelayTime"),
                ImmediateNode = this.GetKeyValue(valueNames, "ImmediateNode"),
                RepeatCt = this.GetKeyValue(valueNames, "RepeatCt")
            };
            return playInfoAction;
        }

        private AttendantProfile GetProfileInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            AttendantProfile attendantProfile = new AttendantProfile();
            if (this.ReadingProfile != null)
            {
                this.ReadingProfile.BeginInvoke(this, new ActionEventArgs(this.GetKeyValue(valueNames, "Name")), null, null);
            }
            attendantProfile.Active = this.GetKeyValue(valueNames, "Active");
            attendantProfile.AddNote = this.GetKeyValue(valueNames, "AddNote");
            attendantProfile.AllowTransfers = this.GetKeyValue(valueNames, "AllowTransfers");
            attendantProfile.ANIString = this.GetKeyValue(valueNames, "ANIString");
            attendantProfile.AudioFile = this.GetKeyValue(valueNames, "AudioFile");
            attendantProfile.Default = this.GetKeyValue(valueNames, "Default");
            attendantProfile.Digit = this.GetKeyValue(valueNames, "Digit");
            attendantProfile.DirectToQueueOnce = this.GetKeyValue(valueNames, "DirectToQueueOnce");
            attendantProfile.DisableSpeechRec = this.GetKeyValue(valueNames, "DisableSpeechRec");
            attendantProfile.DNISString = this.GetKeyValue(valueNames, "DNISString");
            attendantProfile.FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath"));
            attendantProfile.JumpNode = this.GetKeyValue(valueNames, "JumpNode");
            attendantProfile.JumpToType = this.GetKeyValue(valueNames, "JumpToType");
            attendantProfile.Name = this.GetKeyValue(valueNames, "Name");
            attendantProfile.QueueType = this.GetKeyValue(valueNames, "QueueType");
            attendantProfile.Reporting = this.GetKeyValue(valueNames, "Reporting");
            attendantProfile.Style = this.GetKeyValue(valueNames, "Style");
            attendantProfile.SystemActive = this.GetKeyValue(valueNames, "SystemActive");
            attendantProfile.SystemNode = this.GetKeyValue(valueNames, "SystemNode");
            attendantProfile.Timeout = this.GetKeyValue(valueNames, "Timeout");
            attendantProfile.Type = this.GetKeyValue(valueNames, "Type");
            attendantProfile.UseANI = this.GetKeyValue(valueNames, "UseANI");
            attendantProfile.UseDNIS = this.GetKeyValue(valueNames, "UseDNIS");
            attendantProfile.UseTrunk = this.GetKeyValue(valueNames, "UseTrunk");
            attendantProfile.Valid = this.GetKeyValue(valueNames, "Valid");
            attendantProfile.Warnings = this.GetKeyValue(valueNames, "Warnings");
            attendantProfile.ServerPath = this.RegistryServerPath;
            attendantProfile.IsSelected = true;
            return attendantProfile;
        }

        private QueueAudioAction GetQueueAudioInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueAudioAction queueAudioAction = new QueueAudioAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                Continuous = this.GetKeyValue(valueNames, "Continuous"),
                Style = this.GetKeyValue(valueNames, "Style"),
                Timeout = this.GetKeyValue(valueNames, "Timeout")
            };
            return queueAudioAction;
        }

        private QueueMenuAction GetQueueMenuInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueMenuAction queueMenuAction = new QueueMenuAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AudioFile = this.GetKeyValue(valueNames, "AudioFile"),
                Timeout = this.GetKeyValue(valueNames, "Timeout")
            };
            return queueMenuAction;
        }

        private QueueRepeatAction GetQueueRepeatInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueRepeatAction queueRepeatAction = new QueueRepeatAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                RepeatCt = this.GetKeyValue(valueNames, "RepeatCt"),
                RepeatNode = this.GetKeyValue(valueNames, "RepeatNode")
            };
            return queueRepeatAction;
        }

        private QueueStayAction GetQueueStayInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueStayAction queueStayAction = new QueueStayAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return queueStayAction;
        }

        private QueueTransferAction GetQueueTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueTransferAction queueTransferAction = new QueueTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Workgroup = this.GetKeyValue(valueNames, "Workgroup")
            };
            return queueTransferAction;
        }

        private QueueUpdateSkillsAction GetQueueUpdateSkillsInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            QueueUpdateSkillsAction queueUpdateSkillsAction = new QueueUpdateSkillsAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                DynamicSkillsToAdd = this.GetKeyValue(valueNames, "DynamicSkillsToAdd"),
                DynamicSkillsToRemove = this.GetKeyValue(valueNames, "DynamicSkillsToRemove")
            };
            return queueUpdateSkillsAction;
        }

        private ReceiveFaxAction GetReceiveFaxInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ReceiveFaxAction receiveFaxAction = new ReceiveFaxAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                QueueType = this.GetKeyValue(valueNames, "QueueType"),
                UserQueue = this.GetKeyValue(valueNames, "UserQueue"),
                WorkgroupQueue = this.GetKeyValue(valueNames, "WorkgroupQueue")
            };
            return receiveFaxAction;
        }

        private RemoteAccessAction GetRemoteAccessInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            RemoteAccessAction remoteAccessAction = new RemoteAccessAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Code = this.GetKeyValue(valueNames, "Code")
            };
            return remoteAccessAction;
        }

        private RemoteDataQueryAction GetRemoteDataQueryInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            RemoteDataQueryAction remoteDataQueryAction = new RemoteDataQueryAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Certificate = this.GetKeyValue(valueNames, "Certificate"),
                ConnectTimeout = this.GetKeyValue(valueNames, "ConnectTimeout"),
                IgnoreCertMismatch = this.GetKeyValue(valueNames, "IgnoreCertMismatch"),
                IgnoreInvalidCert = this.GetKeyValue(valueNames, "IgnoreInvalidCert"),
                IgnoreUnknownCA = this.GetKeyValue(valueNames, "IgnoreUnknownCA"),
                IgnoreWrongCert = this.GetKeyValue(valueNames, "IgnoreWrongCert"),
                InputXML = this.GetKeyValue(valueNames, "InputXML"),
                MethodOutputAttributeNames = this.GetKeyValue(valueNames, "MethodOutputAttributeNames"),
                MethodOutputXMLNames = this.GetKeyValue(valueNames, "MethodOutputXMLNames"),
                Password = this.GetKeyValue(valueNames, "Password"),
                RequestTimeout = this.GetKeyValue(valueNames, "RequestTimeout"),
                SSLEnabled = this.GetKeyValue(valueNames, "SSLEnabled"),
                Timeout = this.GetKeyValue(valueNames, "Timeout"),
                URL = this.GetKeyValue(valueNames, "URL"),
                Username = this.GetKeyValue(valueNames, "Username")
            };
            return remoteDataQueryAction;
        }

        private ScheduleInformation GetScheduleInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ScheduleInformation scheduleInformation = new ScheduleInformation();
            if (this.ReadingSchedule != null)
            {
                this.ReadingSchedule.BeginInvoke(this, new ActionEventArgs(this.GetKeyValue(valueNames, "Name")), null, null);
            }
            scheduleInformation.Active = this.GetKeyValue(valueNames, "Active");
            scheduleInformation.AddNote = this.GetKeyValue(valueNames, "AddNote");
            scheduleInformation.AudioFile = this.GetKeyValue(valueNames, "AudioFile");
            scheduleInformation.Default = this.GetKeyValue(valueNames, "Default");
            scheduleInformation.DelayTime = this.GetKeyValue(valueNames, "DelayTime");
            scheduleInformation.Digit = this.GetKeyValue(valueNames, "Digit");
            scheduleInformation.EndTime = this.GetKeyValue(valueNames, "EndTime");
            scheduleInformation.ErrorAudioFile = this.GetKeyValue(valueNames, "ErrorAudioFile");
            scheduleInformation.ErrorType = this.GetKeyValue(valueNames, "ErrorType");
            scheduleInformation.ExtensionListener = this.GetKeyValue(valueNames, "ExtensionListener");
            scheduleInformation.ExtensionTimeout = this.GetKeyValue(valueNames, "ExtensionTimeout");
            scheduleInformation.FaxListener = this.GetKeyValue(valueNames, "FaxListener");
            scheduleInformation.FaxQueueType = this.GetKeyValue(valueNames, "FaxQueueType");
            scheduleInformation.FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath"));
            scheduleInformation.JumpNode = this.GetKeyValue(valueNames, "JumpNode");
            scheduleInformation.JumpToType = this.GetKeyValue(valueNames, "JumpToType");
            scheduleInformation.Menu = this.GetKeyValue(valueNames, "Menu");
            scheduleInformation.MenuActions = this.GetKeyValue(valueNames, "MenuActions");
            scheduleInformation.MenuDigits = this.GetKeyValue(valueNames, "MenuDigits");
            scheduleInformation.Name = this.GetKeyValue(valueNames, "Name");
            scheduleInformation.OperatorProfile = this.GetKeyValue(valueNames, "OperatorProfile");
            scheduleInformation.QueueType = this.GetKeyValue(valueNames, "QueueType");
            scheduleInformation.Periodicity = this.GetKeyValue(valueNames, "Periodicity");
            scheduleInformation.RepeatCt = this.GetKeyValue(valueNames, "RepeatCt");
            scheduleInformation.Reporting = this.GetKeyValue(valueNames, "Reporting");
            scheduleInformation.ScheduleData = this.GetKeyValue(valueNames, "ScheduleData");
            scheduleInformation.ScheduleTimeZoneID = this.GetKeyValue(valueNames, "ScheduleTimeZoneID");
            scheduleInformation.StartTime = this.GetKeyValue(valueNames, "StartTime");
            scheduleInformation.StationQueue = this.GetKeyValue(valueNames, "StationQueue");
            scheduleInformation.SystemActive = this.GetKeyValue(valueNames, "SystemActive");
            scheduleInformation.SystemNode = this.GetKeyValue(valueNames, "SystemNode");
            scheduleInformation.Type = this.GetKeyValue(valueNames, "Type");
            scheduleInformation.UserQueue = this.GetKeyValue(valueNames, "UserQueue");
            scheduleInformation.Valid = this.GetKeyValue(valueNames, "Valid");
            scheduleInformation.Warnings = this.GetKeyValue(valueNames, "Warnings");
            scheduleInformation.WorkgroupQueue = this.GetKeyValue(valueNames, "WorkgroupQueue");
            return scheduleInformation;
        }

        private ScreenPopAction GetScreenPopInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            ScreenPopAction screenPopAction = new ScreenPopAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                OverrideValues = this.GetKeyValue(valueNames, "OverrideValues"),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                ScreenPopType = this.GetKeyValue(valueNames, "ScreenPopType"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings")
            };
            return screenPopAction;
        }

        private SelectionAction GetSelectionInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            SelectionAction selectionAction = new SelectionAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                ExpressionFinalStepText = this.GetKeyValue(valueNames, "ExpressionFinalStepText"),
                ExpressionSubText1 = this.GetKeyValue(valueNames, "ExpressionSubText1"),
                ExpressionText = this.GetKeyValue(valueNames, "ExpressionText"),
                ExpressionType = this.GetKeyValue(valueNames, "ExpressionType"),
                LHSData = this.GetKeyValue(valueNames, "LHSData"),
                SelectionChildren = this.GetKeyValue(valueNames, "SelectionChildren")
            };
            return selectionAction;
        }

        private SetAdvancedStatisticAction GetSetAdvancedStatisticInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            SetAdvancedStatisticAction setAdvancedStatisticAction = new SetAdvancedStatisticAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                StatType = this.GetKeyValue(valueNames, "StatType"),
                StatValue = this.GetKeyValue(valueNames, "StatValue")
            };
            return setAdvancedStatisticAction;
        }

        private SetAttributeAction GetSetAttributeInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            SetAttributeAction setAttributeAction = new SetAttributeAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Attribute = this.GetKeyValue(valueNames, "Attribute"),
                Value = this.GetKeyValue(valueNames, "Value")
            };
            return setAttributeAction;
        }

        private string GetStringValue(string registryPath, string name)
        {
            string empty;
            try
            {
                RegValueObject item = this.AttendantRegValues[registryPath][name];
                empty = item.Value.Replace("\r\n", string.Empty);
            }
            catch (KeyNotFoundException)
            {
                empty = string.Empty;
            }
            return empty;
        }

        private IEnumerable<KeyValuePair<string, Dictionary<string, RegValueObject>>> GetSubKeys(string registryPath)
        {
            //foreach(var regValue in AttendantRegValues)
            //{
            //    if (regValue.Key.StartsWith(registryPath))
            //    {
            //        var parentPath = regValue.Key.Substring(0, regValue.Key.LastIndexOf("\\"));
            //        if (parentPath.Equals(registryPath, StringComparison.InvariantCultureIgnoreCase))
            //        {
            //            string test = "got it!";
            //        }
            //    }
            //}

            return
                from regValue in this.AttendantRegValues
                where regValue.Key.StartsWith(registryPath)
                where regValue.Key.Substring(0, regValue.Key.LastIndexOf("\\")).Equals(registryPath, StringComparison.InvariantCultureIgnoreCase)
                select regValue;
        }

        private SubroutineInitiatorAction GetSubroutineInitiatorInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            SubroutineInitiatorAction subroutineInitiatorAction = new SubroutineInitiatorAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                Subroutine = this.GetKeyValue(valueNames, "Subroutine")
            };
            return subroutineInitiatorAction;
        }

        private VoicemailTransferAction GetVoicemailTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            VoicemailTransferAction voicemailTransferAction = new VoicemailTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                JumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "JumpLocation")),
                JumpNode = this.GetKeyValue(valueNames, "JumpNode"),
                JumpToType = this.GetKeyValue(valueNames, "JumpToType"),
                Name = this.GetKeyValue(valueNames, "Name"),
                NextLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "NextLocation")),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AttempIATThreadedTransfer = this.GetKeyValue(valueNames, "AttempIATThreadedTransfer"),
                EscapeCharacter = this.GetKeyValue(valueNames, "EscapeCharacter"),
                GroupType = this.GetKeyValue(valueNames, "GroupType"),
                IATThreadedTransferTimeout = this.GetKeyValue(valueNames, "IATThreadedTransferTimeout"),
                IsACD = this.GetKeyValue(valueNames, "IsACD"),
                Priority = this.GetKeyValue(valueNames, "Priority"),
                PriorityType = this.GetKeyValue(valueNames, "PriorityType"),
                Timeout = this.GetKeyValue(valueNames, "Timeout"),
                TimeoutJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "TimeoutJumpLocation")),
                TimeoutJumpToType = this.GetKeyValue(valueNames, "TimeoutJumpToType"),
                TimeoutStyle = this.GetKeyValue(valueNames, "TimeoutStyle"),
                WorkgroupQueue = this.GetKeyValue(valueNames, "WorkgroupQueue")
            };
            return voicemailTransferAction;
        }

        private WorkgroupTransferAction GetWorkgroupTransferInformation(KeyValuePair<string, Dictionary<string, RegValueObject>> valueNames)
        {
            WorkgroupTransferAction workgroupTransferAction = new WorkgroupTransferAction()
            {
                Active = this.GetKeyValue(valueNames, "Active"),
                AddNote = this.GetKeyValue(valueNames, "AddNote"),
                ChildIndex = this.GetKeyValue(valueNames, "ChildIndex"),
                Default = this.GetKeyValue(valueNames, "Default"),
                Digit = this.GetKeyValue(valueNames, "Digit"),
                ErrorJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "ErrorJumpLocation")),
                ErrorType = this.GetKeyValue(valueNames, "ErrorType"),
                FullNodePath = this.FormatRegistryPath(this.GetKeyValue(valueNames, "FullNodePath")),
                Name = this.GetKeyValue(valueNames, "Name"),
                Reporting = this.GetKeyValue(valueNames, "Reporting"),
                SystemActive = this.GetKeyValue(valueNames, "SystemActive"),
                SystemNode = this.GetKeyValue(valueNames, "SystemNode"),
                Type = this.GetKeyValue(valueNames, "Type"),
                Valid = this.GetKeyValue(valueNames, "Valid"),
                Warnings = this.GetKeyValue(valueNames, "Warnings"),
                AttempIATThreadedTransfer = this.GetKeyValue(valueNames, "AttempIATThreadedTransfer"),
                EscapeCharacter = this.GetKeyValue(valueNames, "EscapeCharacter"),
                GroupType = this.GetKeyValue(valueNames, "GroupType"),
                IATThreadedTransferTimeout = this.GetKeyValue(valueNames, "IATThreadedTransferTimeout"),
                IsACD = this.GetKeyValue(valueNames, "IsACD"),
                Priority = this.GetKeyValue(valueNames, "Priority"),
                PriorityType = this.GetKeyValue(valueNames, "PriorityType"),
                Timeout = this.GetKeyValue(valueNames, "Timeout"),
                TimeoutJumpLocation = this.FormatRegistryPath(this.GetKeyValue(valueNames, "TimeoutJumpLocation")),
                TimeoutStyle = this.GetKeyValue(valueNames, "TimeoutStyle"),
                Workgroup = this.GetKeyValue(valueNames, "Workgroup")
            };
            return workgroupTransferAction;
        }

        public bool HasActions(string parentPath)
        {
            return this.GetSubKeys(parentPath).Any<KeyValuePair<string, Dictionary<string, RegValueObject>>>();
        }

        public void InitRegistryPaths()
        {
            this._activeAttendantConfig = this.GetStringValue(string.Concat(this.RegistryServerPath, "\\AttendantData"), "ActiveConfig");
            if (string.IsNullOrEmpty(this._activeAttendantConfig))
            {
                throw new Exception("Could not find Active Attendant Config.");
            }
            this._baseProfilesPath = string.Format("{0}\\AttendantData\\{1}", this.RegistryServerPath, this._activeAttendantConfig);
        }

        public void LoadFrom(string filename)
        {
        }

        public void SaveTo(string filename)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<AttendantProfile>), ActionTypes.AllTypes);
            using (StreamWriter streamWriter = new StreamWriter(filename))
            {
                xmlSerializer.Serialize(streamWriter, this._attendantProfiles);
            }
        }

        public event ReadingActionEventHandler ReadingAction;

        public event ReadingProfileEventHandler ReadingProfile;

        public event ReadingScheduleEventHandler ReadingSchedule;

        public enum RegHive : uint
        {
            HkeyClassesRoot = 2147483648,
            HkeyCurrentUser = 2147483649,
            HkeyLocalMachine = 2147483650,
            HkeyUsers = 2147483651,
            HkeyCurrentConfig = 2147483653
        }

        public enum RegType
        {
            RegSz = 1,
            RegExpandSz = 2,
            RegBinary = 3,
            RegDword = 4,
            RegMultiSz = 7
        }
    }
}