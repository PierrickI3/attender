﻿using System;
using System.Runtime.InteropServices;

namespace Helper
{
    public class NetworkShare
    {
        #region Member Variables
        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2A(ref NetResource refNetResource, string inPassword, string inUsername, int inFlags);
        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2A(string inServer, int inFlags, int inForce);

        private String _server;
        private String _share;
        private String _driveLetter;
        private String _username;
        private String _password;
        private int _flags;
        private NetResource _netResource;
        Int32 _allowDisconnectWhenInUse = 1; // 0 = False; Any other value is True;
        #endregion

        #region Constructors
        /// <summary>
        /// The default constructor
        /// </summary>
        public NetworkShare()
        {
        }

        /// <summary>
        /// This constructor takes a server and a share.
        /// </summary>
        public NetworkShare(String inServer, String inShare)
        {
            _server = inServer;
            _share = inShare;
        }

        /// <summary>
        /// This constructor takes a server and a share and a local drive letter.
        /// </summary>
        public NetworkShare(String inServer, String inShare, String inDriveLetter)
        {
            _server = inServer;
            _share = inShare;
            DriveLetter = inDriveLetter;
        }

        /// <summary>
        /// This constructor takes a server, share, username, and password.
        /// </summary>
        public NetworkShare(String inServer, String inShare, String inUsername, String inPassword)
        {
            _server = inServer;
            _share = inShare;
            _username = inUsername;
            _password = inPassword;
        }

        /// <summary>
        /// This constructor takes a server, share, drive letter, username, and password.
        /// </summary>
        public NetworkShare(String inServer, String inShare, String inDriveLetter, String inUsername, String inPassword)
        {
            _server = inServer;
            _share = inShare;
            _driveLetter = inDriveLetter;
            _username = inUsername;
            _password = inPassword;
        }
        #endregion

        #region Properties
        public String Server
        {
            get { return _server; }
            set { _server = value; }
        }

        public String Share
        {
            get { return _share; }
            set { _share = value; }
        }

        public String FullPath
        {
            get { return string.Format(@"\\{0}\{1}", _server, _share); }
        }

        public String DriveLetter
        {
            get { return _driveLetter; }
            set { SetDriveLetter(value); }
        }

        public String Username
        {
            get { return String.IsNullOrEmpty(_username) ? null : _username; }
            set { _username = value; }
        }

        public String Password
        {
            get { return String.IsNullOrEmpty(_password) ? null : _username; }
            set { _password = value; }
        }

        public int Flags
        {
            get { return _flags; }
            set { _flags = value; }
        }

        public NetResource Resource
        {
            get { return _netResource; }
            set { _netResource = value; }
        }

        public bool AllowDisconnectWhenInUse
        {
            get { return Convert.ToBoolean(_allowDisconnectWhenInUse); }
            set { _allowDisconnectWhenInUse = Convert.ToInt32(value); }
        }

        #endregion

        #region Functions
        /// <summary>
        /// Establishes a connection to the share.
        ///
        /// Throws:
        ///
        ///
        /// </summary>
        public int Connect()
        {
            _netResource.Scope = (int)Scope.ResourceGlobalnet;
            _netResource.Type = (int)Type.ResourcetypeDisk;
            _netResource.DisplayType = (int)DisplayType.ResourcedisplaytypeShare;
            _netResource.Usage = (int)Usage.ResourceusageConnectable;
            _netResource.RemoteName = FullPath;
            _netResource.LocalName = DriveLetter;

            return WNetAddConnection2A(ref _netResource, _password, _username, _flags);
        }

        /// <summary>
        /// Disconnects from the share.
        /// </summary>
        public int Disconnect()
        {
            int retVal;
            if (null != _driveLetter)
            {
                retVal = WNetCancelConnection2A(_driveLetter, _flags, _allowDisconnectWhenInUse);
                retVal = WNetCancelConnection2A(FullPath, _flags, _allowDisconnectWhenInUse);
            }
            else
            {
                retVal = WNetCancelConnection2A(FullPath, _flags, _allowDisconnectWhenInUse);
            }
            return retVal;
        }

        private void SetDriveLetter(String inString)
        {
            switch (inString.Length)
            {
                case 1:
                    if (char.IsLetter(inString.ToCharArray()[0]))
                    {
                        _driveLetter = inString + ":";
                    }
                    else
                    {
                        // The character is not a drive letter
                        _driveLetter = null;
                    }
                    break;
                case 2:
                    {
                        var drive = inString.ToCharArray();
                        if (char.IsLetter(drive[0]) && drive[1] == ':')
                        {
                            _driveLetter = inString;
                        }
                        else
                        {
                            // The character is not a drive letter
                            _driveLetter = null;
                        }
                    }
                    break;
                default:
                    _driveLetter = null;
                    break;
            }
        }

        #endregion

        #region NetResource Struct
        [StructLayout(LayoutKind.Sequential)]
        public struct NetResource
        {
            public uint Scope;
            public uint Type;
            public uint DisplayType;
            public uint Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }
        #endregion

        #region Enums
        public enum Scope
        {
            ResourceConnected = 1,
            ResourceGlobalnet,
            ResourceRemembered,
            ResourceRecent,
            ResourceContext
        }

        public enum Type : uint
        {
            ResourcetypeAny,
            ResourcetypeDisk,
            ResourcetypePrint,
            ResourcetypeReserved = 8,
            ResourcetypeUnknown = 4294967295
        }

        public enum DisplayType
        {
            ResourcedisplaytypeGeneric,
            ResourcedisplaytypeDomain,
            ResourcedisplaytypeServer,
            ResourcedisplaytypeShare,
            ResourcedisplaytypeFile,
            ResourcedisplaytypeGroup,
            ResourcedisplaytypeNetwork,
            ResourcedisplaytypeRoot,
            ResourcedisplaytypeShareadmin,
            ResourcedisplaytypeDirectory,
            ResourcedisplaytypeTree,
            ResourcedisplaytypeNdscontainer
        }

        public enum Usage : uint
        {
            ResourceusageConnectable = 1,
            ResourceusageContainer = 2,
            ResourceusageNolocaldevice = 4,
            ResourceusageSibling = 8,
            ResourceusageAttached = 16,
            ResourceusageAll = 31,
            ResourceusageReserved = 2147483648
        }

        public enum ConnectionFlags : uint
        {
            ConnectUpdateProfile = 1,
            ConnectUpdateRecent = 2,
            ConnectTemporary = 4,
            ConnectInteractive = 8,
            ConnectPrompt = 16,
            ConnectNeedDrive = 32,
            ConnectRefcount = 64,
            ConnectRedirect = 128,
            ConnectLocaldrive = 256,
            ConnectCurrentMedia = 512,
            ConnectDeferred = 1024,
            ConnectCommandline = 2048,
            ConnectCmdSavecred = 4096,
            ConnectCredReset = 8192,
            ConnectReserved = 4278190080
        }
        #endregion
    }
}