﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Helper
{
    public static class ActionTypes
    {
        public static Type[] AllTypes
        {
            get
            {
                return new Type[]
                {
                    typeof(AudioPlaybackAction),
                    typeof(AgentTransferAction),
                    typeof(CallbackRequestAction),
                    typeof(CallerDataEntryAction),
                    typeof(CaseAction),
                    typeof(ComplexOperationAction),
                    typeof(ConferenceAction),
                    typeof(DialByNameAction),
                    typeof(DBInsertAction),
                    typeof(DBQueryAction),
                    typeof(DBUpdateAction),
                    typeof(DisconnectAction),
                    typeof(ExtensionDialAction),
                    typeof(ExternalTransferAction),
                    typeof(FaxBackAction),
                    typeof(IAtTransferAction),
                    typeof(LanguageSelectionAction),
                    typeof(LoggingAction),
                    typeof(LogicalTransferAction),
                    typeof(MenuAction),
                    typeof(MenuTransferAction),
                    typeof(OtherToolsAction),
                    typeof(OperatorAction),
                    typeof(PlayInfoAction),
                    typeof(QueueAudioAction),
                    typeof(QueueMenuAction),
                    typeof(QueueRepeatAction),
                    typeof(QueueStayAction),
                    typeof(QueueTransferAction),
                    typeof(QueueUpdateSkillsAction),
                    typeof(ReceiveFaxAction),
                    typeof(RemoteAccessAction),
                    typeof(RemoteDataQueryAction),
                    typeof(SelectionAction),
                    typeof(SetAdvancedStatisticAction),
                    typeof(SetAttributeAction),
                    typeof(SubroutineInitiatorAction),
                    typeof(VoicemailTransferAction),
                    typeof(WorkgroupTransferAction)
                };
            }
        }
    }

    public class Connector
    {
        public int Id { get; set; }
        public int FromShapeId { get; set; }
        public int ToShapeId { get; set; }
        public string Text { get; set; }

        public Connector(int id, int fromShapeId, int toShapeId, string text)
        {
            Id = id;
            FromShapeId = fromShapeId;
            ToShapeId = toShapeId;
            Text = text;
        }
    }

    [Serializable]
    public class AttendantProfile
    {
        public string Active { get; set; }
        public string AddNote { get; set; }
        public string AllowTransfers { get; set; }
        public string ANIString { get; set; }
        public string AudioFile { get; set; }
        public string Default { get; set; }
        public string Digit { get; set; }
        public string DirectToQueueOnce { get; set; }
        public string DisableSpeechRec { get; set; }
        public string DNISString { get; set; }
        public string FullNodePath { get; set; }
        public string JumpNode { get; set; }
        public string JumpToType { get; set; }
        public string Name { get; set; }
        public string QueueType { get; set; }
        public string Reporting { get; set; }
        public string Style { get; set; }
        public string SystemActive { get; set; }
        public string SystemNode { get; set; }
        public string Timeout { get; set; }
        public string Type { get; set; }
        public string UseANI { get; set; }
        public string UseDNIS { get; set; }
        public string UseTrunk { get; set; }
        public string Valid { get; set; }
        public string Warnings { get; set; }
        public bool IsSelected { get; set; }
        public string ServerPath { get; set; } //For offline files

        private List<ScheduleInformation> _schedules;
        public List<ScheduleInformation> Schedules
        {
            get
            {
                return _schedules ?? (_schedules = new List<ScheduleInformation>());
            }
            set
            {
                _schedules = value;
            }
        }

        public int ShapeId { get; set; }
    }

    [Serializable]
    public class ScheduleInformation
    {
        public string Active { get; set; }
        public string AddNote { get; set; }
        public string AudioFile { get; set; }
        public string Default { get; set; }
        public string DelayTime { get; set; }
        public string Digit { get; set; }
        public string EndTime { get; set; }
        public string ErrorAudioFile { get; set; }
        public string ErrorType { get; set; }
        public string ExtensionListener { get; set; }
        public string ExtensionTimeout { get; set; }
        public string FaxListener { get; set; }
        public string FaxQueueType { get; set; }
        public string FullNodePath { get; set; }
        public string JumpNode { get; set; }
        public string JumpToType { get; set; }
        /// <summary>
        /// Used in the Schedule Error Handling dialog
        /// </summary>
        public string Menu { get; set; }
        public string MenuActions { get; set; }
        public string MenuDigits { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Used in the Schedule Error Handling dialog
        /// </summary>
        public string OperatorProfile { get; set; }
        public string Periodicity { get; set; }
        public string QueueType { get; set; }
        public string RepeatCt { get; set; }
        public string Reporting { get; set; }
        public string ScheduleData { get; set; }
        public string ScheduleTimeZoneID { get; set; }
        public string StartTime { get; set; }
        /// <summary>
        /// Used in the Schedule Error Handling dialog
        /// </summary>
        public string StationQueue { get; set; }
        public string SystemActive { get; set; }
        public string SystemNode { get; set; }
        public string Type { get; set; }
        /// <summary>
        /// Used in the Schedule Error Handling dialog
        /// </summary>
        public string UserQueue { get; set; }
        public string Valid { get; set; }
        public string Warnings { get; set; }
        /// <summary>
        /// Used in the Schedule Error Handling dialog
        /// </summary>
        public string WorkgroupQueue { get; set; }

        private List<Action> _actionList;
        public List<Action> ActionList
        {
            get { return _actionList ?? (_actionList = new List<Action>()); }
            set
            {
                _actionList = value;
            }
        }

        public int ShapeId { get; set; }
    }

    [Serializable]
    public abstract class Action
    {
        public string ParentFullNodePath { get; set; }

        public string Active { get; set; }
        public string AddNote { get; set; }
        public string ChildIndex { get; set; }
        public string Color { get; set; }
        public string Default { get; set; }
        public string Digit { get; set; }
        public string ErrorJumpLocation { get; set; }
        public string ErrorType { get; set; }
        public string FullNodePath { get; set; }
        public string JumpLocation { get; set; }
        public string JumpNode { get; set; }
        public string JumpToType { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Used in a multi-action container when selecting "Jump to the next location in this container"
        /// </summary>
        public string NextLocation { get; set; }
        public string Reporting { get; set; }
        public string SystemActive { get; set; }
        public string SystemNode { get; set; }
        public string Type { get; set; }
        public string Valid { get; set; }
        public string Warnings { get; set; }

        public int ShapeId { get; set; }
    }

    [Serializable]
    public class AudioPlaybackAction : Action
    {
        public string AudioFile { get; set; }
        public string AudioSource { get; set; }
        public string AudioSourceTimeout { get; set; }
        public string DelayTime { get; set; }
        public string DynamicAudio { get; set; }
        public string ExternalAudioSource { get; set; }
        public string RepeatCt { get; set; }
    }

    [Serializable]
    public class AgentTransferAction : Action
    {
        public string ParkInteraction { get; set; }
        public string QueueType { get; set; }
        public string StationQueue { get; set; }
        public string UserQueue { get; set; }
    }

    [Serializable]
    public class CallbackRequestAction : Action
    {
        public string AudioFile { get; set; }
        public string CallbackNumberNode { get; set; }
        public string CallbackNumberSource { get; set; }
        public string Workgroup { get; set; }
    }

    [Serializable]
    public class CallerDataEntryAction : Action
    {
        public string AudioFile { get; set; }
        public string Character { get; set; }
        public string DDE { get; set; }
        public string DDEAttr { get; set; }
        public string ErrorAudio { get; set; }
        public string Length { get; set; }
        public string Termination { get; set; }
        public string Timeout { get; set; }
        public string Tries { get; set; }
        public string VerificationType { get; set; }
        public string Verify { get; set; }
    }

    [Serializable]
    public class CaseAction : Action
    {
        public string ExpressionFinalStepText { get; set; }
        public string ExpressionSubText1 { get; set; }
        public string ExpressionText { get; set; }
        public string Operator { get; set; }
        public string RHSData { get; set; }
    }

    [Serializable]
    public class ComplexOperationAction : Action
    {
        public string AudioFile { get; set; }
    }

    [Serializable]
    public class ConferenceAction : Action
    {
    }

    [Serializable]
    public class DialByNameAction : Action
    {
        public string Chars { get; set; }
        public string LimitedSearch { get; set; }
        public string MatchBy { get; set; }
        public string MatchStyle { get; set; }
    }

    [Serializable]
    public class DBInsertAction : Action
    {
        public string DataSource { get; set; }
        public string Insert { get; set; }
        public string TableName { get; set; }
        public string WhereColumn { get; set; }
        public string WhereDataValue { get; set; }
        public string WhereOperator { get; set; }
        public string WhereTimeValue { get; set; }
        public string WhereType { get; set; }
    }

    [Serializable]
    public class DBQueryAction : Action
    {
        public string DataSource { get; set; }
        public string DDE { get; set; }
        public string DDEAttr { get; set; }
        public string NoReturnAction { get; set; }
        public string QueryLocation { get; set; }
        public string ReadDataType { get; set; }
        public string ReadToCaller { get; set; }
        public string ReturnColumn { get; set; }
        public string TableName { get; set; }
        public string WhereAttributeValue { get; set; }
        public string WhereColumn { get; set; }
        public string WhereDataValue { get; set; }
        public string WhereOperator { get; set; }
        public string WhereTimeValue { get; set; }
        public string WhereType { get; set; }
    }

    [Serializable]
    public class DBUpdateAction : Action
    {
        public string DataSource { get; set; }
        public string TableName { get; set; }
        public string UpdateColumn { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateString { get; set; }
        public string UpdateTime { get; set; }
        public string UpdateType { get; set; }
        public string WhereColumn { get; set; }
        public string WhereDataValue { get; set; }
        public string WhereOperator { get; set; }
        public string WhereStringValue { get; set; }
        public string WhereTimeValue { get; set; }
        public string WhereType { get; set; }
    }

    [Serializable]
    public class DisconnectAction : Action
    {
        public string AudioFile { get; set; }
        public string MoveToFolder { get; set; }
    }

    [Serializable]
    public class ExtensionDialAction : Action
    {
        public string AudioFile { get; set; }
        public string LimitedSearch { get; set; }
        public string RepeatCt { get; set; }
        public string Timeout { get; set; }
    }

    [Serializable]
    public class ExternalTransferAction : Action
    {
        public string Number { get; set; }
        public string ReleaseOnTransfer { get; set; }
        public string TransferType { get; set; }
    }

    [Serializable]
    public class FaxBackAction : Action
    {
        public string FaxNum { get; set; }
        public string File { get; set; }
        public string SendOnThisCall { get; set; }
    }

    [Serializable]
    public class IAtTransferAction : Action
    {
        public string TransferConfig { get; set; }
        public string TransferSel { get; set; }
        public string TransferType { get; set; }
    }

    [Serializable]
    public class LanguageSelectionAction : Action
    {
        public string Language { get; set; }
        public string LanguageName { get; set; }
    }

    [Serializable]
    public class LoggingAction : Action
    {
        public string CallerDataNode { get; set; }
        public string DBDataNode { get; set; }
        public string LogData { get; set; }
        public string LoggingAttr { get; set; }
        public string LoggingText { get; set; }
    }

    [Serializable]
    public class LogicalTransferAction : Action
    {
        public string CallOperator { get; set; }
        public string CallTrunkOperator { get; set; }
        public string DBOperator { get; set; }
        public string DurationOperator { get; set; }
        public string ExpType { get; set; }
        public string Initialized { get; set; }
        public string LHSCallDataAttrType { get; set; }
        public string LHSCallDataRoutingAttr { get; set; }
        public string LHSWorkgroupAttr { get; set; }
        public string OnFalse { get; set; }
        public string OnTrue { get; set; }
        public string RHSCallDataValueType { get; set; }
        public string RHSCallLength { get; set; }
        public string RHSCallLengthUnits { get; set; }
        public string RHSDateTime { get; set; }
        public string RHSDBType { get; set; }
        public string RHSDBValueType { get; set; }
        public string RHSIsDate { get; set; }
        public string RHSIsTime { get; set; }
        public string RHSWgDurType { get; set; }
        public string RHSWgDurValue { get; set; }
        public string RHSWgNumType { get; set; }
        public string RHSWgTimeUnits { get; set; }
        public string TimeOperator { get; set; }
        public string UserOperator { get; set; }
        public string WgDurOperator { get; set; }
        public string WgNumOperator { get; set; }
    }

    [Serializable]
    public class MenuAction : Action
    {
        public string AudioFile { get; set; }
        public string DelayTime { get; set; }
        public string ExtensionListener { get; set; }
        public string ExtensionTimeout { get; set; }
        public string FaxListener { get; set; }
        public string FaxQueueType { get; set; }
        public string MenuActions { get; set; }
        public string MenuDigits { get; set; }
        public string RepeatCt { get; set; }
    }

    [Serializable]
    public class MenuTransferAction : Action
    {
        public string Menu { get; set; }
    }

    [Serializable]
    public class OtherToolsAction : Action
    {
        public string Tool { get; set; }
        public string ToolName { get; set; }
    }

    [Serializable]
    public class OperatorAction : Action
    {
    }

    [Serializable]
    public class PlayInfoAction : Action
    {
        public string AttributeNames { get; set; }
        public string AudioFileNames { get; set; }
        public string CustomFormatAttributeValues { get; set; }
        public string CustomFormatAudioFileNames { get; set; }
        public string DataFormats { get; set; }
        public string DelayTime { get; set; }
        public string ImmediateNode { get; set; }
        public string RepeatCt { get; set; }
    }

    [Serializable]
    public class QueueAudioAction : Action
    {
        public string AudioFile { get; set; }
        public string Continuous { get; set; }
        public string Style { get; set; }
        public string Timeout { get; set; }
    }

    [Serializable]
    public class QueueMenuAction : Action
    {
        public string AudioFile { get; set; }
        public string Timeout { get; set; }
    }

    [Serializable]
    public class QueueRepeatAction : Action
    {
        public string RepeatCt { get; set; }
        public string RepeatNode { get; set; }
    }

    [Serializable]
    public class QueueStayAction : Action
    {
    }

    [Serializable]
    public class QueueTransferAction : Action
    {
        public string Workgroup { get; set; }
    }

    [Serializable]
    public class QueueUpdateSkillsAction : Action
    {
        public string DynamicSkillsToAdd { get; set; }
        public string DynamicSkillsToRemove { get; set; }
    }

    [Serializable]
    public class ReceiveFaxAction : Action
    {
        public string QueueType { get; set; }
        public string UserQueue { get; set; }
        public string WorkgroupQueue { get; set; }
    }

    [Serializable]
    public class RemoteAccessAction : Action
    {
        public string Code { get; set; }
    }

    [Serializable]
    public class RemoteDataQueryAction : Action
    {
        public string Certificate { get; set; }
        public string ConnectTimeout { get; set; }
        public string DNSResolveTimeout { get; set; }
        public string IgnoreCertMismatch { get; set; }
        public string IgnoreInvalidCert { get; set; }
        public string IgnoreUnknownCA { get; set; }
        public string IgnoreWrongCert { get; set; }
        public string InputXML { get; set; }
        public string MethodOutputAttributeNames { get; set; }
        public string MethodOutputXMLNames { get; set; }
        public string Password { get; set; }
        public string RequestTimeout { get; set; }
        public string SSLEnabled { get; set; }
        public string Timeout { get; set; }
        public string URL { get; set; }
        public string Username { get; set; }
    }

    public class ScreenPopAction : Action
    {
        public string OverrideValues { get; set; }

        public string ScreenPopType { get; set; }
    }

    public class SelectionAction : Action
    {
        public string ExpressionFinalStepText { get; set; }
        public string ExpressionSubText1 { get; set; }
        public string ExpressionText { get; set; }
        public string ExpressionType { get; set; }
        public string LHSData { get; set; }
        public string SelectionChildren { get; set; }
    }

    [Serializable]
    public class SetAdvancedStatisticAction : Action
    {
        public string StatType { get; set; }
        public string StatValue { get; set; }
    }

    [Serializable]
    public class SetAttributeAction : Action
    {
        public string Attribute { get; set; }
        public string Value { get; set; }
    }

    [Serializable]
    public class SubroutineInitiatorAction : Action
    {
        public string Subroutine { get; set; }
    }

    [Serializable]
    public class VoicemailTransferAction : Action
    {
        public string AttempIATThreadedTransfer { get; set; }
        public string EscapeCharacter { get; set; }
        public string GroupType { get; set; }
        public string IATThreadedTransferTimeout { get; set; }
        public string IsACD { get; set; }
        public string Priority { get; set; }
        public string PriorityType { get; set; }
        public string Timeout { get; set; }
        public string TimeoutJumpLocation { get; set; }
        public string TimeoutJumpToType { get; set; }
        public string TimeoutStyle { get; set; }
        public string WorkgroupQueue { get; set; }
    }

    [Serializable]
    public class WorkgroupTransferAction : Action
    {
        public string AttempIATThreadedTransfer { get; set; }
        public string EscapeCharacter { get; set; }
        public string GroupType { get; set; }
        public string IATThreadedTransferTimeout { get; set; }
        public string IsACD { get; set; }
        public string Priority { get; set; }
        public string PriorityType { get; set; }
        public string Timeout { get; set; }
        public string TimeoutJumpLocation { get; set; }
        public string TimeoutJumpToType { get; set; }
        public string TimeoutStyle { get; set; }
        public string Workgroup { get; set; }
    }
}