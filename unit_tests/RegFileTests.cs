﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Helper;

namespace unit_tests
{
    [TestClass]
    public class RegFileTests
    {
        private string _regFileName = "ININ Export.reg";
        private RegFileReader _regFileReader;

        [TestInitialize]
        public void LoadRegFile()
        {
            _regFileReader = new RegFileReader(_regFileName);
        }

        [TestMethod]
        public void CanReadRegFile()
        {
            
            _regFileReader.LoadFrom(_regFileName);
            Assert.IsTrue(_regFileReader.AttendantProfiles.Count > 0);
        }

        [TestMethod]
        public void CanGetProfiles()
        {
            var attendantProfiles = _regFileReader.GetAttendantProfiles(null);

        }
    }
}
