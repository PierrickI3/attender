﻿namespace Addin
{
    partial class AttenderRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public AttenderRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attenderTab = this.Factory.CreateRibbonTab();
            this.cicConfigurationGroup = this.Factory.CreateRibbonGroup();
            this.btnImportProfiles = this.Factory.CreateRibbonButton();
            this.logFileGroup = this.Factory.CreateRibbonGroup();
            this.callIdEditBox = this.Factory.CreateRibbonEditBox();
            this.attendantGroup = this.Factory.CreateRibbonGroup();
            this.btnLoadProfiles = this.Factory.CreateRibbonButton();
            this.btnSaveProfiles = this.Factory.CreateRibbonButton();
            this.helpGroup = this.Factory.CreateRibbonGroup();
            this.btnHelp = this.Factory.CreateRibbonButton();
            this.btnPurchase = this.Factory.CreateRibbonButton();
            this.lblTrial = this.Factory.CreateRibbonLabel();
            this.btnLoadFromReg = this.Factory.CreateRibbonButton();
            this.attenderTab.SuspendLayout();
            this.cicConfigurationGroup.SuspendLayout();
            this.logFileGroup.SuspendLayout();
            this.attendantGroup.SuspendLayout();
            this.helpGroup.SuspendLayout();
            // 
            // attenderTab
            // 
            this.attenderTab.Groups.Add(this.cicConfigurationGroup);
            this.attenderTab.Groups.Add(this.logFileGroup);
            this.attenderTab.Groups.Add(this.attendantGroup);
            this.attenderTab.Groups.Add(this.helpGroup);
            this.attenderTab.Label = "ATTENDER";
            this.attenderTab.Name = "attenderTab";
            // 
            // cicConfigurationGroup
            // 
            this.cicConfigurationGroup.Items.Add(this.btnImportProfiles);
            this.cicConfigurationGroup.Name = "cicConfigurationGroup";
            // 
            // btnImportProfiles
            // 
            this.btnImportProfiles.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnImportProfiles.Image = global::Addin.Properties.Resources.server_to_client1;
            this.btnImportProfiles.Label = "Import Attendant Profiles";
            this.btnImportProfiles.Name = "btnImportProfiles";
            this.btnImportProfiles.ShowImage = true;
            this.btnImportProfiles.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnImportProfiles_Click);
            // 
            // logFileGroup
            // 
            this.logFileGroup.Items.Add(this.callIdEditBox);
            this.logFileGroup.Label = "Troubleshooting";
            this.logFileGroup.Name = "logFileGroup";
            // 
            // callIdEditBox
            // 
            this.callIdEditBox.Label = "Call Id";
            this.callIdEditBox.MaxLength = 10;
            this.callIdEditBox.Name = "callIdEditBox";
            this.callIdEditBox.Text = null;
            // 
            // attendantGroup
            // 
            this.attendantGroup.Items.Add(this.btnLoadProfiles);
            this.attendantGroup.Items.Add(this.btnSaveProfiles);
            this.attendantGroup.Items.Add(this.btnLoadFromReg);
            this.attendantGroup.Label = "Offline Use";
            this.attendantGroup.Name = "attendantGroup";
            // 
            // btnLoadProfiles
            // 
            this.btnLoadProfiles.Image = global::Addin.Properties.Resources.import_xml;
            this.btnLoadProfiles.Label = "Load From XML";
            this.btnLoadProfiles.Name = "btnLoadProfiles";
            this.btnLoadProfiles.ShowImage = true;
            this.btnLoadProfiles.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnLoadProfiles_Click);
            // 
            // btnSaveProfiles
            // 
            this.btnSaveProfiles.Image = global::Addin.Properties.Resources.export_xml;
            this.btnSaveProfiles.Label = "Save To XML";
            this.btnSaveProfiles.Name = "btnSaveProfiles";
            this.btnSaveProfiles.ShowImage = true;
            this.btnSaveProfiles.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSaveProfiles_Click);
            // 
            // helpGroup
            // 
            this.helpGroup.Items.Add(this.btnHelp);
            this.helpGroup.Items.Add(this.btnPurchase);
            this.helpGroup.Items.Add(this.lblTrial);
            this.helpGroup.Name = "helpGroup";
            // 
            // btnHelp
            // 
            this.btnHelp.Label = "Help";
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.OfficeImageId = "Help";
            this.btnHelp.ShowImage = true;
            this.btnHelp.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnHelp_Click);
            // 
            // btnPurchase
            // 
            this.btnPurchase.Image = global::Addin.Properties.Resources.upload;
            this.btnPurchase.Label = "How to Buy";
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.ShowImage = true;
            this.btnPurchase.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnPurchase_Click);
            // 
            // lblTrial
            // 
            this.lblTrial.Label = "0 days left";
            this.lblTrial.Name = "lblTrial";
            // 
            // btnLoadFromReg
            // 
            this.btnLoadFromReg.Image = global::Addin.Properties.Resources.import_xml;
            this.btnLoadFromReg.Label = "Load Exported Registry";
            this.btnLoadFromReg.Name = "btnLoadFromReg";
            this.btnLoadFromReg.ShowImage = true;
            this.btnLoadFromReg.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnLoadFromReg_Click);
            // 
            // AttenderRibbon
            // 
            this.Name = "AttenderRibbon";
            this.RibbonType = "Microsoft.Visio.Drawing";
            this.Tabs.Add(this.attenderTab);
            this.Close += new System.EventHandler(this.AttenderRibbon_Close);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.AttenderRibbon_Load);
            this.attenderTab.ResumeLayout(false);
            this.attenderTab.PerformLayout();
            this.cicConfigurationGroup.ResumeLayout(false);
            this.cicConfigurationGroup.PerformLayout();
            this.logFileGroup.ResumeLayout(false);
            this.logFileGroup.PerformLayout();
            this.attendantGroup.ResumeLayout(false);
            this.attendantGroup.PerformLayout();
            this.helpGroup.ResumeLayout(false);
            this.helpGroup.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab attenderTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup cicConfigurationGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup attendantGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnImportProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSaveProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup logFileGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLoadProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonEditBox callIdEditBox;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup helpGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnHelp;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblTrial;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnPurchase;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLoadFromReg;
    }

    partial class ThisRibbonCollection
    {
        internal AttenderRibbon AttenderRibbon
        {
            get { return this.GetRibbon<AttenderRibbon>(); }
        }
    }
}