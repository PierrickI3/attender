﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Addin
{
    public partial class CICConfigurationForm : Form
    {
        public string DomainUsername { get; set; }
        public string Password { get; set; }
        public string CICServer { get; set; }

        public CICConfigurationForm()
        {
            InitializeComponent();
            txtDomainUsername.Focus();
        }

        public void UpdateValues()
        {
            txtDomainUsername.Text = DomainUsername;
            txtPassword.Text = Password;
            txtCICServer.Text = CICServer;
        }

        private void CICConfigurationForm_Load(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void EnableControls()
        {
            btnOK.Enabled = !String.IsNullOrEmpty(txtDomainUsername.Text) && !String.IsNullOrEmpty(txtPassword.Text) && !String.IsNullOrEmpty(txtCICServer.Text);
        }

        private void txtCICUsername_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void txtCICPassword_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void txtCICServer_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DomainUsername = txtDomainUsername.Text;
            Password = txtPassword.Text;
            CICServer = txtCICServer.Text;

            Close();
        }
    }
}