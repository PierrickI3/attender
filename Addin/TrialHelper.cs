﻿using Microsoft.VisualBasic;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Addin
{
    public class TrialHelper
    {
        const string APP_IDENTIFIER = "282";
        const int TRIAL_PERIOD_DAYS = 30;

        #region Private Variables

        private string _baseString;
        private string _serial;
        private string _RegFilePath;
        private string _HideFilePath;
        private int _DefDays;
        private string _Text;
        private string _SoftName;
        private string _Identifier;

        #endregion

        #region Properties

        /// <summary>
        /// Indicate File path for storing password
        /// </summary>
        public string RegFilePath
        {
            get
            {
                return _RegFilePath;
            }
            set
            {
                _RegFilePath = value;
            }
        }

        /// <summary>
        /// Indicate file path for storing hidden information
        /// </summary>
        public string HideFilePath
        {
            get
            {
                return _HideFilePath;
            }
            set
            {
                _HideFilePath = value;
            }
        }

        /// <summary>
        /// Get default number of days for trial period
        /// </summary>
        public int TrialPeriodDays
        {
            get
            {
                return _DefDays;
            }
        }

        /// <summary>
        /// Get or Set TripleDES key for encrypting files to save
        /// </summary>
        public byte[] TripleDESKey
        {
            get
            {
                return FileReadWrite.key;
            }
            set
            {
                FileReadWrite.key = value;
            }
        }

        #region Usage Properties

        public bool UseProcessorID
        {
            get
            {
                return SystemInfo.UseProcessorID;
            }
            set
            {
                SystemInfo.UseProcessorID = value;
            }
        }

        public bool UseBaseBoardProduct
        {
            get
            {
                return SystemInfo.UseBaseBoardProduct;
            }
            set
            {
                SystemInfo.UseBaseBoardProduct = value;
            }
        }

        public bool UseBaseBoardManufacturer
        {
            get
            {
                return SystemInfo.UseBiosManufacturer;
            }
            set
            {
                SystemInfo.UseBiosManufacturer = value;
            }
        }

        public bool UseDiskDriveSignature
        {
            get
            {
                return SystemInfo.UseDiskDriveSignature;
            }
            set
            {
                SystemInfo.UseDiskDriveSignature = value;
            }
        }

        public bool UseVideoControllerCaption
        {
            get
            {
                return SystemInfo.UseVideoControllerCaption;
            }
            set
            {
                SystemInfo.UseVideoControllerCaption = value;
            }
        }

        public bool UsePhysicalMediaSerialNumber
        {
            get
            {
                return SystemInfo.UsePhysicalMediaSerialNumber;
            }
            set
            {
                SystemInfo.UsePhysicalMediaSerialNumber = value;
            }
        }

        public bool UseBiosVersion
        {
            get
            {
                return SystemInfo.UseBiosVersion;
            }
            set
            {
                SystemInfo.UseBiosVersion = value;
            }
        }

        public bool UseBiosManufacturer
        {
            get
            {
                return SystemInfo.UseBiosManufacturer;
            }
            set
            {
                SystemInfo.UseBiosManufacturer = value;
            }
        }

        public bool UseWindowsSerialNumber
        {
            get
            {
                return SystemInfo.UseWindowsSerialNumber;
            }
            set
            {
                SystemInfo.UseWindowsSerialNumber = value;
            }
        }

        #endregion

        #endregion

        public TrialHelper()
        {
            SystemInfo.UseBaseBoardManufacturer = true;
            SystemInfo.UseBaseBoardProduct = true;
            SystemInfo.UseBiosManufacturer = true;
            SystemInfo.UseBiosVersion = true;
            SystemInfo.UseDiskDriveSignature = false;
            SystemInfo.UsePhysicalMediaSerialNumber = false;
            SystemInfo.UseProcessorID = true;
            SystemInfo.UseVideoControllerCaption = false;
            SystemInfo.UseWindowsSerialNumber = true;

            _SoftName = "AttenderVisioAddin";
            _RegFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "RegFile.reg");
            _HideFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TMSetup.dbf");
            _Text = "Thank you for using Attender";
            _DefDays = TRIAL_PERIOD_DAYS;
            _Identifier = APP_IDENTIFIER;
        }

        public string GetBaseString()
        {
            _baseString = Encryption.Boring(Encryption.InverseByBase(SystemInfo.GetSystemInfo(_SoftName), 10));
            return _baseString;
        }
        public string GetSerial()
        {
            if (String.IsNullOrEmpty(_baseString))
                GetBaseString();

            _serial = Encryption.MakeSerial(_baseString, _Identifier);
            return StandardizeString(_serial);
        }

        private string StandardizeString(string stringToStandardize)
        {
            return Regex.Replace(stringToStandardize, @"^(.....)(.....)(.....)(.....)(.....)$", "$1-$2-$3-$4-$5");
        }

        public bool HasRegistered()
        {
            GetSerial();
            return CheckRegister();
        }
        public bool HasTrialExpired()
        {
            return DaysToEnd() <= 0;
        }

        /// <summary>
        /// Show registering dialog to user
        /// </summary>
        /// <returns>Type of running</returns>
        public RunTypes ShowDialog()
        {
            // check if registered before
            if (CheckRegister() == true)
                return RunTypes.Full;

            GetSerial();

            frmDialog PassDialog = new frmDialog(_baseString, _serial, DaysToEnd(), _Text);

            MakeHideFile();

            DialogResult DR = PassDialog.ShowDialog();

            if (DR == System.Windows.Forms.DialogResult.OK)
            {
                MakeRegFile();
                return RunTypes.Full;
            }
            else if (DR == DialogResult.Cancel)
            {
                return RunTypes.Unknown;
            }
            else if (DR == DialogResult.Retry)
            {
                return RunTypes.Trial;
            }
            else
            {
                return RunTypes.Expired;
            }
        }

        /// <summary>
        /// Saves password to Registration file for next time usage
        /// </summary>
        private void MakeRegFile()
        {
            FileReadWrite.WriteFile(_RegFilePath, _serial);
        }

        /// <summary>
        /// Controls Registeration file for serial 
        /// </summary>
        /// <returns>if password saved correctly, returns true</returns>
        private bool CheckRegister()
        {
            string serial = FileReadWrite.ReadFile(_RegFilePath);

            if (_serial == serial)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Indicates how many days can user use program 
        /// If the file does not exists, create it
        /// </summary>
        /// <returns>Days left until trial expires</returns>
        public int DaysToEnd()
        {
            FileInfo hf = new FileInfo(_HideFilePath);
            if (hf.Exists == false)
            {
                MakeHideFile();
                return _DefDays;
            }
            return CheckHideFile();
        }

        /// <summary>
        /// Stores hidden information to hidden file
        /// Date,DaysToEnd,BaseString(ComputerID)
        /// </summary>
        private void MakeHideFile()
        {
            string HideInfo;
            HideInfo = DateTime.Now.Ticks + ";";
            HideInfo += _DefDays + ";" + _baseString;
            FileReadWrite.WriteFile(_HideFilePath, HideInfo);
        }

        /// <summary>
        /// Gets data from hidden file if it exists
        /// </summary>
        /// <returns>Number of trial days left</returns>
        private int CheckHideFile()
        {
            string[] HideInfo;
            HideInfo = FileReadWrite.ReadFile(_HideFilePath).Split(';');
            long DiffDays;
            int DaysToEnd;

            if (_baseString == HideInfo[2])
            {
                DaysToEnd = Convert.ToInt32(HideInfo[1]);
                if (DaysToEnd <= 0)
                {
                    _DefDays = 0;
                    return 0;
                }
                DateTime dt = new DateTime(Convert.ToInt64(HideInfo[0]));
                DiffDays = DateAndTime.DateDiff(DateInterval.Day, dt.Date, DateTime.Now.Date, FirstDayOfWeek.Saturday, FirstWeekOfYear.FirstFullWeek);

                DaysToEnd = Convert.ToInt32(HideInfo[1]);

                DiffDays = Math.Abs(DiffDays);

                _DefDays = DaysToEnd - Convert.ToInt32(DiffDays);
            }
            return _DefDays;
        }

        public enum RunTypes
        {
            Trial = 0,
            Full,
            Expired,
            Unknown
        }
    }
}
