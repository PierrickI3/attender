﻿using System;
using System.Windows.Forms;

namespace Addin
{
    public partial class ProgressDialog : Form
    {
        delegate void ShowMessageCallback(string message);
        public event EventHandler<EventArgs> Cancelled;

        public string Message
        {
            set { lblMessage.Text = value; }
        }

        public ProgressDialog()
        {
            InitializeComponent();
            progressBar1.MarqueeAnimationSpeed = 5;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            EventHandler<EventArgs> cancelledEventHandler = Cancelled;

            if (cancelledEventHandler != null)
                cancelledEventHandler(this, e);
        }
    }
}