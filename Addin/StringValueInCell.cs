﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Addin
{
    public class StringValueInCell
    {
        public StringValueInCell()
        {

            // No initialization is required.
        }

        /// <summary>This method sets the value of the specified Visio cell
        /// to the new string passed as a parameter.</summary>
        /// <param name="formulaCell">Cell in which the value is to be set
        /// </param>
        /// <param name="newValue">New string value that will be set</param>
        public void SetCellValueToString(Microsoft.Office.Interop.Visio.Cell formulaCell, string newValue)
        {

            try
            {

                // Set the value for the cell.
                formulaCell.FormulaU = StringToFormulaForString(newValue);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
                throw;
            }
        }

        /// <summary>This method converts the input string to a Visio string by
        /// replacing each double quotation mark (") with a pair of double
        /// quotation marks ("") and then adding double quotation marks around
        /// the entire string.</summary>
        /// <param name="inputValue">Input string that will be converted
        /// to Visio String</param>
        /// <returns>Converted Visio string</returns>
        public static string StringToFormulaForString(string inputValue)
        {

            string result = "";
            string quote = "\"";
            string quoteQuote = "\"\"";

            try
            {

                result = inputValue != null ? inputValue : String.Empty;

                // Replace all (") with ("").
                result = result.Replace(quote, quoteQuote);

                // Add ("") around the whole string.
                result = quote + result + quote;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
                throw;
            }

            return result;
        }
    }
}