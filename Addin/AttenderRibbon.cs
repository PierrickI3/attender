﻿using Helper;
using Microsoft.Office.Interop.Visio;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Visio = Microsoft.Office.Interop.Visio;

namespace Addin
{
    public partial class AttenderRibbon
    {
        const string ENCRYPTION_KEY = "A!!3nd3r&";
        const string BASIC_FLOWCHART_STENCIL_VISIO2013 = "BASFLO_M.VSSX";
        const string BASIC_FLOWCHART_STENCIL_VISIO2010 = "BASFLO_M.VSS";
        const string DYNAMIC_CONNECTOR_MASTER = "Dynamic Connector";
        const string MESSAGE_NOT_SAME_PAGE = "Both shapes are not on the same page.";

        #region Fields and Properties

        private bool _isLicensed = false;

        public string DomainUsername { get; private set; }
        public string Password { get; private set; }
        public string CICServer { get; private set; }
        public string Serial { get; private set; }

        private TrialHelper _trialHelper = new TrialHelper();

        private WMIHelper _attender;

        private ISourceReader _reader;

        private BackgroundWorker _backgroundWorker;
        private ProgressDialog _progressDialog;

        private List<AttendantProfile> _allProfiles;
        public List<AttendantProfile> AllProfiles
        {
            get
            {
                if (_allProfiles == null)
                {
                    _allProfiles = new List<AttendantProfile>();
                }
                return _allProfiles;
            }
            set
            {
                _allProfiles = value;
            }
        }

        private List<Visio.Shape> _shapes = new List<Visio.Shape>();
        private Visio.Page _currentPage = null;

        private Configuration _configuration = null;

        Random RandomLocation = new Random(); // Randomizing the placement of shapes helps lay out shapes more efficiently when the layout method is called for this page.
        Visio.Document FlowchartStencil;

        #endregion

        #region Constructor (Startup) and Initialization

        private void AttenderRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            LoadConfiguration();

            CheckLicense(false);
        }
        private void LoadConfiguration()
        {
            _configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (_configuration.AppSettings.Settings.Count == 0)
            {
                return;
            }

            if (_configuration.AppSettings.Settings.AllKeys.Contains("LastUsedDomainUsername"))
            {
                DomainUsername = _configuration.AppSettings.Settings["LastUsedDomainUsername"].Value;
            }

            if (_configuration.AppSettings.Settings.AllKeys.Contains("LastUsedICServer"))
            {
                CICServer = _configuration.AppSettings.Settings["LastUsedICServer"].Value;
            }

            if (_configuration.AppSettings.Settings.AllKeys.Contains("LastUsedPassword"))
            {
                var encryptor = new Encryptor();
                Password = _configuration.AppSettings.Settings["LastUsedPassword"].Value;
                if (!String.IsNullOrEmpty(Password))
                {
                    Password = encryptor.DecryptFromBase64String(Password, ENCRYPTION_KEY);
                }
            }

            if (_configuration.AppSettings.Settings.AllKeys.Contains("LastUsedCallId"))
            {
                callIdEditBox.Text = _configuration.AppSettings.Settings["LastUsedCallId"].Value;
            }

            if (_configuration.AppSettings.Settings.AllKeys.Contains("Serial"))
            {
                Serial = _configuration.AppSettings.Settings["Serial"].Value;
            }
        }
        private void SaveConfigurationSettings()
        {
            _configuration.AppSettings.Settings["LastUsedDomainUsername"].Value = DomainUsername;
            _configuration.AppSettings.Settings["LastUsedICServer"].Value = CICServer;

            var encryptor = new Encryptor();
            var encryptedPassword = encryptor.EncryptToBase64String(Password, ENCRYPTION_KEY);
            _configuration.AppSettings.Settings["LastUsedPassword"].Value = encryptedPassword;

            _configuration.AppSettings.Settings["LastUsedCallId"].Value = callIdEditBox.Text;

            _configuration.Save();
        }
        private void InitAttenderHelper()
        {
            _attender = new WMIHelper(DomainUsername, Password, CICServer);
        }
        private void ReadAttendantInformation()
        {
            var attendantProfiles = _attender.GetAttendantProfiles(null);
            AllProfiles = new List<AttendantProfile>(attendantProfiles);
            _attender.GetAttendantInformation(AllProfiles.Where(p => p.IsSelected));
        }

        #endregion

        #region Ribbon Buttons

        private void btnImportProfiles_Click(object sender, RibbonControlEventArgs e)
        {
            if (!CheckLicense(true))
            {
                return;
            }

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.WorkerReportsProgress = true;

            _backgroundWorker.DoWork += (s, eventArgs) =>
            {
                try
                {
                    EnableInterface(false);

                    _backgroundWorker.ReportProgress(10, "Initializing...");
                    InitAttenderHelper();

                    _attender.ReadingProfile += (readingSender, readingEventArgs) =>
                    {
                        _backgroundWorker.ReportProgress(0, "Profile: " + readingEventArgs.ItemName);
                    };
                    _attender.ReadingSchedule += (readingSender, readingEventArgs) =>
                    {
                        _backgroundWorker.ReportProgress(0, "Schedule: " + readingEventArgs.ItemName);
                    };
                    _attender.ReadingAction += (readingSender, readingEventArgs) =>
                    {
                        _backgroundWorker.ReportProgress(0, "Action: " + readingEventArgs.ItemName);
                    };

                    _backgroundWorker.ReportProgress(10, "Reading Attendant Profiles...");
                    ReadAttendantInformation();

                    _backgroundWorker.ReportProgress(10, "Drawing profiles");

                    if (_backgroundWorker.CancellationPending)
                        eventArgs.Cancel = true;

                    DrawAttendant();
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("Credentials are invalid. Please start again.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    EnableInterface(true);
                }
            };

            _backgroundWorker.ProgressChanged += (s, eventArgs) =>
            {
                _progressDialog.Message = eventArgs.UserState as String;
            };

            _backgroundWorker.RunWorkerCompleted += (s, args) =>
            {
                _progressDialog.Close();
                EnableInterface(true);
            };

            _progressDialog = new ProgressDialog();
            _progressDialog.Cancelled += (cancelSender, cancelEventArgs) =>
            {
                if (_backgroundWorker.WorkerSupportsCancellation == true)
                {
                    _backgroundWorker.CancelAsync();
                    _progressDialog.Close();
                    EnableInterface(true);
                }
            };

            var cicConfigurationForm = new CICConfigurationForm();
            cicConfigurationForm.DomainUsername = DomainUsername;
            cicConfigurationForm.Password = Password;
            cicConfigurationForm.CICServer = CICServer;
            cicConfigurationForm.UpdateValues();

            if (cicConfigurationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DomainUsername = cicConfigurationForm.DomainUsername;
                Password = cicConfigurationForm.Password;
                CICServer = cicConfigurationForm.CICServer;

                SaveConfigurationSettings();

                _progressDialog.Show();
                _backgroundWorker.RunWorkerAsync();
            }
            else
            {
                EnableInterface(true);
            }
        }

        #endregion

        #region Draw

        private void DrawAttendant()
        {
            var document = Globals.ThisAddIn.Application.Documents.Add("");
            var page = document.Pages.Add();
            _currentPage = page;
            _shapes.Clear();

            DrawAttendant(page);
        }
        private void DrawAttendant(Visio.Page page)
        {
            try
            {
                DropAllShapes(page);

                //TODO Offer several layouts?
                var layout = new short[]
                {
                    (short)Visio.VisCellVals.visLOPlaceHierarchyLeftToRightMiddle,
                    (short)Visio.VisCellVals.visPLOPlaceDepthMedium,
                    (short)Visio.VisCellVals.visLORouteOrgChartNS,
                    (short)Visio.VisCellVals.visLORouteExtStraight
                };

                // Set Layout and Routing property
                SetPageLayoutAndRouting(page, (short)Visio.VisCellIndices.visPLOPlaceStyle, layout[0]);

                // Set PlaceDepth property.
                SetPageLayoutAndRouting(page, (short)Visio.VisCellIndices.visPLOPlaceDepth, layout[1]);

                // Set RouteStyle property.
                SetPageLayoutAndRouting(page, (short)Visio.VisCellIndices.visPLORouteStyle, layout[2]);

                // Set LineRouteExt property.
                SetPageLayoutAndRouting(page, (short)Visio.VisCellIndices.visPLOLineRouteExt, layout[3]);

                // Perform the layout for the current page.
                page.Layout();

                // Page size & zoom
                page.ResizeToFitContents();
                Globals.ThisAddIn.Application.ActiveWindow.Zoom = -1.0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// This method sets the value of a cell in the Page Layout
        /// section of the PageSheet.
        /// </summary>
        /// <param name="targetPage">Reference to the page that will be used</param>
        /// <param name="cellIndex">Index of the cell in the Page Layout section that will be set</param>
        /// <param name="cellValue">Value to be set in the cell</param>
        public static void SetPageLayoutAndRouting(Visio.Page targetPage, short cellIndex, int cellValue)
        {
            try
            {
                // Set the value of the cell in the Page Layout section.
                targetPage.PageSheet.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowPageLayout, (short)cellIndex).ResultIU = (double)cellValue;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
                throw;
            }
        }

        #region Shapes

        private void DropAllShapes(Page targetPage)
        {
            Debug.WriteLine("Dropping all shapes...");

            #region Validation

            if (targetPage == null)
            {
                return;
            }

            #endregion

            if (CheckForCancellation()) return;

            #region Optimize Visio

            var visioApplication = targetPage.Application;
            var undoEnabledPrevious = visioApplication.UndoEnabled;
            visioApplication.UndoEnabled = false;
            visioApplication.ScreenUpdating = 0;
            visioApplication.ShowChanges = false;

            #endregion

            #region Drop Shapes

            var itemsCount = 0;

            //Array initialization
            Array objectsToInstance = Array.CreateInstance(typeof(object), 1);
            Array xyArray = Array.CreateInstance(typeof(double), 2);

            FlowchartStencil = GetBasicFlowchartStencil(BASIC_FLOWCHART_STENCIL_VISIO2013, BASIC_FLOWCHART_STENCIL_VISIO2010, targetPage.Application);

            #region Add Root

            itemsCount++;
            var rootMaster = FlowchartStencil.Masters.get_ItemU("Process");
            objectsToInstance.SetValue(rootMaster, itemsCount - 1);
            xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
            xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

            #endregion
            Debug.WriteLine("Number of profiles: {0}", AllProfiles.Count);
            if (AllProfiles.Count == 0)
            {
                MessageBox.Show("No profiles found");
                return;
            }

            foreach (var profile in AllProfiles)
            {
                Debug.WriteLine("Adding object for Current Profile: {0}", profile.Name);

                #region Add Profiles

                itemsCount++;

                //Resize Array
                objectsToInstance = ResizeArray(objectsToInstance, itemsCount);
                xyArray = ResizeArray(xyArray, itemsCount * 2);

                //Add Shape
                var profileMaster = FlowchartStencil.Masters.get_ItemU("Process");
                objectsToInstance.SetValue(profileMaster, itemsCount - 1);
                xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);
                profile.ShapeId = itemsCount - 1;

                #endregion

                foreach (var schedule in profile.Schedules)
                {
                    Debug.WriteLine("Adding object for Current Schedule: {0}", schedule.Name);

                    #region Add Schedules

                    itemsCount++;

                    //Resize Array
                    objectsToInstance = ResizeArray(objectsToInstance, itemsCount);
                    xyArray = ResizeArray(xyArray, itemsCount * 2);

                    //Add Shape
                    var scheduleMaster = FlowchartStencil.Masters.get_ItemU("Process");
                    objectsToInstance.SetValue(scheduleMaster, itemsCount - 1);
                    xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                    xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);
                    schedule.ShapeId = itemsCount - 1;

                    #endregion

                    foreach (var action in schedule.ActionList)
                    {
                        Debug.WriteLine("Adding object for Current Action: {0}", action.Name);

                        #region Add Actions

                        itemsCount++;

                        //Resize Array
                        objectsToInstance = ResizeArray(objectsToInstance, itemsCount);
                        xyArray = ResizeArray(xyArray, itemsCount * 2);

                        //Add Shape
                        Master actionMaster;
                        if (action is LogicalTransferAction)
                        {
                            actionMaster = FlowchartStencil.Masters.get_ItemU("Decision");
                        }
                        else
                        {
                            actionMaster = FlowchartStencil.Masters.get_ItemU("Process");
                        }

                        objectsToInstance.SetValue(actionMaster, itemsCount - 1);
                        xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                        xyArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);
                        action.ShapeId = itemsCount - 1;

                        #endregion
                    }
                }
            }


            if (CheckForCancellation()) return;

            //Drop!
            targetPage.DropMany(ref objectsToInstance, ref xyArray, out Array shapesIdArray);

            #endregion

            #region Set Shape properties

            if (CheckForCancellation()) return;

            var _vShapes = targetPage.Shapes;

            #region Set Root Text

            var rootShape = _vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(0)));
            rootShape.Text = "Root";
            rootShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 24); //8
            _shapes.Add(rootShape);

            #endregion

            foreach (var profile in AllProfiles)
            {
                if (CheckForCancellation()) return;

                #region Set Profile Properties

                var profileShape = _vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(profile.ShapeId)));

                profileShape.Text = profile.Name;
                if (_attender == null)
                {
                    _attender = new WMIHelper();
                }
                profileShape.Data1 = _attender.FormatRegistryPath(profile.FullNodePath);
                profileShape.Data2 = String.Empty;

                profileShape.get_CellsSRC(
                (short)Visio.VisSectionIndices.visSectionObject,
                (short)Visio.VisRowIndices.visRowFill,
                (short)Visio.VisCellIndices.visFillForegnd)
                .FormulaU = "RGB(255,0,0)";

                profileShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

                _shapes.Add(profileShape);

                #endregion

                foreach (var schedule in profile.Schedules)
                {
                    if (CheckForCancellation()) return;

                    #region Set Schedule Properties

                    var scheduleShape = _vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(schedule.ShapeId)));

                    scheduleShape.Text = schedule.Name;
                    scheduleShape.Data1 = _attender.FormatRegistryPath(schedule.FullNodePath);
                    scheduleShape.Data2 = _attender.FormatRegistryPath(profile.FullNodePath);

                    scheduleShape.get_CellsSRC(
                    (short)Visio.VisSectionIndices.visSectionObject,
                    (short)Visio.VisRowIndices.visRowFill,
                    (short)Visio.VisCellIndices.visFillForegnd)
                    .FormulaU = "RGB(0,255,0)";

                    scheduleShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

                    _shapes.Add(scheduleShape);

                    #endregion

                    foreach (var action in schedule.ActionList)
                    {
                        if (CheckForCancellation()) return;

                        #region Set Action Properties

                        var actionShape = _vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(action.ShapeId)));

                        actionShape.Data1 = action.FullNodePath;
                        actionShape.Data2 = action.ParentFullNodePath;

                        if (action is OtherToolsAction)
                        {
                            actionShape.Text = action.Name + " (" + action.Type + ")";
                        }
                        else if (action is AgentTransferAction)
                        {
                            actionShape.Text = action.Name;

                            var agentTransferAction = action as AgentTransferAction;
                            switch (agentTransferAction.QueueType)
                            {
                                case "User":
                                    actionShape.Text += "\n(" + agentTransferAction.UserQueue + ")";
                                    break;
                                case "Station":
                                    actionShape.Text += "\n(" + agentTransferAction.StationQueue + ")";
                                    break;
                                case "Dynamic":
                                    actionShape.Text += "\n(Attribute)";
                                    break;
                            }
                        }
                        else if (action is ExternalTransferAction)
                        {
                            actionShape.Text = action.Name;

                            var externalTransferAction = action as ExternalTransferAction;
                            switch (externalTransferAction.TransferType)
                            {
                                case "Number":
                                    actionShape.Text += "\n(" + externalTransferAction.Number + ")";
                                    break;
                                case "Static":
                                    actionShape.Text += "\n(Dynamic)";
                                    break;
                            }

                            actionShape.Text += " (" + externalTransferAction.Type + ")";
                        }
                        else if (action is LogicalTransferAction)
                        {
                            actionShape.Text = action.Name + " (" + action.Type + ")";

                            actionShape.get_CellsSRC(
                                (short)Visio.VisSectionIndices.visSectionObject,
                                (short)Visio.VisRowIndices.visRowFill,
                                (short)Visio.VisCellIndices.visFillForegnd)
                                .FormulaU = "RGB(127,127,255)";
                        }
                        else
                        {
                            actionShape.Text = action.Name + " (" + action.Type + ")";
                        }

                        actionShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

                        _shapes.Add(actionShape);

                        #endregion
                    }
                }
            }

            #endregion

            #region Drop Connectors

            // At this point, all shapes have been dropped on the page. It is time to connect them together
            Debug.WriteLine("Dropping connectors...");
            var connectors = new List<Connector>();
            itemsCount = 0;

            //Initialize Arrays
            Array connectorsToInstance = Array.CreateInstance(typeof(object), itemsCount);
            Array connectorsXYArray = Array.CreateInstance(typeof(double), itemsCount * 2);
            Array connectorsIdArray = null;

            foreach (var currentAttendantProfile in AllProfiles)
            {
                Debug.WriteLine("Current Attendant Profile: {0}", currentAttendantProfile.Name);

                #region Root to Profile connectors

                if (CheckForCancellation()) return;

                itemsCount++;

                //Resize Array
                connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                //Add Connector Shape
                var connectorMaster = FlowchartStencil.Masters.get_ItemU(DYNAMIC_CONNECTOR_MASTER);
                connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                var rootConnector = new Connector(itemsCount - 1, 0, currentAttendantProfile.ShapeId, GetBoolResultFromStringValue(currentAttendantProfile.UseDNIS) ? String.Format("DNIS: {0}", currentAttendantProfile.DNISString) : GetBoolResultFromStringValue(currentAttendantProfile.UseANI) ? String.Format("ANI: {0}", currentAttendantProfile.ANIString) : String.Empty);
                connectors.Add(rootConnector);

                #endregion

                foreach (var currentSchedule in currentAttendantProfile.Schedules)
                {
                    Debug.WriteLine("Current Schedule: {0}", currentSchedule.Name);

                    #region Profile to Schedule connectors

                    if (CheckForCancellation()) return;

                    itemsCount++;

                    //Resize Array
                    connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                    connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                    //Add Connector Shape
                    connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                    var scheduleConnector = new Connector(itemsCount - 1, currentAttendantProfile.ShapeId, currentSchedule.ShapeId, String.Empty);
                    connectors.Add(scheduleConnector);

                    #endregion

                    #region Schedule Fax Receiver

                    //If a fax receiver is set, add a link to it
                    //TODO Replace scheduleInformation.FaxQueueType below with correct property for the fax receiver (when able to have a user or workgroup mailbox)
                    if (currentSchedule.FaxListener.Equals("Yes", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //Since a new shape is being created, I can't use the arrays for this
                        var faxListenerShape = DropAction(targetPage, "Fax Recipient", currentSchedule.FullNodePath + "\\FaxRecipient", currentSchedule.FullNodePath);
                        AddLink(currentSchedule.FullNodePath, faxListenerShape, "Fax Detected");
                    }

                    #endregion

                    #region Schedule Error Path

                    //Show error path for the schedule
                    switch (currentSchedule.ErrorType)
                    {
                        case "Disconnect":
                            var disconnectShape = DropAction(targetPage, "Disconnect", currentSchedule.FullNodePath + "\\Disconnect", currentSchedule.FullNodePath);
                            AddLink(currentSchedule.FullNodePath, disconnectShape, "On Error");
                            break;
                        case "Queue Transfer":
                            var queueTransferShape = DropAction(targetPage, "Queue Transfer", currentSchedule.FullNodePath + "\\QueueTransfer", currentSchedule.FullNodePath);
                            AddLink(currentSchedule.FullNodePath, queueTransferShape, "On Error");
                            break;
                        case "Menu Transfer":
                            //No need to create a shape for it. The destination is a known location.
                            AddLink(currentSchedule.FullNodePath, currentSchedule.Menu, "On Error");
                            break;
                    }

                    #endregion

                    #region Actions

                    foreach (var action in currentSchedule.ActionList)
                    {
                        Debug.WriteLine("Action Name: {0}", action.Name);
                        if (CheckForCancellation()) return;

                        #region Link to the next action (JumpNode)

                        //If in a multi-action container, the next location is set in the NextLocation property and not in the JumpLocation property
                        if (!String.IsNullOrEmpty(action.NextLocation))
                        {
                            action.JumpLocation = action.NextLocation;
                        }

                        if (action.JumpNode != null && GetBoolResultFromStringValue(action.JumpNode))
                        {
                            Debug.WriteLine("Action jump tye: {0} => {1}", action.Name, action.JumpToType);

                            switch (action.JumpToType)
                            {
                                case "Custom":
                                    itemsCount++;

                                    //Resize Array
                                    connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                                    connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                                    //Add Connector Shape
                                    connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                                    var jumpShapeId = GetShapeId(action.JumpLocation);

                                    var customConnector = new Connector(itemsCount - 1, action.ShapeId, jumpShapeId, String.Empty);
                                    connectors.Add(customConnector);
                                    break;
                                case "Operator":
                                    var operatorShape = DropAction(targetPage, "Operator", action.FullNodePath + "\\Operator", action.FullNodePath);
                                    AddLink(action.FullNodePath, operatorShape, "Operator");
                                    break;
                                case "Previous":
                                    // Create a new box for the Previous Menu as it is only possible to figure it out by accessing the inin logs or IVR database logs.
                                    var previousMenuShape = DropAction(targetPage, "Previous Menu", action.FullNodePath + "\\PreviousMenu", action.FullNodePath);
                                    AddLink(action.FullNodePath, previousMenuShape, "Previous Menu");
                                    break;
                                case "Next":
                                    AddLink(action.FullNodePath, action.NextLocation, String.Empty);
                                    break;
                                case "Disconnect":
                                    var disconnectShape = DropAction(targetPage, "Disconnect", action.FullNodePath + "\\Disconnect", action.FullNodePath);
                                    AddLink(action.FullNodePath, disconnectShape, String.Empty);
                                    break;
                            }
                        }

                        #endregion

                        #region Special Actions Paths (i.e. Logical Transfer, Menu Transfer)

                        if (action is LogicalTransferAction)
                        {
                            Debug.WriteLine("Action is logical transfer: {0}", action.Name);
                            var logicalTransferAction = action as LogicalTransferAction;

                            itemsCount++;

                            //Resize Array
                            connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                            connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                            //Add Connector Shape
                            connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                            //True
                            var trueShapeId = GetShapeId(logicalTransferAction.OnTrue);
                            var trueConnector = new Connector(itemsCount - 1, action.ShapeId, trueShapeId, "True");
                            connectors.Add(trueConnector);

                            itemsCount++;

                            //Resize Array
                            connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                            connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                            //Add Connector Shape
                            connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                            //False
                            var falseShapeId = GetShapeId(logicalTransferAction.OnFalse);
                            var falseConnector = new Connector(itemsCount - 1, action.ShapeId, falseShapeId, "False");
                            connectors.Add(falseConnector);

                            //AddLink(logicalTransferAction.FullNodePath, logicalTransferAction.OnTrue, "True");
                            //AddLink(logicalTransferAction.FullNodePath, logicalTransferAction.OnFalse, "False");
                        }
                        else if (action is MenuTransferAction)
                        {
                            Debug.WriteLine("Action is menu transfer: {0}", action.Name);
                            var menuTransferAction = action as MenuTransferAction;

                            itemsCount++;

                            //Resize Array
                            connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                            connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                            //Add Connector Shape
                            connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                            var jumpShapeId = GetShapeId(menuTransferAction.Menu);
                            var menuConnector = new Connector(itemsCount - 1, action.ShapeId, jumpShapeId, "Menu Transfer");
                            connectors.Add(menuConnector);

                            //AddLink(menuTransferAction.FullNodePath, menuTransferAction.Menu, "Menu Transfer");
                        }
                        else if (action is WorkgroupTransferAction)
                        {
                            Debug.WriteLine("Action is workgroup transfer: {0}", action.Name);
                            //Add link to first child (childindex 00000)
                            //Children should be linked to each other in childindex order

                            Helper.Action previousChild = null;
                            foreach (var childAction in currentSchedule.ActionList.Where(a => a.FullNodePath.StartsWith(action.FullNodePath) && !a.FullNodePath.Equals(action.FullNodePath) && !String.IsNullOrEmpty(a.ChildIndex)).OrderBy(a => a.ChildIndex))
                            {
                                if (childAction.ChildIndex == "00000")
                                {
                                    itemsCount++;

                                    //Resize Array
                                    connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                                    connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                                    //Add Connector Shape
                                    connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                                    var jumpShapeId = GetShapeId(childAction.FullNodePath);
                                    var childConnector = new Connector(itemsCount - 1, action.ShapeId, jumpShapeId, childAction.ChildIndex);
                                    connectors.Add(childConnector);

                                    //AddLink(action.FullNodePath, childAction.FullNodePath, childAction.ChildIndex);
                                    previousChild = childAction;
                                    continue;
                                }

                                if (previousChild != null)
                                {
                                    itemsCount++;

                                    //Resize Array
                                    connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                                    connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                                    //Add Connector Shape
                                    connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                                    connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                                    var previousShapeId = GetShapeId(previousChild.FullNodePath);
                                    var jumpShapeId = GetShapeId(childAction.FullNodePath);
                                    var childConnector = new Connector(itemsCount - 1, previousShapeId, jumpShapeId, childAction.ChildIndex);
                                    connectors.Add(childConnector);

                                    //AddLink(previousChild.FullNodePath, childAction.FullNodePath, childAction.ChildIndex);
                                    previousChild = childAction;
                                }
                            }
                        }

                        #endregion

                        #region Link to Error path

                        //If an error path is set, add a link for it
                        if (!action.ErrorType.Equals("Default", StringComparison.InvariantCultureIgnoreCase) && !String.IsNullOrEmpty(action.ErrorJumpLocation))
                        {
                            Debug.WriteLine("Action has error path set: {0}", action.Name);
                            itemsCount++;

                            //Resize Array
                            connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                            connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                            //Add Connector Shape
                            connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                            var jumpShapeId = GetShapeId(action.ErrorJumpLocation);
                            var errorConnector = new Connector(itemsCount - 1, action.ShapeId, jumpShapeId, "Default");
                            connectors.Add(errorConnector);

                            //AddLink(action.FullNodePath, action.ErrorJumpLocation, "Default");
                        }

                        #endregion

                        // Only link the action to its parent if it's a default action or has a digit assigned
                        if (IsADefaultAction(action) || HasDigit(action))
                        {
                            Debug.WriteLine("Action is default or has digit: {0}", action.Name);
                            itemsCount++;

                            //Resize Array
                            connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                            connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                            //Add Connector Shape
                            connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                            connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                            var parentShapeId = GetShapeId(action.ParentFullNodePath);
                            var defaultConnector = new Connector(itemsCount - 1, parentShapeId, action.ShapeId, action.Digit.Replace("-", "(Default)"));
                            connectors.Add(defaultConnector);

                            //AddLink(action.ParentFullNodePath, action.FullNodePath, action.Digit.Replace("-", "(Default)"));
                        }
                        else if (IsChild(action))
                        {
                            Debug.WriteLine("Action is child: {0}", action.Name);
                            var parentAction = _attender.GetAction(action.ParentFullNodePath);
                            if (!(parentAction is ComplexOperationAction) && !(parentAction is WorkgroupTransferAction))
                            {
                                itemsCount++;

                                //Resize Array
                                connectorsToInstance = ResizeArray(connectorsToInstance, itemsCount);
                                connectorsXYArray = ResizeArray(connectorsXYArray, itemsCount * 2);

                                //Add Connector Shape
                                connectorsToInstance.SetValue(connectorMaster, itemsCount - 1);
                                connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2);
                                connectorsXYArray.SetValue(RandomLocation.NextDouble() * 10, (itemsCount - 1) * 2 + 1);

                                var parentShapeId = GetShapeId(action.ParentFullNodePath);
                                var childConnector = new Connector(itemsCount - 1, parentShapeId, action.ShapeId, String.Empty);
                                connectors.Add(childConnector);

                                //AddLink(action.ParentFullNodePath, action.FullNodePath, String.Empty);
                            }
                            else
                            {
                                Debug.WriteLine("Parent is a complex operation. Not linking.");
                            }
                        }

                        if (!String.IsNullOrEmpty(action.ChildIndex))
                        {
                            Debug.WriteLine("Action has children: {0}", action.Name);
                            //TODO This is a sub-entry of a "Transfer to workgroup". Options should be linked in order of childindex

                            //TODO Add a HasChildren() method to find out if "Workgroup transfer" item has children
                            //TODO Add actions using the "childindex" as order
                            //TODO Link childindex 00000 to parent (Workgroup transfer action)
                        }
                    }

                    #endregion
                }
            }

            if (CheckForCancellation()) return;

            //Drop Connectors!
            targetPage.DropMany(ref connectorsToInstance, ref connectorsXYArray, out connectorsIdArray);

            #endregion

            #region Set Connector properties and glue them

            _vShapes = targetPage.Shapes;

            foreach (var connector in connectors)
            {
                var connectorShape = _vShapes.get_ItemFromID(Convert.ToInt16(connectorsIdArray.GetValue(connector.Id)));
                connectorShape.Text = connector.Text;

                // Connect the begin point of the dynamic connector to the PinX cell of the first 2-D shape.
                var beginX = connectorShape.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXForm1D, (short)Visio.VisCellIndices.vis1DBeginX);
                try
                {
                    beginX.GlueTo(_vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(connector.FromShapeId))).get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXFormOut, (short)Visio.VisCellIndices.visXFormPinX));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }

                // Connect the end point of the dynamic connector to the PinX cell of the second 2-D shape.
                var endX = connectorShape.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXForm1D, (short)Visio.VisCellIndices.vis1DEndX);
                try
                {
                    endX.GlueTo(_vShapes.get_ItemFromID(Convert.ToInt16(shapesIdArray.GetValue(connector.ToShapeId))).get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXFormOut, (short)Visio.VisCellIndices.visXFormPinX));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }

                connectorShape.get_Cells("EndArrow").set_Result(Visio.VisUnitCodes.visNumber, 5);
            }

            #endregion

            visioApplication.UndoEnabled = undoEnabledPrevious;
            visioApplication.ScreenUpdating = -1;
        }
        public static System.Array ResizeArray(System.Array oldArray, int newSize)
        {
            int oldSize = oldArray.Length;
            System.Type elementType = oldArray.GetType().GetElementType();
            System.Array newArray = System.Array.CreateInstance(elementType, newSize);
            int preserveLength = System.Math.Min(oldSize, newSize);
            if (preserveLength > 0)
                System.Array.Copy(oldArray, newArray, preserveLength);
            return newArray;
        }
        public Visio.Shape DropProcessShape(Visio.Page targetPage, string shapeText, string backgroundColorFormula, string data1, string data2)
        {
            if (targetPage == null)
            {
                return null;
            }

            Visio.Document flowchartStencil = GetBasicFlowchartStencil(BASIC_FLOWCHART_STENCIL_VISIO2013, BASIC_FLOWCHART_STENCIL_VISIO2010, targetPage.Application);
            Visio.Master processMaster = flowchartStencil.Masters.get_ItemU("Process");

            Visio.Shape processShape = targetPage.Drop(processMaster, 0.0, 0.0);
            processShape.Text = shapeText;
            processShape.Data1 = data1;
            processShape.Data2 = data2;

            if (backgroundColorFormula != String.Empty)
            {
                processShape.get_CellsSRC(
                (short)Visio.VisSectionIndices.visSectionObject,
                (short)Visio.VisRowIndices.visRowFill,
                (short)Visio.VisCellIndices.visFillForegnd)
                .FormulaU = backgroundColorFormula;
            }

            processShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

            _shapes.Add(processShape);

            return processShape;
        }
        private Visio.Shape DropProfile(Visio.Page page, string name, string fullNodePath)
        {
            return DropProcessShape(page, name, "RGB(255,0,0)", fullNodePath, String.Empty);
        }
        private Visio.Shape DropSchedule(Visio.Page page, string name, string fullNodePath, string parentNodePath)
        {
            return DropProcessShape(page, name, "RGB(0,255,0)", fullNodePath, parentNodePath);
        }
        private Visio.Shape DropAction(Visio.Page page, string name, string fullNodePath, string parentNodePath)
        {
            return DropProcessShape(page, name, "RGB(255,255,255)", fullNodePath, parentNodePath);
        }

        /// <summary>
        /// This method draws a rectangle and sets the appropriate properties of the shape to make it a text-only shape.
        /// </summary>
        /// <param name="targetPage">Reference to the page that will be used</param>
        /// <param name="shapeText">Text to be set in the text-only shape</param>
        /// <param name="colorFormula">e.g. RGB(255,0,0) for a red background</param>
        /// <returns>Text-only shape that is created, otherwise null</returns>
        public Visio.Shape DropTextShape(Visio.Page targetPage, string shapeText, string backgroundColorFormula, string fullNodePath, string parentFullNodePath)
        {
            return DropProcessShape(targetPage, shapeText, backgroundColorFormula, fullNodePath, parentFullNodePath);

            //if (targetPage == null)
            //{
            //    return null;
            //}

            //const string styleName = "Text Only";

            //Visio.Shape textOnlyShape = null;

            //try
            //{

            //    // Draw a rectangle shape.
            //    textOnlyShape = targetPage.DrawRectangle(2, 2, 3, 3);

            //    // Set the LineStyle and FillStyle properties of the shape.  The
            //    // Text Only style is one of the Visio default document styles.
            //    textOnlyShape.LineStyle = styleName;
            //    textOnlyShape.FillStyle = styleName;

            //    if (backgroundColorFormula != String.Empty)
            //    {
            //        textOnlyShape.get_CellsSRC(
            //        (short)Visio.VisSectionIndices.visSectionObject,
            //        (short)Visio.VisRowIndices.visRowFill,
            //        (short)Visio.VisCellIndices.visFillForegnd)
            //        .FormulaU = backgroundColorFormula;
            //    }

            //    // Set the shape text with the text passed in.
            //    textOnlyShape.Text = shapeText;
            //    textOnlyShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);
            //}
            //catch (Exception err)
            //{
            //    MessageBox.Show(err.ToString());
            //    throw;
            //}

            //_shapes.Add(textOnlyShape);
            //return textOnlyShape;
        }
        public Visio.Shape DropDecisionShape(Visio.Page targetPage, string shapeText, string backgroundColorFormula, string fullNodePath, string parentFullNodePath)
        {
            if (targetPage == null)
            {
                return null;
            }

            Visio.Document flowchartStencil = GetBasicFlowchartStencil(BASIC_FLOWCHART_STENCIL_VISIO2013, BASIC_FLOWCHART_STENCIL_VISIO2010, targetPage.Application);
            Visio.Master decisionMaster = flowchartStencil.Masters.get_ItemU("Decision");

            Visio.Shape decisionShape = targetPage.Drop(decisionMaster, 0.0, 0.0);
            decisionShape.Text = shapeText;
            decisionShape.Data1 = fullNodePath;
            decisionShape.Data2 = parentFullNodePath;

            if (backgroundColorFormula != String.Empty)
            {
                decisionShape.get_CellsSRC(
                (short)Visio.VisSectionIndices.visSectionObject,
                (short)Visio.VisRowIndices.visRowFill,
                (short)Visio.VisCellIndices.visFillForegnd)
                .FormulaU = backgroundColorFormula;
            }

            decisionShape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

            _shapes.Add(decisionShape);

            return decisionShape;
        }
        public Visio.Shape DropStartEndShape(Visio.Page targetPage, string shapeText, string backgroundColorFormula, string fullNodePath, string parentFullNodePath)
        {
            if (targetPage == null)
            {
                return null;
            }

            Visio.Document flowchartStencil = GetBasicFlowchartStencil(BASIC_FLOWCHART_STENCIL_VISIO2013, BASIC_FLOWCHART_STENCIL_VISIO2010, targetPage.Application);
            Visio.Master master = flowchartStencil.Masters.get_ItemU("Start/End");

            Visio.Shape shape = targetPage.Drop(master, 0.0, 0.0);
            shape.Text = shapeText;
            shape.Data1 = fullNodePath;
            shape.Data2 = parentFullNodePath;

            if (backgroundColorFormula != String.Empty)
            {
                shape.get_CellsSRC(
                (short)Visio.VisSectionIndices.visSectionObject,
                (short)Visio.VisRowIndices.visRowFill,
                (short)Visio.VisCellIndices.visFillForegnd)
                .FormulaU = backgroundColorFormula;
            }

            shape.Characters.set_CharProps((short)Visio.VisCellIndices.visCharacterSize, 8);

            _shapes.Add(shape);

            return shape;
        }

        private Visio.Shape GetShape(string registryPath)
        {
            Visio.Shape shape = null;
            try
            {
                shape = _shapes.First(s => s.Data1 == registryPath);
            }
            catch (InvalidOperationException)
            {
                try
                {
                    shape = _shapes.First(s => s.Data1 == _attender.FormatRegistryPath(registryPath));
                }
                catch
                {
                }
            }
            if (shape == null)
            {
                var test = String.Empty;
            }
            return shape;
        }
        private int GetShapeId(string registryPath)
        {
            if (string.IsNullOrEmpty(registryPath))
            {
                Debug.WriteLine("Registry path is empty");
                return -1;
            }
            foreach (var profile in AllProfiles)
            {
                if (profile.FullNodePath == registryPath)
                {
                    return profile.ShapeId;
                }
                foreach (var schedule in profile.Schedules)
                {
                    if (schedule.FullNodePath == registryPath)
                    {
                        return schedule.ShapeId;
                    }
                    foreach (var action in schedule.ActionList)
                    {
                        if (action.FullNodePath == registryPath)
                        {
                            return action.ShapeId;
                        }
                    }
                }
            }
            Debug.WriteLine("Unknown registry path: {0}", registryPath);
            return -1;
        }

        private bool CheckForCancellation()
        {
            return _backgroundWorker.CancellationPending;
        }
        private bool GetBoolResultFromStringValue(string value)
        {
            if (value != null && !String.IsNullOrEmpty(value) && value.Equals("Yes", StringComparison.InvariantCultureIgnoreCase) || value.Equals("True", StringComparison.InvariantCultureIgnoreCase) || value.Equals("1"))
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Connectors

        private void AddLink(string parentPath, string shapePath, string text)
        {
            var parentShape = GetShape(parentPath);
            var shape = GetShape(shapePath);

            if (parentShape != null && shape != null)
            {
                AddLink(parentShape, shape, text);
            }
        }
        private void AddLink(string parentPath, Visio.Shape shape, string text)
        {
            var parentShape = GetShape(parentPath);

            if (parentShape != null)
            {
                AddLink(parentShape, shape, text);
            }
        }
        private void AddLink(Visio.Shape parentShape, Visio.Shape shape, string text)
        {
            ConnectWithDynamicGlueAndConnector(parentShape, shape, text);
        }

        /// <summary>
        /// This method accesses the Basic Flowchart Shapes stencil and
        /// the dynamic connector master on the stencil. It connects two 2-D
        /// shapes using the dynamic connector by gluing the connector to the
        /// PinX cells of the 2-D shapes to create dynamic (walking) glue.
        ///
        /// Note: To get dynamic glue, a dynamic connector must be used and
        /// connected to the PinX or PinY cell of the 2-D shape.
        /// For more information about dynamic glue, see the "Working with 1-D
        /// Shapes, Connectors, and Glue" section in the book, Developing 
        /// Microsoft Visio Solutions.
        /// </summary>
        /// <param name="shapeFrom">Shape from which the dynamic connector
        /// begins</param>
        /// <param name="shapeTo">Shape at which the dynamic connector ends
        /// </param>
        public void ConnectWithDynamicGlueAndConnector(Visio.Shape shapeFrom, Visio.Shape shapeTo, string text)
        {
            if (shapeFrom == null || shapeTo == null)
            {
                return;
            }

            Visio.Application visioApplication;
            Visio.Document stencil;
            Visio.Master masterInStencil;
            Visio.Shape connector;
            Visio.Cell beginX;
            Visio.Cell endX;

            // Get the Application object from the shape.
            visioApplication = (Visio.Application)shapeFrom.Application;

            try
            {
                // Verify that the shapes are on the same page.
                if (shapeFrom.ContainingPage != null && shapeTo.ContainingPage != null && shapeFrom.ContainingPage.Equals(shapeTo.ContainingPage))
                {

                    // Access the Basic Flowchart Shapes stencil from the Documents collection of the application.
                    stencil = GetBasicFlowchartStencil(BASIC_FLOWCHART_STENCIL_VISIO2013, BASIC_FLOWCHART_STENCIL_VISIO2010, visioApplication);

                    // Get the dynamic connector master on the stencil by its universal name.
                    masterInStencil = stencil.Masters.get_ItemU(DYNAMIC_CONNECTOR_MASTER);

                    // Drop the dynamic connector on the active page.
                    connector = shapeFrom.ContainingPage.Drop(masterInStencil, 0, 0);

                    // Set Text
                    connector.Text = text;

                    // Connect the begin point of the dynamic connector to the PinX cell of the first 2-D shape.
                    beginX = connector.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXForm1D, (short)Visio.VisCellIndices.vis1DBeginX);
                    beginX.GlueTo(shapeFrom.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXFormOut, (short)Visio.VisCellIndices.visXFormPinX));

                    // Connect the end point of the dynamic connector to the PinX cell of the second 2-D shape.
                    endX = connector.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXForm1D, (short)Visio.VisCellIndices.vis1DEndX);
                    endX.GlueTo(shapeTo.get_CellsSRC((short)Visio.VisSectionIndices.visSectionObject, (short)Visio.VisRowIndices.visRowXFormOut, (short)Visio.VisCellIndices.visXFormPinX));

                    connector.get_Cells("EndArrow").set_Result(Visio.VisUnitCodes.visNumber, 5);
                }
                else
                {
                    // Processing cannot continue because the shapes are not on the same page.
                    Debug.WriteLine(MESSAGE_NOT_SAME_PAGE);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
            }
        }

        private static Visio.Document GetBasicFlowchartStencil(string BASIC_FLOWCHART_STENCIL_VISIO2013, string BASIC_FLOWCHART_STENCIL_VISIO2010, Visio.Application visioApplication)
        {
            Visio.Document stencil;
            try
            {
                stencil = visioApplication.Documents.OpenEx(BASIC_FLOWCHART_STENCIL_VISIO2013, (short)Visio.VisOpenSaveArgs.visOpenDocked);
            }
            catch (Exception)
            {
                //TODO There has to be a better way to do  Detect Visio version before opening?
                stencil = visioApplication.Documents.OpenEx(BASIC_FLOWCHART_STENCIL_VISIO2010, (short)Visio.VisOpenSaveArgs.visOpenDocked);
            }
            return stencil;
        }

        #endregion

        #endregion

        #region Action Properties

        private bool IsScheduleChild(Helper.Action action, Helper.ScheduleInformation schedule)
        {
            return action.ParentFullNodePath.Equals(schedule.FullNodePath);
        }

        /// <summary>
        /// Checks if the current action is a default action.
        /// </summary>
        /// <param name="action">Action</param>
        /// <returns>True if the action is a default action, false otherwise</returns>
        private bool IsADefaultAction(Helper.Action action)
        {
            return GetBoolResultFromStringValue(action.Default);
        }

        private bool IsChild(Helper.Action action)
        {
            return !String.IsNullOrEmpty(action.ChildIndex);
        }

        /// <summary>
        /// Checks if the action has a digit assigned.
        /// </summary>
        /// <param name="action">Action</param>
        /// <returns>True if the action has a digit assigned, false otherwise</returns>
        private bool HasDigit(Helper.Action action)
        {
            return !action.Digit.Equals("-") && !action.Digit.Equals("X");
        }

        private bool HasChildren(Helper.Action action)
        {
            return _attender.HasActions(action.FullNodePath);
        }

        #endregion

        #region Visio Custom Properties

        /// <summary>This method creates a custom property for the shape that
        /// is passed in as a parameter.</summary>
        /// <param name="addedToShape">Shape to which the custom property is 
        /// to be added</param>
        /// <param name="localRowName">Local name for the row. This name will
        /// appear in custom properties dialog for users running in developer
        /// mode.</param>
        /// <param name="rowNameU">Universal name of the custom property to be
        /// created</param>
        /// <param name="labelName">Label of the custom property</param>
        /// <param name="propType">Type of the value of the custom property.
        /// Not all VisCellVals constants are valid for this parameter. Only
        /// constants that start with visPropType make sense in this context.
        /// </param>
        /// <param name="format">Format of the custom property</param>
        /// <param name="prompt">Prompt for the custom property</param>
        /// <param name="askOnDrop">Value of the "Ask On Drop" check box of the
        /// custom property. Only seen in developer mode</param>
        /// <param name="hidden">Value of the "Hidden" check box of the custom
        /// property. Only seen in developer mode</param>
        /// <param name="sortKey">Value of the Sort key of the custom property.
        /// Only seen in developer mode</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool AddCustomProperty(Visio.Shape shape, string labelName, string value)
        {

            Visio.Cell shapeCell;
            short rowIndex;
            StringValueInCell formatHelper;
            bool returnValue = false;


            if (shape == null)
            {
                return false;
            }

            try
            {
                formatHelper = new StringValueInCell();

                rowIndex = shape.AddNamedRow((short)(Visio.VisSectionIndices.visSectionProp), labelName, (short)(Visio.VisRowIndices.visRowProp));

                shapeCell = shape.get_CellsSRC((short)Visio.VisSectionIndices.visSectionProp, rowIndex, (short)Visio.VisCellIndices.visCustPropsLabel);
                formatHelper.SetCellValueToString(shapeCell, value);

                returnValue = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
                throw;
            }

            return returnValue;
        }

        /// <summary>This method reads a custom property and returns its value.</summary>
        /// <param name="customPropertyShape">Shape which has the custom
        /// property</param>
        /// <param name="cellName">Name of the custom property to be accessed
        /// </param>
        /// <param name="isLocalName">Cell name is a universal or local name
        /// </param>
        public string ReadANamedCustomProperty(Visio.Shape customPropertyShape, string cellName, bool isLocalName)
        {
            if (customPropertyShape == null || cellName == null)
            {
                return String.Empty;
            }

            const string CUST_PROP_PREFIX = "Prop.";

            Visio.Application visioApplication = (Visio.Application)customPropertyShape.Application;

            string propName;
            Visio.Cell customPropertyCell = null;

            try
            {
                // Verify that all incoming string parameters are not of zero 
                // length, except for the ones that have default values as ""
                // and the output parameters.
                if (cellName.Length == 0)
                {
                    throw new System.ArgumentNullException("cellName", "Zero length string input.");
                }

                // Custom properties have a prefix of "Prop".
                propName = CUST_PROP_PREFIX + cellName;

                // If the custom property exists, get the value of the cell from
                // the shape.
                if (isLocalName)
                {
                    if (customPropertyShape.get_CellExists(propName, (short)Visio.VisExistsFlags.visExistsAnywhere) != 0)
                    {
                        // If the cell exists, the cell variable gets a
                        // valid value
                        customPropertyCell = customPropertyShape.get_Cells(propName);
                    }
                }
                else if (customPropertyShape.get_CellExistsU(propName, (short)Visio.VisExistsFlags.visExistsAnywhere) != 0)
                {
                    // If the cell exists, the cell variable gets a
                    // valid value
                    customPropertyCell = customPropertyShape.get_CellsU(propName);
                }

                // Only display message boxes if the AlertResponse is 0.
                if (visioApplication.AlertResponse == 0)
                {
                    // Proceed if the property exists.
                    if (customPropertyCell == null)
                    {
                        MessageBox.Show(propName + " was not found.");
                    }
                    else
                    {
                        return customPropertyCell.get_ResultStr(Visio.VisUnitCodes.visNoCast);
                    }
                }
                return String.Empty;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
                throw;
            }
        }

        #endregion

        #region Enable/Disable Buttons

        private void EnableInterface(bool enable)
        {
            btnImportProfiles.Enabled = enable;

            callIdEditBox.Enabled = enable;

            btnLoadProfiles.Enabled = enable;
            btnSaveProfiles.Enabled = enable;

            btnHelp.Enabled = enable;
        }

        #endregion

        #region Load/Save

        private void btnSaveProfiles_Click(object sender, RibbonControlEventArgs e)
        {
            if (!CheckLicense(true))
            {
                return;
            }

            if (_attender == null)
                return;

            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = "XML Files|*.xml";

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                _attender.SaveTo(saveDialog.FileName);
                MessageBox.Show("Profiles were successfully exported");
            }
        }
        private void btnLoadProfiles_Click(object sender, RibbonControlEventArgs e)
        {
            if (!CheckLicense(true))
            {
                return;
            }

            var loadDialog = new OpenFileDialog();
            loadDialog.Filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*";

            if (loadDialog.ShowDialog() == DialogResult.OK)
            {
                var filename = loadDialog.FileName;
                if (_attender == null)
                {
                    _attender = new WMIHelper();
                }

                _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.WorkerSupportsCancellation = true;
                _backgroundWorker.WorkerReportsProgress = true;

                _backgroundWorker.DoWork += (s, eventArgs) =>
                {
                    try
                    {
                        EnableInterface(false);

                        _backgroundWorker.ReportProgress(10, "Loading file...");
                        _attender.LoadFrom(filename);

                        _backgroundWorker.ReportProgress(10, "Getting profiles...");
                        AllProfiles = new List<AttendantProfile>(_attender.AttendantProfiles);

                        _backgroundWorker.ReportProgress(10, "Drawing profiles...");
                        DrawAttendant();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    finally
                    {
                        EnableInterface(true);
                    }
                };

                _backgroundWorker.ProgressChanged += (s, eventArgs) =>
                {
                    _progressDialog.Message = eventArgs.UserState as String;
                };

                _backgroundWorker.RunWorkerCompleted += (s, args) =>
                {
                    _progressDialog.Close();
                    EnableInterface(true);
                };

                _progressDialog = new ProgressDialog();
                _progressDialog.Cancelled += (cancelSender, cancelEventArgs) =>
                {
                    if (_backgroundWorker.WorkerSupportsCancellation == true)
                    {
                        _backgroundWorker.CancelAsync();
                        _progressDialog.Close();
                        EnableInterface(true);
                    }
                };

                _progressDialog.Show();
                _backgroundWorker.RunWorkerAsync();
            }
        }

        #endregion

        private void AttenderRibbon_Close(object sender, EventArgs e)
        {
            SaveConfigurationSettings();
        }

        #region Help Button

        private void btnHelp_Click(object sender, RibbonControlEventArgs e)
        {
            OpenHelpFile();
        }
        private void OpenHelpFile()
        {
            var helpFilename = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Attender.chm");

            if (System.IO.File.Exists(helpFilename))
            {
                System.Diagnostics.Process.Start(helpFilename);
            }
            else
            {
                MessageBox.Show("Help file (Attender.chm) not found");
            }
        }

        #endregion

        #region License

        private bool CheckLicense(bool showTrialDialog)
        {
            if (_isLicensed)
            {
                return true;
            }


            //Get Base String
            var baseString = _trialHelper.GetBaseString();

            if (_trialHelper.HasRegistered())
            {
                _isLicensed = true;
                btnPurchase.Visible = false;
                lblTrial.Visible = false;
                return true;
            }

            //Got a serial in the app.config file?
            if (!String.IsNullOrEmpty(Serial))
            {
                //Is it valid?
                var serial = _trialHelper.GetSerial();
                if (!String.Equals(Serial, serial))
                {
                    _isLicensed = false;
                    return false;
                }
            }
            else
            {
                if (_trialHelper.HasTrialExpired())
                {
                    if (showTrialDialog)
                    {
                        ShowTrialDialog();
                        return _isLicensed;
                    }
                }
                else
                {
                    lblTrial.Label = String.Format("{0} days left", _trialHelper.DaysToEnd());
                    _isLicensed = false;
                }
            }

            return true;
        }
        private void ShowTrialDialog()
        {
            switch (_trialHelper.ShowDialog())
            {
                case TrialHelper.RunTypes.Full:
                    lblTrial.Visible = false;
                    btnPurchase.Visible = false;
                    _configuration.AppSettings.Settings["Serial"].Value = _trialHelper.GetSerial();
                    _configuration.Save();
                    _isLicensed = true;
                    break;
                case TrialHelper.RunTypes.Expired:
                case TrialHelper.RunTypes.Trial:
                case TrialHelper.RunTypes.Unknown:
                    _isLicensed = false;
                    var numDaysLeft = _trialHelper.DaysToEnd();
                    lblTrial.Label = String.Format("{0} days left", numDaysLeft);
                    btnPurchase.Visible = true;
                    if (numDaysLeft <= 0)
                    {
                        EnableInterface(false);
                    }
                    break;
            }
        }
        private void btnPurchase_Click(object sender, RibbonControlEventArgs e)
        {
            ShowTrialDialog();
        }

        #endregion

        private void btnLoadFromReg_Click(object sender, RibbonControlEventArgs e)
        {
            if (!CheckLicense(true))
            {
                return;
            }
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Filter = "REG Files (*.reg)|*.reg|All Files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = openFileDialog.FileName;
                _backgroundWorker = new BackgroundWorker()
                {
                    WorkerSupportsCancellation = true,
                    WorkerReportsProgress = true
                };
                _backgroundWorker.DoWork += new DoWorkEventHandler((object s, DoWorkEventArgs eventArgs) => {
                    EnableInterface(false);
                    _backgroundWorker.ReportProgress(10, "Loading registry file...");
                    _reader = new RegFileReader(fileName);
                    _backgroundWorker.ReportProgress(20, "Getting profiles...");
                    if (_reader.GetAttendantInformation(_reader.GetAttendantProfiles(null)))
                    {
                        AllProfiles = _reader.AttendantProfiles;
                    }
                    _backgroundWorker.ReportProgress(40, "Drawing profiles...");
                    DrawAttendant();
                });
                _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler((object s, ProgressChangedEventArgs eventArgs) => _progressDialog.Message = eventArgs.UserState as string);
                _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object s, RunWorkerCompletedEventArgs args) => {
                    if (args.Error != null)
                    {
                        MessageBox.Show(args.Error.ToString());
                    }
                    _progressDialog.Close();
                    EnableInterface(true);
                });
                _progressDialog = new ProgressDialog();
                _progressDialog.Cancelled += new EventHandler<EventArgs>((object cancelSender, EventArgs cancelEventArgs) => {
                    if (_backgroundWorker.WorkerSupportsCancellation)
                    {
                        _backgroundWorker.CancelAsync();
                        _progressDialog.Close();
                        EnableInterface(true);
                    }
                });
                _progressDialog.Show();
                _backgroundWorker.RunWorkerAsync();
            }
        }
    }
}
