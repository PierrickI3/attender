﻿namespace Addin
{
    partial class CICConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblCICUsername = new System.Windows.Forms.Label();
            this.lblCICPassword = new System.Windows.Forms.Label();
            this.lblCICServer = new System.Windows.Forms.Label();
            this.txtDomainUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtCICServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(138, 101);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(219, 101);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblCICUsername
            // 
            this.lblCICUsername.AutoSize = true;
            this.lblCICUsername.Location = new System.Drawing.Point(13, 13);
            this.lblCICUsername.Name = "lblCICUsername";
            this.lblCICUsername.Size = new System.Drawing.Size(99, 13);
            this.lblCICUsername.TabIndex = 2;
            this.lblCICUsername.Text = "Domain\\Username:";
            // 
            // lblCICPassword
            // 
            this.lblCICPassword.AutoSize = true;
            this.lblCICPassword.Location = new System.Drawing.Point(54, 39);
            this.lblCICPassword.Name = "lblCICPassword";
            this.lblCICPassword.Size = new System.Drawing.Size(56, 13);
            this.lblCICPassword.TabIndex = 3;
            this.lblCICPassword.Text = "Password:";
            // 
            // lblCICServer
            // 
            this.lblCICServer.AutoSize = true;
            this.lblCICServer.Location = new System.Drawing.Point(49, 65);
            this.lblCICServer.Name = "lblCICServer";
            this.lblCICServer.Size = new System.Drawing.Size(61, 13);
            this.lblCICServer.TabIndex = 4;
            this.lblCICServer.Text = "CIC Server:";
            // 
            // txtDomainUsername
            // 
            this.txtDomainUsername.Location = new System.Drawing.Point(116, 10);
            this.txtDomainUsername.MaxLength = 255;
            this.txtDomainUsername.Name = "txtDomainUsername";
            this.txtDomainUsername.Size = new System.Drawing.Size(178, 20);
            this.txtDomainUsername.TabIndex = 5;
            this.txtDomainUsername.Text = "DEV2000\\devlab_user";
            this.txtDomainUsername.TextChanged += new System.EventHandler(this.txtCICUsername_TextChanged);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(116, 36);
            this.txtPassword.MaxLength = 255;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(178, 20);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.Text = "Dogfood";
            this.txtPassword.TextChanged += new System.EventHandler(this.txtCICPassword_TextChanged);
            // 
            // txtCICServer
            // 
            this.txtCICServer.Location = new System.Drawing.Point(116, 62);
            this.txtCICServer.MaxLength = 255;
            this.txtCICServer.Name = "txtCICServer";
            this.txtCICServer.Size = new System.Drawing.Size(178, 20);
            this.txtCICServer.TabIndex = 7;
            this.txtCICServer.Text = "172.19.32.57";
            this.txtCICServer.TextChanged += new System.EventHandler(this.txtCICServer_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Use Domain\\Windows credentials and NOT CIC credentials";
            // 
            // CICConfigurationForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(306, 131);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCICServer);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtDomainUsername);
            this.Controls.Add(this.lblCICServer);
            this.Controls.Add(this.lblCICPassword);
            this.Controls.Add(this.lblCICUsername);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CICConfigurationForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CIC Configuration";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CICConfigurationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblCICUsername;
        private System.Windows.Forms.Label lblCICPassword;
        private System.Windows.Forms.Label lblCICServer;
        private System.Windows.Forms.TextBox txtDomainUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtCICServer;
        private System.Windows.Forms.Label label1;
    }
}