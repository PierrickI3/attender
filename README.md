# ATTENDER #

The Attender package is a Microsoft Office Visio 2010/2013/2016 (32/64-bit) add-in developed to export Interaction Attendant profiles from PureConnect 2.3.1/2.4/3.0/4.0/201XRX.

### Minimum Requirements ###
* Visio 2010/2013/2016 x86 (32-bit) or x64 (64-bit)
* 4 GB RAM (depends on the number of profiles)
* 2 CPU cores

### How do I get set up? ###

* Clone this repository
* Open the Solution in Visual Studio 2017
* Click on `Start`. Visual Studio will open Visio automatically.

### Help File ###
* To modify and generate a new help file, please follow these instructions
  * Install [Sandcastle Help File Builder](https://github.com/EWSoftware/SHFB)
  * Load the `Doc\Doc.shfbproj`
  * Use its GUI to build the help file
  * Copy the generated help file manually in the Add-in's bin\Release folder

### Setup ###
* To generate setup file, use the `Publish` option in the Addin project (right-click Addin project, select Properties and go to the Publish section). Output will be stored in the \Setup folder by default.
* Setup will automatically install .Net Framework 4.6.2 and the VSTO Office Runtime (x86 & x84)

### Troubleshooting ###
* Do not run the setup file from a network share, this will cause a certificate error

### Who do I talk to? ###

* Repo owner (that's me!)
